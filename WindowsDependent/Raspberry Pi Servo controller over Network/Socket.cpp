#include "Socket.h"

Socket::Socket() 
{
	handle = 0;
}

Socket::~Socket() 
{
	Close();
}

bool Socket::Init() 
{
	#if PLATFORM == PLATFORM_WINDOWS
	WSADATA WsaData;
	return WSAStartup( MAKEWORD(2,2), &WsaData ) == NO_ERROR;
	#else
	return true;
	#endif
}

void Socket::Quit()
{
	#if PLATFORM == PLATFORM_WINDOWS
	WSACleanup();
	#endif
}

bool Socket::Open( unsigned short port ) // Open and listen to port 'port'
{
	if( IsOpen() ) 
	{
		printf( "Socket is already open\n" );
	}
	
	handle = socket( AF_INET, SOCK_DGRAM, IPPROTO_UDP );
	
	if ( handle <= 0 )
	{
		printf( "failed to create socket\n" );
		handle = 0;
		return false;
	}

	// bind to port

	sockaddr_in address;
	address.sin_family = AF_INET;
	address.sin_addr.s_addr = INADDR_ANY;
	address.sin_port = htons( (unsigned short) port );

	if ( bind( handle, (const sockaddr*) &address, sizeof(sockaddr_in) ) < 0 )
	{
		printf( "failed to bind socket\n" );
		//Close();
		return false;
	}
	return true;
}

void Socket::Close() // Close open port
{
	if( IsOpen() ) 
	{
		#if PLATFORM == PLATFORM_MAC || PLATFORM == PLATFORM_UNIX
		close( handle );
		#elif PLATFORM == PLATFORM_WINDOWS
		closesocket( handle );
		#endif
		handle = 0;
	}
}

bool Socket::UnBind() 
{
	// set non-blocking
	#if PLATFORM == PLATFORM_MAC || PLATFORM == PLATFORM_UNIX

		int nonBlocking = 1;
		if ( fcntl( handle, F_SETFL, O_NONBLOCK, nonBlocking ) == -1 )
		{
			printf( "failed to set non-blocking socket\n" );
			Close();
			return false;
		}
	
	#elif PLATFORM == PLATFORM_WINDOWS

		DWORD nonBlocking = 1;
		if ( ioctlsocket( handle, FIONBIO, &nonBlocking ) != 0 )
		{
			printf( "failed to set non-blocking socket\n" );
			Close();
			return false;
		}

	#endif
}

bool Socket::IsOpen() const // Return true if port is open and bound
{
	return handle != 0;	//Return true if not equals 0
}

bool Socket::Send( const Address &destination, const void* data, int size ) // Send data to address
{
	if(data == NULL || size <= 0)
	{
		printf("Invalid data or buffer size\n");
		return false;
	}
	
	if ( handle == 0 )
	{
		printf("Invalid socket\n");
		return false;
	}
	
	sockaddr_in address;
	address.sin_family = AF_INET;
	address.sin_addr.s_addr = htonl( destination.GetAddress() );
	address.sin_port = htons( (unsigned short) destination.GetPort() );

	int sent_bytes = sendto( handle, (const char*)data, size, 0, (sockaddr*)&address, sizeof(sockaddr_in) );

	return sent_bytes == size;	// return true if sent_bytes equals size
}

int Socket::Receive( Address & sender, void * data, int size ) { // Check if received something. If no data was received, return 0

	if( size <= 0 ) 
	{
		printf("Invalid buffer size\n");
		return false;
	}

	if ( handle == 0 )
	{
		printf("Invalid socket\n");
		return false;
	}
	
	#if PLATFORM == PLATFORM_WINDOWS
	typedef int socklen_t;
	#endif
	
	sockaddr_in from;
	socklen_t fromLength = sizeof( from );

	int received_bytes = recvfrom( handle, (char*)data, size, 0, (sockaddr*)&from, &fromLength );

	if ( received_bytes <= 0 )
		return 0;

	unsigned int address = ntohl( from.sin_addr.s_addr );
	unsigned int port = ntohs( from.sin_port );

	sender = Address( address, port );

	return received_bytes;
}