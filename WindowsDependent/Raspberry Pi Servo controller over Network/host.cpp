#include <iostream>
#include "Socket.h"
#include "Address.h"
#include <windows.h>

#define CLIENTPORT 30000
#define BUFFERSIZE 256
#define ACK 			0x06	//Hexadecimal representation of 06

using namespace std;

int main()
{
	Socket client;
	if( Socket::Init() && client.Open( CLIENTPORT )/* && client.UnBind() */ )
	{
		cout << "Socket initialised!" << endl;
	} else {
		cerr << "Error opening Socket!" << endl;
		return 1;
	}
	
	cout << "Raspberry IP address: ";
	// Stolen from 'Octothorp.cpp' from 'Octothorp Multiplayer' 03-09-2014 {
	string serverIP_str;
	getline(cin,serverIP_str);
	//subdivide string
	int IP[4];
	unsigned short pos[4];
	pos[0] = serverIP_str.find('.');
	pos[1] = serverIP_str.find('.',pos[0]+1);
	pos[2] = serverIP_str.find('.',pos[1]+1);
	pos[3] = serverIP_str.find('\n',pos[2]+1);
	IP[0] = atoi(serverIP_str.substr(0,pos[0]).c_str());
	IP[1] = atoi(serverIP_str.substr(pos[0]+1,pos[1]).c_str());
	IP[2] = atoi(serverIP_str.substr(pos[1]+1,pos[2]).c_str());
	IP[3] = atoi(serverIP_str.substr(pos[2]+1,pos[3]).c_str());
	// ..................................... }
	Address clientAddress;
	clientAddress = Address(IP[0],IP[1],IP[2],IP[3],CLIENTPORT);
	char data[1] = { ACK };
	client.Send( clientAddress, data, sizeof(ACK) );
	
	Address sender;
	unsigned char buffer[BUFFERSIZE];
	int bytes_read;
	
	while(true)
	{
		bytes_read = client.Receive( sender, buffer, sizeof(buffer) );
		cout << "Received: ";
		if(buffer[0] == ACK)
		{
			cout << "ACK" << endl;
			client.Send( sender, data, sizeof(ACK) );
		}
		else
			cout << buffer << endl;
		Sleep(100); // To avoid stealing all da interwebz from the other meatbags
	}
	return 0;
}