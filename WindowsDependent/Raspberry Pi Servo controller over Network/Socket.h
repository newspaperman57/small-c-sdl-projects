#ifndef __SOCKET
#define __SOCKET

// platform detection
#define PLATFORM_WINDOWS  1
#define PLATFORM_MAC      2
#define PLATFORM_UNIX     3

#if defined(_WIN32)
#define PLATFORM PLATFORM_WINDOWS
#elif defined(__APPLE__)
#define PLATFORM PLATFORM_MAC
#else
#define PLATFORM PLATFORM_UNIX
#endif

#if PLATFORM == PLATFORM_WINDOWS

	#include <winsock2.h>
	#pragma comment( lib, "wsock32.lib" )

#elif PLATFORM == PLATFORM_MAC || PLATFORM == PLATFORM_UNIX

	#include <sys/socket.h>
	#include <netinet/in.h>
	#include <fcntl.h>

#else

	#error unknown platform!

#endif

#include "Address.h"
#include <stdio.h>

class Socket
{
public:

	Socket();
	~Socket();
	static bool Init();
	static void Quit();
	bool Open( unsigned short port );
	void Close();
	bool UnBind();
	bool IsOpen() const;
	bool Send( const Address & destination, const void * data, int size );
	int Receive( Address & sender, void * data, int size );

private:

	int handle;
};

#endif