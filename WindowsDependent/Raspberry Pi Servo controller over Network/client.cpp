#include <iostream>
#include "Socket.h"
#include "Address.h"
#include <windows.h>

#define CLIENTPORT 30000
#define BUFFERSIZE 256
#define ACK 			0x06	//Hexadecimal representation of 06

using namespace std;

int main()
{
	Socket host;
	if( Socket::Init() && host.Open( HOSTPORT )/* && host.UnBind() */ )
	{
		cout << "Socket initialised!" << endl;
	} else {
		cerr << "Error opening Socket!" << endl;
		return 1;
	}
	
	char data[1] = { ACK };
	Address sender;
	unsigned char buffer[BUFFERSIZE];
	int bytes_read;
	
	while(true)
	{
		bytes_read = host.Receive( sender, buffer, sizeof(buffer) );
		cout << "Received: ";
		if(buffer[0] == ACK)
		{
			cout << "ACK" << endl;
			host.Send( sender, data, sizeof(ACK) );
		}
		else
			cout << buffer << endl;
		Sleep(100); // To avoid stealing all da interwebz from the other meatbags
	}
	return 0;
}