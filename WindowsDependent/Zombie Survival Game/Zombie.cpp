#include "Zombie.h"

Zombie::Zombie()
{
	maxhealth = 150;
	health = maxhealth;
	damage = 20; /*Gets higher for every level the player reaches.*/
}

Zombie::~Zombie()
{
	//Drop("Food")
}

int Zombie::GetDamage()
{
	return damage;
}

void Zombie::OnSave(string file)
{
	
}

void Zombie::SetPos(int x, int y)
{
	posX = x;
	posY = y;
}

void Zombie::TakeDamage(int amount)
{
	health -= amount;
}

int Zombie::GetHealth()
{
	return health;
}