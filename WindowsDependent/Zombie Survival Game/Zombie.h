#ifndef __ZOMBIE
#define __ZOMBIE

#include <iostream>

using namespace std;

class Zombie
{
	private:
		int maxhealth;
		int health;
		int damage;
		int posX;
		int posY;
	public:
		void SetPos(int x, int y);
		int GetHealth();
		int GetDamage();
		void TakeDamage(int amount);
		Zombie();
		~Zombie();
};

#endif