#include <iostream>
#include <windows.h>
#include "Window.h"
//#include "Savegame.cpp"

using namespace std;

int Window::screenSizeX;
int Window::screenSizeY;
int Window::screenY;
int Window::screenX;
string Window::screen;
string Window::screenColor[2048];
int Window::setColor;
	

class Player
{
	private:
		
	public:
		static void Print(int x, int y, int color);
};

void drawTree(int x, int y ,int health)
{
	
}

void drawpausemenu(int chosenoption,int x,int y,bool gamesaved)
{  	
	Sleep(1000/2);
	int standardColor;
	Window::SetTextColor(standardColor);
	//system("cls"); 
	
	Window::Write(1, 1, chosenoption);
	int middleX = x/2;
	int middleY = y/2;
	int posX = middleX - 5;
	int posY = middleY - 3;
	if(gamesaved == true)
	{
		Window::Write(middleX + 3 ,middleY + 3, standardColor,"Nope...");
	}
	Window::Write(middleX-3,middleY - 3, "RESUME");
	Window::Write(middleX-3,middleY ,"OPTIONS");
	Window::Write(middleX-3,middleY + 3,"SAVE");
	Window::Write(middleX-3,middleY + 6,"EXIT");
	if( chosenoption == 1 )
	{
		posY = middleY - 3;
	}
	else if( chosenoption == 2 )
	{
		posY = middleY;
	}
	else if( chosenoption == 3 )
	{
		posY = middleY + 3;
	}
	else if( chosenoption == 4 )
	{
		posY = middleY + 6;
	}
	Window::Write(posX,posY,">");
	Sleep(1000/5);
}

bool pausemenu(int x, int y)
{
	bool gamesaved = false;
	bool gamepaused = true;
	int chosenoption = 1;
	drawpausemenu( chosenoption , x , y , gamesaved );
	while(gamepaused)
	{
		if(chosenoption > 1) 
		{
			if( GetAsyncKeyState(VK_UP) != 0 || GetAsyncKeyState(0x57) != 0 ) // W
			{
				chosenoption--;
				drawpausemenu( chosenoption , x , y , gamesaved );
			}
		}
		if(chosenoption < 4) //  Yay! a heart :3
		{
			if( GetAsyncKeyState(VK_DOWN) != 0 || GetAsyncKeyState(0x53) != 0 ) // S
			{
				chosenoption++;
				drawpausemenu( chosenoption , x , y , gamesaved );
			}
		}
		if(GetAsyncKeyState(VK_ESCAPE) != 0) // ESCAPE
		{
			Sleep(1000/2);
			gamepaused = false;
		}
		if( GetAsyncKeyState(VK_RETURN) != 0 || GetAsyncKeyState(0x45) != 0 ) // ENTER or E
		{
			if(chosenoption == 1) // RESUME
			{
				gamepaused = false;
			}
			if(chosenoption == 3) // RESUME
			{
				gamesaved = true;
			}
			else if(chosenoption == 4) // EXIT
			{
				Window::Write(x/2-6,y/2, "EXTERMINATE!!!");
				Sleep(1);
				return false;
			}
			drawpausemenu( chosenoption , x , y , gamesaved );
		}
		Sleep(1000/60);
	}
	return true;
}

static void Player::Print(int x, int y, int color)
{
	Window::Write(x,y,color,"O");
}

int main()
{
	int screenX, screenY, stamina, staminapercent;
	float playerX, playerY, speed, i, staminacount;
	bool running = true;
	bool staminabool = true;
	
	staminacount = 30;
	
	i = 32;
	
	screenX = 32+6;
	screenY = 32;
	
	playerX = (1600 / 2) / 8 - screenX / 2;
	playerY = (900 / 2) / 8 - screenY / 2;
	
	speed = 0.2;
	
	Window window;
	
	/*
	
	for 1,math.random(25, 50) do
		x = math.random(res x)	
		y = math.random(res y)
	end
	
	Random stone generator
	
	*/
	
		
		
	window.Resize(screenX, screenY);
	window.RemoveCursor();
	
	Window::InitScreen();
	while(running)
	{
	    if( GetAsyncKeyState(VK_CONTROL) != 0) // ONLY CONTROL
		{
			speed = 0.1;
		}
		else if( GetAsyncKeyState(VK_SHIFT) != 0) // ONLY SHIFT
		{
			speed = 0.4;
		}
		else if( GetAsyncKeyState(VK_SHIFT) != 0 && GetAsyncKeyState(VK_CONTROL) != 0) // SHIFT AND CONTROL
		{
			speed = 0.4;
		}
		else
		{
			speed = 0.2;
		}
		
		if(staminabool == false)
		{
			speed = 0.1;
		}
		
		if( GetAsyncKeyState(VK_UP) != 0 || GetAsyncKeyState(0x57) != 0 ) // W
		{
			Window::Write(0,0,"UP");
			playerY -= speed;
		}
		if( GetAsyncKeyState(VK_RIGHT) != 0 || GetAsyncKeyState(0x44) != 0 ) // D
		{
			Window::Write(0,0,"RIGHT");
			playerX += speed;
		}
		if( GetAsyncKeyState(VK_LEFT) != 0 || GetAsyncKeyState(0x41) != 0 ) // A
		{
			Window::Write(0,0,"LEFT");
			playerX -= speed;
		}
		if( GetAsyncKeyState(VK_DOWN) != 0 || GetAsyncKeyState(0x53) != 0 ) // S
		{
			Window::Write(0,0,"DOWN");
			playerY += speed;
		}
		
		if(
		GetAsyncKeyState(VK_UP) != 0 || GetAsyncKeyState(0x57) != 0 ||
		GetAsyncKeyState(VK_RIGHT) != 0 || GetAsyncKeyState(0x44) != 0 ||
		GetAsyncKeyState(VK_LEFT) != 0 || GetAsyncKeyState(0x41) != 0 ||
		GetAsyncKeyState(VK_DOWN) != 0 || GetAsyncKeyState(0x53) != 0) // IF JUST ONE OF THE KEYS ARE PRESSED
		{
			if(staminacount > 0)
			{
				staminacount -= speed*2;
			}
		}
		
		if((int)staminacount == 0)// NO STAMINA
		{
			staminabool = false; // CAN'T RUN
		}
		if( staminacount >= 30 ) // STAMINA AT MAX
		{
			staminabool = true;  // CAN RUN
		}
		if( GetAsyncKeyState(VK_ESCAPE) != 0 )
		{
			running = pausemenu( screenX , screenY );  // GO TO PAUSE MENU
		}
		
		if(staminacount < 30)
		{
			staminacount += 0.5;
		}
		
		window.Move( (int)playerX * 8, (int)playerY * 8);
		if(i > 47)
			i = 32;
		i+=0.5;
		Window::Write(1,5,(int)i);
		Window::SetTextColor(40);
		
		COORD windowPos = Window::GetWindowPos();
		
		for(int i = 100; i <= 110; i++)
		{
			Window::WriteMap(i,50, "O");
		}
		
		for(int i = 45; i <= 55; i++)
		{
			Window::WriteMap(105, i, "O");
		}
		
		staminapercent = staminacount/30*100;
		if(staminapercent > 100) // YOU SHOULDN'T HAVE MORE THAN 100 PERCENT
		{
			staminapercent = 100;
		}
		
		Window::SetTextColor(15);
		
		for(int staminaY = screenY-1; staminaY < screenY; staminaY++)
		{
			for(int staminaX = 0; staminaX < screenX; staminaX++)
			{
				Window::Write(staminaX,staminaY," ");
			}
		}
		if(staminabool == false) // IF YOU CAN'T RUN
		{
			Window::Write(8,screenY-2," - you can't run!");
		}
		Window::Write(1,screenY-2,"STAMINA");
		Window::Write(0,screenY-1,staminapercent);
		Window::Write(0,screenY-4,"%"); // INFO TO SCREEN END
		for(float i = 0; i < 30; i++)
		{
			Window::SetTextColor(51);
			stamina = i * 10;
			if(staminacount > i)
			{
				Window::Write(i + 5,screenY-1," "); // THE BAR ITSELF
			}
		}
		
		Window::GotoXY(0, 0);
		Window::OnRender();
		Sleep(1000/60);
		Window::SetTextColor(40);
	}
	
	Window::GotoXY(0, 0);
	return 0;
}
