#ifndef __WINDOW
#define __WINDOW

#ifndef _WIN32_WINNT
#define _WIN32_WINNT 0x0500
#endif /*_WIN32_WINNT*/

#include <Windows.h>
#include <string>
#include <iostream>

using namespace std;

class Window
{
	private:
		HWND windowHandle;
		COORD size;
		static string screen;
		static string screenColor[2048];
		static int setColor;
		static int screenX, screenY, screenSizeX, screenSizeY;
	public:
		Window();
		static void OnRender();
		bool Move(int x, int y);
		bool Resize(short int x, short int y);
		void RemoveCursor();
		static COORD GetWindowPos();
		static bool GotoXY(int x, int y);
		void GetMainMonitorRes(int &X, int &Y);
		static void SetTextColor(int);
		static bool WriteMap(int,int,string);
		HWND GetHandle();
		static void InitScreen();
		static void Write(int,int,int);
		static void Write(int,int,string);
		static void Write(int,int,int,int);
		static void Write(int,int,int,string);
};

#endif