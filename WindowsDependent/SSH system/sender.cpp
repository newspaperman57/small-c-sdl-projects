#include "socket.h"
#include <iostream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>

using namespace std;

int main()
{
	Socket::Init();
	//THIS SYSTEM IS NOT BUILT FOR INVALID OUTPUT!!!
	system("cls");
	system("title UDP SSH System");
	//Get port between 0 and 65535
	cout << "choose OUT port (must be between 0 and 65535): ";
	string input;
	getline(cin,input);
	int port_out = atoi( input.c_str() );
	
	Socket socket_sender;
	if(socket_sender.Open(port_out))
	{
		cout << "Socket opened on port " << port_out << "..." << endl << endl;
		cout << "choose recievers IP: ";//endl is added by return key
		getline(cin, input);
		//subdevide string
		int IP[4];
		unsigned short pos[4];
		pos[0] = input.find('.');
		pos[1] = input.find('.',pos[0]+1);
		pos[2] = input.find('.',pos[1]+1);
		pos[3] = input.find('\n',pos[2]+1);
		IP[0] = atoi(input.substr(0,pos[0]).c_str());
		IP[1] = atoi(input.substr(pos[0]+1,pos[1]).c_str());
		IP[2] = atoi(input.substr(pos[1]+1,pos[2]).c_str());
		IP[3] = atoi(input.substr(pos[2]+1,pos[3]).c_str());
		Address receiver(IP[0],IP[1],IP[2],IP[3],30000);
		
		system("cls");
		cout << "Enter packet data." << endl;
		cout << "Enter 'quit' to return to menu." << endl << endl;
			
		while(true)
		{
			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 0x0F);//White
			
			//Get packet data
			getline(cin,input);
			if(input == "quit")
				break;
			input += " 2>&1";
			char data[input.length()];
			strcpy(data, input.c_str());
			if(!sizeof(data))
				continue;
			socket_sender.Send(receiver, data, sizeof(data));
			
			//tell packet size
			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 0x08);//Grey
			cout << sizeof(data) << " bytes sent" << endl;
			//display reciever
			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 0x06);//Yellow
			cout << (short)receiver.GetA() << "." << (short)receiver.GetB() << "." << (short)receiver.GetC() << "." << (short)receiver.GetD() << ":" << receiver.GetPort() << endl;
			cout << endl;
			cout << "Checking for incoming packets..." << endl << endl;
			Sleep(1200);
			unsigned char buffer[256];
			Address sender;
			int bytes_read;
			do
			{
				bytes_read = socket_sender.Receive(sender, buffer, sizeof( buffer ));
				if ( bytes_read > 0)
				{
					//display sender
					SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 0x06);//Yellow
					//cout << (short)sender.GetA() << "." << (short)sender.GetB() << "." << (short)sender.GetC() << "." << (short)sender.GetD() << ":" << sender.GetPort() << endl;
					SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 0x08);//Grey
					//cout << bytes_read << " bytes read:" << endl;
					//display data
					SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 0x0F);//White
					cout << buffer;
				}
			} while (bytes_read > 0);
			cout << endl << "Done! New packet:" << endl;					
		}
	}
	Sleep(1000);
	cout << " .";
	Sleep(500);
	cout << ".";
	Sleep(500);
	cout << ".";
	Sleep(1000);
	return 0;
}