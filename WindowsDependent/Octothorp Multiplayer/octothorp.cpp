#include "octothorp.h"

#define CLIENTPORT		30000
#define SERVERPORT		25000

#define BUFFERSIZE		256


#define ACK 			0x06	//Hexadecimal representation of 06
#define NAK 			0x15	//Hexadecimal representation of 21

#define JOIN 			0x10	//Hexadecimal representation of 16
#define DISCONNECT 		0x11	//Hexadecimal representation of 17
#define LIST 			0x12	//Hexadecimal representation of 18
#define POS 			0x13	//Hexadecimal representation of 19


#define SCREEN_WIDTH 	40
#define SCREEN_HEIGHT 	20

#define SPS 			5	//STEPS PER SECOND

Game::Game()
{
	cout << "<Game> Initializing Class" << endl;
	
	
	running = true;
	controller.SetPos(0, 0);
	window.Resize(SCREEN_WIDTH, SCREEN_HEIGHT);
	window.InitScreen();
	window.RemoveCursor();
	
}

Game::~Game()
{
	cout << "<Game> Destroying Class" << endl;
	Socket::Quit();
}

bool Game::initialize()
{
	cout << "<Game> Initializing Instance" << endl;
	
	cout << "ACK: " << (char)ACK << endl;
	cout << "NAK: " << (char)NAK << endl;
	cout << "JOIN: " << (char)JOIN << endl;
	cout << "DISCONNECT: " << (char)DISCONNECT << endl;
	cout << "LIST: " << (char)LIST << endl;
	cout << "POS: " << (char)POS << endl;
	
	if ( Socket::Init() && socket_in.Open( CLIENTPORT ) && socket_in.UnBind() && socket_out.Open( CLIENTPORT+1 ) && socket_out.UnBind() )
		cout << "Sockets initialized" << endl;
	else
	{
		printf( "failed to open socket!\n" );
		return false;
	}
	
	cout << "choose recievers IP: ";//endl is added by return key
	string serverIP_str;
	getline(cin,serverIP_str);
	//subdevide string
	int IP[4];
	unsigned short pos[4];
	pos[0] = serverIP_str.find('.');
	pos[1] = serverIP_str.find('.',pos[0]+1);
	pos[2] = serverIP_str.find('.',pos[1]+1);
	pos[3] = serverIP_str.find('\n',pos[2]+1);
	IP[0] = atoi(serverIP_str.substr(0,pos[0]).c_str());
	IP[1] = atoi(serverIP_str.substr(pos[0]+1,pos[1]).c_str());
	IP[2] = atoi(serverIP_str.substr(pos[1]+1,pos[2]).c_str());
	IP[3] = atoi(serverIP_str.substr(pos[2]+1,pos[3]).c_str());
	server = Address(IP[0],IP[1],IP[2],IP[3],SERVERPORT);
	
	char data[1] = {JOIN}; 
	cout << "Connecting to server..." << endl;
	socket_out.Send(server, data, sizeof(data)); // Sending JOIN request to server.
	for(int i = 0; i < 100; i++)
	{
		//Wait 10 seconds for respond
		Sleep(100);
		Address sender;
        unsigned char buffer[BUFFERSIZE];
		int bytes_read = socket_in.Receive( sender, buffer, sizeof(buffer) );
		if(!bytes_read) // Here we need to ensure that the packet with the player-list isn't received here
			continue;
		
		if(buffer[0] == JOIN)
		{
			if(buffer[1] == NAK)
			{
				cout << "Server is full" << endl;
				return false;
			}
			else if(buffer[1] == ACK)
			{
				cout << "Connection successful" << endl;
				controller.ID = (buffer[2] << 8) | buffer[3];
				cout << "ID: " << controller.ID << endl;
				connected = true;
				return true;
			}
			else
			{
				cout << "Unknown respond from server: " << buffer << endl;
			}
		}
		else
			cout << "Unexpected respond from server:: " << buffer[1] << endl;
		
	}
	//No response from server
	cout << "Nothing received!" << endl;
	system("Title Single player!!!");
}

void Game::update()
{
	/*GET INPUT*/
	if( GetAsyncKeyState(VK_ESCAPE) != 0 )
	{
		if(connected)
		{
			unsigned char data[1] = {DISCONNECT};
			socket_out.Send(server,data,sizeof(data));
			connected = false;
		}
		else
			running = false;
	}
	controller.Update();	//Player
	//Keep player inside screen
	if( controller.GetPosX() < 0 )
	{controller.SetPos(0,controller.GetPosY());}
	else if( controller.GetPosX() >= SCREEN_WIDTH  )
	{controller.SetPos(SCREEN_WIDTH -1,controller.GetPosY());}
	if( controller.GetPosY() < 0 )
	{controller.SetPos(controller.GetPosX(),0);}
	else if( controller.GetPosY() >= SCREEN_HEIGHT  )
	{controller.SetPos(controller.GetPosX(),SCREEN_HEIGHT -1);}
	
	/*PACKET CONTROL*/
	// broadcast position
	BroadcastPos();
    // receive packets
    while ( true )
    {
        Address sender;
        unsigned char buffer[BUFFERSIZE];
        int bytes_read = socket_in.Receive( sender, buffer, sizeof( buffer ) );
        if ( !bytes_read )
            break;
        // process packet
		
		switch(buffer[0])
		{
			case DISCONNECT:
			{
				if(connected == false && buffer[1] == ACK)
					running = false;
				break;
			}
			case LIST:
			{
				cout << "Updating list!" << endl;
				players.clear();
				playerData.clear();
				//List of Clients IP - 4 bytes IP + 2 byte ID
				for(int i = 0; i < (bytes_read-1) / 6; i++)
				{
					PlayerData temp_data = { (unsigned long)(	(buffer[1+ i*6] << 24) |
															    (buffer[2+ i*6] << 16) |
																(buffer[3+ i*6] <<  8) |
																(buffer[4+ i*6])),
											 (unsigned short)(  (buffer[5+ i*6] <<  8) |
																(buffer[6+ i*6]))
										   };
					playerData.push_back(temp_data);
					Player temp_player;
					temp_player.ID = temp_data.ID;
					players.push_back(temp_player);
				}
				//Return ACK signal to server
				unsigned char data[2] = {LIST, ACK};
				socket_out.Send( server, data, sizeof(data));
				break;
			}
			case POS:
			{
				UpdatePos(buffer);
				break;
			}
		}
	}
}

void Game::UpdatePos(unsigned char buffer[])
{
	// Update the position of a player.
	short ID = ((buffer[1] << 8) | buffer[2]);
	cout << "ID: " << ID << " is being updated" << endl;
	for(int i = 0; i < playerData.size(); i++)
	{
		if(ID == playerData[i].ID && ID != controller.ID)
		{
			players[i].Activate();
			players[i].SetPos((int)buffer[3], (int)buffer[4]);
			break;
		}
	}
}

void Game::BroadcastPos()
{
	if(playerData.size() < 2)
		return;
	char data[5];
	data[0] = POS;
	short tempID = controller.ID;
	data[1] = (tempID >> 8);
	tempID = controller.ID;
	data[2] = tempID;
	data[3] = (char)controller.GetPosX();
	data[4] = (char)controller.GetPosY();
	for(int i = 0; i < playerData.size(); i++)
	{
		if(playerData[i].ID == controller.ID)
			continue;
		cout << "Sending POS to ID: " << playerData[i].ID << endl;
		socket_out.Send( Address(playerData[i].IP,CLIENTPORT), data, sizeof(data));
	}
}

void Game::run()
{
	cout << "<Game> Running mainloop!" << endl;
	while(running)
	{
		Sleep(1000/SPS);
		update();
		controller.Display(&window);
		for(int i = 0; i < players.size();i++)
			if(players[i].IsActive())
				players[i].Display(&window);
		window.OnRender();
	}
}

void Game::end()
{
	cout << "<Game> Ending Class" << endl;
}