#ifndef __LOCPLAYER
#define __LOCPLAYER

#include <iostream>

using  namespace std;

struct Pos
{
	int x;
	int y;
};

class LocalPlayer
{
	private:
		Pos pos;
	public:
		LocalPlayer();
		~LocalPlayer();
		void setPos(int, int);
		int getPosX();
		int getPosY();
		void Update();
};

#endif