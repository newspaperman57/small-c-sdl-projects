#ifndef __CONTROLLER
#define __CONTROLLER

#include "Player.h"
#include <windows.h>

#include <iostream>

using  namespace std;

class Controller: public Player
{
	private:
		
	public:
		Controller();
		~Controller();
		void Update();
};

#endif