#ifndef __OCTOTHORP
#define __OCTOTHORP

#include "Player.h"
#include "Controller.h"
#include "window.h"
#include "Socket.h"
#include "Address.h"
#include <iostream>
#include <vector>
#include <windows.h>

using namespace std;

struct PlayerData
{
	unsigned long IP;
	unsigned short ID;
};

class Game
{
	private:
		
		vector<Player> players;
		vector<PlayerData> playerData;
		Controller controller;
		Window window;
		Socket socket_in, socket_out;
		Address server;
		
		bool running, connected;
		int tPosX;
		int tPosY;
		int screenW;
		int screenH;
		
		void BroadcastPos();
		void UpdatePos(unsigned char buffer[]);
		
	public:
		Game();
		~Game();
		
		bool initialize();
		void end();
		
		void update();
		void run();
};

#endif
