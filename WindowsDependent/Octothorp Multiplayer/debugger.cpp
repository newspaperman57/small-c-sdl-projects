#include "Socket.h"
#include "Address.h"
#include <iostream>
#include <string>
#include <windows.h>
#include <vector>

#define CLIENTPORT		30000
#define SERVERPORT		25000
#define BUFFER_SIZE		256

#define ACK 			0x06	//Hexadecimal representation of 06
#define NAK 			0x15	//Hexadecimal representation of 21
#define JOIN 			0x10	//Hexadecimal representation of 16
#define DISCONNECT 		0x11	//Hexadecimal representation of 17
#define LIST 			0x12	//Hexadecimal representation of 18

using namespace std;

void server();
void client();

struct PlayerData
{
	unsigned long IP;
	unsigned short ID;
};

int main()
{
	string wanted;
	cout << "Should this simulate a Server(S) or Client(C): ";
	getline(cin, wanted);
	if(wanted == "S" || wanted == "s")
		server();
	else if(wanted == "C" || wanted == "c")
		client();
	return 0;
}

void client()
{
	string serverIP_str;
	cout << "Please write IP of the server: ";
	getline(cin, serverIP_str);
	Socket socket_in, socket_out;
	
	if(!Socket::Init() || !socket_in.Open(CLIENTPORT) || !socket_out.Open(CLIENTPORT+1))
		cout << "Could not open socket" << endl;
	
	socket_in.UnBind();
	socket_out.UnBind();
	Address server;
	Address sender;
	
	vector<PlayerData> playerData;
	
	//subdevide string
	int IP[4];
	unsigned short pos[4];
	pos[0] = serverIP_str.find('.');
	pos[1] = serverIP_str.find('.',pos[0]+1);
	pos[2] = serverIP_str.find('.',pos[1]+1);
	pos[3] = serverIP_str.find('\n',pos[2]+1);
	IP[0] = atoi(serverIP_str.substr(0,pos[0]).c_str());
	IP[1] = atoi(serverIP_str.substr(pos[0]+1,pos[1]).c_str());
	IP[2] = atoi(serverIP_str.substr(pos[1]+1,pos[2]).c_str());
	IP[3] = atoi(serverIP_str.substr(pos[2]+1,pos[3]).c_str());
	server = Address(IP[0],IP[1],IP[2],IP[3],SERVERPORT);
	char buffer[BUFFER_SIZE];
	cout << "Attempting to join server";
	char data[1] = {JOIN};
	socket_out.Send(server, data, sizeof(data));
	
	for(int i = 0; i < 20;i++)
	{
		Sleep(500);
		cout << ".";
		int bytes_read = socket_in.Receive(sender, buffer, sizeof(buffer));
		if(bytes_read > 0)
		{
			cout << endl;
			switch( buffer[0] )
			{
				case LIST:
				{
					cout << "Received LIST: " << endl;
					for(int i = 0; i < (bytes_read - 1)/6; i++)
					{
						cout << "ID " << (unsigned short)( ( buffer[ 5 + i*6 ] << 8 )|( buffer[ 6 + i*6 ] )) << ": \n" 
										<< (int)buffer[ 1 + i*6 ] << "."
										<< (int)buffer[ 2 + i*6 ] << "."
										<< (int)buffer[ 3 + i*6 ] << "."
										<< (int)buffer[ 4 + i*6 ] << endl;
					}
					break;
				}
				case JOIN:
				{
					if(buffer[1] == ACK)
					{
						cout << "Join accepted!" << endl;
					} 
					else if(buffer[1] == NAK)
					{
						cout << "Join refused!" << endl;
					}
					break;
				}
			}
		}
	}
}

void server()
{
	Socket socket_in, socket_out;
	Address address;
	
	if(!Socket::Init() || !socket_in.Open(SERVERPORT) || !socket_out.Open(SERVERPORT+1))
		cout << "Could not open socket" << endl;
	socket_in.UnBind();
	socket_out.UnBind();
	unsigned char buffer[BUFFER_SIZE];
	Address sender;
	int bytes_read;
	while(true)
	{
		memset (buffer,0,sizeof(buffer));
		bytes_read = 0;
		bytes_read = socket_in.Receive( sender, buffer, sizeof( buffer ) );
		if(bytes_read > 0) //Something was received!
		{
			switch(buffer[0])
			{
				case JOIN:
				{
					cout << (int)sender.GetA() << "." << (int)sender.GetB()<< "."  << (int)sender.GetC()<< "."  << (int)sender.GetD() << ": " << endl;
					cout << "JOIN: Answering with {JOIN, ACK, (2 Byte ID:) 2}..." << endl;
					sender.SetPort(CLIENTPORT);
					char outData[4] = {JOIN, ACK, (2 >> 8), ((2 << 8) >> 8) };
					socket_out.Send(sender, outData, sizeof(outData));
					
					break;
				}
				case DISCONNECT:
				{
					cout << (int)sender.GetA() << "."  << (int)sender.GetB()<< "."  << (int)sender.GetC()<< "."  << (int)sender.GetD() << ": " << endl;
					cout << "DISCONNECT: Answering with {DISCONNECT, ACK}...";
					sender.SetPort(CLIENTPORT);
					char outData[2] = {DISCONNECT, ACK};
					socket_out.Send(sender, outData, sizeof(outData));
					break;
				}
				case LIST:
				{
					if(buffer[1] == ACK)
					{
						cout << (int)sender.GetA() << "." << (int)sender.GetB() << "." << (int)sender.GetC() << "." << (int)sender.GetD() << ": " << endl;
						cout << "LIST|ACK: Won't answer...";
					}
					break;
				}
				case NAK:
				{
					cout << (int)sender.GetA() << "." << (int)sender.GetB() << "." << (int)sender.GetC() << "." << (int)sender.GetD() << ": " << endl;
					cout << "NAK: Won't answer...";
					break;
				}
				case ACK:
				{
					cout << (int)sender.GetA() << "." << (int)sender.GetB() << "." << (int)sender.GetC() << "." << (int)sender.GetD() << ": " << endl;
					cout << "ACK: Won't answer...";
					break;
				}
				default:
				{
					cout << (int)sender.GetA() << "." << (int)sender.GetB() << "." << (int)sender.GetC() << "." << (int)sender.GetD() << ": " << endl;
					cout << "Packet wasn't understood!: ";
					for(int i = 0; i < bytes_read;i++)
						cout << (int)buffer[i];
					break;
				}
			}
			cout << endl << "Waiting for input." << endl << endl;
		}
	}
}