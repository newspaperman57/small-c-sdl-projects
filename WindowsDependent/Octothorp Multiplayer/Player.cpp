#include "Player.h"

Player::Player()
{
	cout << "<Player> Initializing Player class" << endl;
	pos.x = 0;
	pos.y = 0;
	active = false;
}

Player::~Player()
{
	cout << "<Player> Destroying Player Class" << endl;
}

void Player::SetPos(int x, int y)
{
	pos.x = x;
	pos.y = y;
}

int Player::GetPosX()
{
	return pos.x;
}

void Player::Activate()
{
	pos.x = -1;
	pos.y = -1;
	active = true;
}

void Player::Deactivate()
{
	active = false;
}

bool Player::IsActive()
{
	return active;
}

int Player::GetPosY()
{
	return pos.y;
}

void Player::Display(Window* window)
{
	window->Write(pos.x, pos.y, "#");
}