#include "window.h"

HWND Window::GetHandle()
{
	return windowHandle;
}

void Window::Write(int x, int y, int color, string str)
{
	for(int i = 0; i < str.size(); i++)
	{
		screen[(y * screenSizeX) + x + i] = str[i];
		screenColor[(y * screenSizeX) + x + i] = color;
	}
}

void Window::Write(int x, int y, int color, int number)
{
	string str = std::to_string(number);
	for(int i = 0; i < str.size(); i++)
	{
		screen[(y * screenSizeX) + x + i] = str[i];
		screenColor[(y * screenSizeX) + x + i] = color;
	}
}

COORD Window::GetWindowPos()
{
	return {screenX, screenY};
}

void Window::Write(int x, int y, string str)
{
	for(int i = 0; i < str.size(); i++)
	{
		screen[(y * screenSizeX) + x + i] = str[i];
		screenColor[(y * screenSizeX) + x + i] = setColor;
	}
}

  void Window::Write(int x, int y, int number)
{
	string str = std::to_string(number);
	for(int i = 0; i < str.size(); i++)
	{
		screen[(y * screenSizeX) + x + i] = str[i];
		screenColor[(y * screenSizeX) + x + i] = setColor;
	}
}

void Window::GetMainMonitorRes(int &X, int &Y)
{
	RECT desktop;
	// Get a handle to the desktop window
	const HWND hDesktop = GetDesktopWindow();
	// Get the size of screen to the variable desktop
	GetWindowRect(hDesktop, &desktop);
	// The top left corner will have coordinates (0,0)
	// and the bottom right corner will have coordinates
	//(horizontal, vertical)
	X = desktop.right;
	Y = desktop.bottom;
}

bool Window::Move(int x, int y)
{
	if(x < 0 or y < 0)
	{
		return 0;
	}
	screenX = x;
	screenY = y;
	
	SetWindowPos( windowHandle, HWND_TOP, x, y, 0, 0, SWP_NOSIZE );
	return 1;
}

  bool Window::WriteMap(int x, int y, string str)
{
	if(	x - screenX < 0 || x - screenX > screenSizeX || y - screenY < 0 || y - screenY > screenSizeY)
	{
		return false;
	}
	Write(x - screenX, y - screenY, str);
}

bool Window::Resize(short int x, short int y)
{
	if(x < 0 or y < 0)
	{
		return 0;
	}
	screenSizeX = x;
	screenSizeY = y;
	
	string sx,sy,sysCommand;
	sx = std::to_string(x);
	sy = std::to_string(y);
	sysCommand = "mode " + sx + "," + sy;
	system(sysCommand.c_str());   //Set mode to ensure window does not exceed buffer size
	SMALL_RECT WinRect = {0, 0, x, y};   //New dimensions for window in 8x12 pixel chars
	SMALL_RECT* WinSize = &WinRect;
	SetConsoleWindowInfo(GetStdHandle(STD_OUTPUT_HANDLE), true, WinSize);   //Set new size for window
	return 1;
}

  void Window::SetTextColor(int color)
{
    setColor = color;
	//SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), color);
}

  bool Window::GotoXY(int x, int y)
{
	COORD pos = {x, y};
	HANDLE output = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleCursorPosition(output, pos);
	return 1;
}

void Window::RemoveCursor()
{
	CONSOLE_CURSOR_INFO info;
	HANDLE hOutput = GetStdHandle (STD_OUTPUT_HANDLE);
	info.bVisible = FALSE;
	info.dwSize = 1;
	SetConsoleCursorInfo(hOutput,&info);
}

  void Window::OnRender()
{
	GotoXY(0,0);
	cout << screen;
	InitScreen();
}

  void Window::InitScreen()
{
	screen = "";
	for(int i = 0; i < (screenSizeX * screenSizeY)-1;i++)
		screen += " ";
}

Window::Window()
{
	#define MY_BUFSIZE 1024
	char pszNewWindowTitle[MY_BUFSIZE];
	char pszOldWindowTitle[MY_BUFSIZE];
	GetConsoleTitle(pszOldWindowTitle, MY_BUFSIZE);
	wsprintf(pszNewWindowTitle,"%d/%d", GetTickCount(), GetCurrentProcessId());
	SetConsoleTitle(pszNewWindowTitle);
	Sleep(40);// Ensure window title has been updated.
	windowHandle=FindWindow(NULL, pszNewWindowTitle);
	SetConsoleTitle(pszOldWindowTitle);
}