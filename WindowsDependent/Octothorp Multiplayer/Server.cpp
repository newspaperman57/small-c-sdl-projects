#include <iostream>
#include <vector>

#include "Socket.h"
#include "Address.h"

#define CLIENTPORT		30000
#define SERVERPORT		25000
#define BUFFER_SIZE		256

#define ACK 			0x06	//Hexadecimal representation of 06
#define NAK 			0x15	//Hexadecimal representation of 21
#define JOIN 			0x10	//Hexadecimal representation of 16
#define DISCONNECT 		0x11	//Hexadecimal representation of 17
#define LIST 			0x12	//Hexadecimal representation of 18

#define SPS				5		//Steps per second

using namespace std;

struct PlayerData
{
	long IP;
	short ID;
};

void NetworkLog(Address sender, int bytes, string log)
{
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 0x0F);//White
	cout << log << endl;
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 0x08);//Grey
	cout << bytes << " bytes" << endl; 
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 0x06);//Yellow
	cout << (short)sender.GetA() << "." << (short)sender.GetB() << "." << (short)sender.GetC() << "." << (short)sender.GetD() << ":" << sender.GetPort() << endl;
	cout << endl;
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 0x07);//Standard
}

bool SendPlayerList(vector<PlayerData> playerList, Socket *socket, Address sender)
{
	
	
	/*unsigned char data[1 + playerList.size()*6];
	data[0] = LIST;
	for(int i = 0; i < playerList.size(); i++)
	{
		for(int j = 3; j >= 0; j--)
			data[1 + i*4 + j] = playerList[i].IP >> 8*j;
		for(int j = 1; j >= 0; j--)
			data[1 + i*4 + 4 + j] = playerList[i].ID >> 8*j;
	}
	NetworkLog(Address(127,0,0,1,SERVERPORT), sizeof(data), "Server: List update");
	
	for(int i = 0; i < playerList.size();i++)
		socket.Send( Address(playerList[i].IP, CLIENTPORT), data, sizeof(data));
	return true;
	
	/*
	4 bytes IP + 2 bytes ID
	
	
	*/
	
	unsigned char data[1 + playerList.size()*6];
	data[0] = LIST;
	for(int i = 0; i < playerList.size(); i++)
	{
		for(int j = 0;j < 4;j++)
			data[1+j+i*6] = playerList[i].IP >> 8*(3-j);
		for(int j = 4;j < 6;j++)
			data[1+j+i*6] = playerList[i].ID >> 8*(5-j);
	}
	NetworkLog(Address(127,0,0,1,SERVERPORT), sizeof(data), "Server: List update");
	for(int i = 0; i < playerList.size();i++)
		socket->Send(Address(playerList[i].IP, CLIENTPORT), data, sizeof(data));
	return true;
}

int main()
{
	Socket socket_in, socket_out;
	Address address;
	int nPlayers = 0;
	int maxPlayers;
	short playerID = 0;
	vector<PlayerData> playerList;
	
	if(!Socket::Init() || !socket_in.Open(SERVERPORT) || !socket_out.Open(SERVERPORT+1))
		cout << "Could not open socket" << endl;
	socket_in.UnBind();
	socket_out.UnBind();
	
	cout << "Please choose max amount of players(1-99) :";
	string maxPlayers_str;
	getline(cin,maxPlayers_str);
	maxPlayers = atoi(maxPlayers_str.c_str());
	cout << "Server started" << endl
		 << "Awaiting requests" << endl;
	
	
	unsigned char buffer[BUFFER_SIZE];
	Address sender;
	int bytes_read;
	while(true)
	{
		if( GetAsyncKeyState(VK_ESCAPE) != 0 )
			break;
		
		buffer[0] = 0;
		bytes_read = 0;
		bytes_read = socket_in.Receive( sender, buffer, sizeof( buffer ) );
		if(bytes_read > 0)
		{
			switch(buffer[0])
			{
				case JOIN:	//Decide whether to ACK or NAK the new player, if ACK then add and update player list
				{
					NetworkLog(sender, bytes_read, "Client: Join request");
					//Make checks for if player is already in the game
					if(nPlayers < maxPlayers)
					{
						//Send ACK
						short temp1 = playerID;
						short temp2 = (playerID << 8);
						unsigned char data[4] = {JOIN,ACK,(temp1 >> 8),(temp2 >> 8)};
						sender.SetPort(CLIENTPORT);
						
						socket_out.Send( sender, data, sizeof(data));
						NetworkLog(Address(127,0,0,1,SERVERPORT), sizeof(data), "Server: Join acknowledged");
						
						//Update list
						PlayerData temp = {(long)sender.GetAddress(), playerID};
						playerList.push_back(temp);
						
						// Send List to people4
						SendPlayerList(playerList,/*POINTERS ARE*/&/*MATERIAL OF THE GODS*/socket_out,sender);
						//Increment nPlayers
						nPlayers++;
						playerID++; // This is badly designed, because when 3 players join and the 2. quits, de ID is empty, but will stay unused until restart
						break;
					}
					else
					{
						//Send NAK
						NetworkLog(Address(127,0,0,1,SERVERPORT), 2, "Server: Join not acknowledged");
						unsigned char data[1] = {NAK};
						sender.SetPort(CLIENTPORT);
						socket_out.Send( sender, data, sizeof(data));
						break;
					}
					break;
				}
				case DISCONNECT:	//Send ACK back for recieved disconnect and update player list
				{
					NetworkLog(sender, bytes_read, "Client: Disconnect request");
					//Update list
					for(int i = 0; i < playerList.size(); i++)
					{
						if(playerList[i].IP == sender.GetAddress())
						{
							//Send ACK
							NetworkLog(Address(127,0,0,1,SERVERPORT), 2, "Server: Disconnect acknowledged");
							unsigned char data[2] = {DISCONNECT,ACK};
							sender.SetPort(CLIENTPORT);
							socket_out.Send( sender, data, sizeof(data));
							//erase Player from list
							playerList.erase(playerList.begin()+i);
							break;
						}
					}
					
					// Send List to people
					SendPlayerList(playerList,/*POINTERS ARE*/&/*MATERIAL OF THE GODS*/socket_out,sender);
					
					//Decrement nPlayers
					nPlayers--;
					break;
				}
				case LIST:	//Client sends ACK for the received list
				{
					//Acknowledge that the information got through
					if(buffer[1] == ACK)
						NetworkLog(sender, bytes_read, "Client: List recieved");
						
					break;
				}
			}
		}
		Sleep(1000/SPS);
	}
	//Maybe here there should be a call to update all lists to having no members
	Socket::Quit();
	return 0;
}