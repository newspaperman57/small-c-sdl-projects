#ifndef __PLAYER
#define __PLAYER

#include "window.h"
#include <iostream>

using  namespace std;

struct Pos
{
	int x;
	int y;
};

class Player
{
	private:
		bool active;
	protected:
		Pos pos;
	public:
		Player();
		~Player();
		bool IsActive();
		void Activate();
		void Deactivate();
		void SetPos(int, int);
		int GetPosX();
		int GetPosY();
		void Display(Window* window);
		
		//Add functionality to read ID to short and 2 chars format
		short ID;
};

#endif