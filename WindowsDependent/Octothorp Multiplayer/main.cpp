#include "octothorp.h"
#include <iostream>

using namespace std;

int main()
{
	cout << "<Main> Main function beginning!" << endl;
	Game game;
	if(game.initialize())
	{
		game.run();
	}
	else
	{
		cout << "<Main> Error while initializing! Ending." << endl;
		return -1;
	}
	game.end();
	cout << "<Main> Main function ending!" << endl;
	return 0;
}