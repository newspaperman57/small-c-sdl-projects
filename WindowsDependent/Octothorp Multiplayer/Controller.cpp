#include "Controller.h"

Controller::Controller()
{
	cout << "<Controller> Initializing Class" << endl;
}

Controller::~Controller()
{
	cout << "<Controller> Destroying Class" << endl;
}

void Controller::Update()
{
	if( GetAsyncKeyState(VK_UP) != 0)
	{
		pos.y --;
	}
	if( GetAsyncKeyState(VK_RIGHT) != 0)
	{
		pos.x ++;
	}
	if( GetAsyncKeyState(VK_LEFT) != 0)
	{
		pos.x --;
	}
	if( GetAsyncKeyState(VK_DOWN) != 0)
	{
		pos.y ++;
	}
	
}

