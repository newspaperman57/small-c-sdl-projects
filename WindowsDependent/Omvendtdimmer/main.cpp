#include <iostream>
#include <conio.h>
#include <stdio.h>
#include <windows.h>

#include <cstdlib>
#include <winuser.h>
#include <cmath>
#include <iomanip>
#include <complex>
#include <string>


using namespace std;

void toClipboard(HWND hwnd, const std::string &s);

int main()
{
	int c[2064];
	bool trueis = true;
	int count = 0;
	cout << "Write the sentence here.\nFinish with a press on the ENTER button.\nThis will copy the output to your Clipboard!:\n";
	while (trueis)
	{
		c[count] = getch();
		if (c[count] == 13)
			trueis = false;
		putchar(c[count]);
		count++;
	}
	
	count--;
	count--;
	string AAA;
	cout <<"\n\n";
	while (count > -1)
	{
		putchar(c[count]);
		AAA += c[count]; // Puts every character into a string
		count--;
	}
	
	// Puts the string made before into the clipholder!
	size_t len = strlen(AAA.c_str());
	HWND hwnd = GetDesktopWindow();
	toClipboard(hwnd, AAA);
	
	cout <<"\n\nThis has been put in your clipboard!\n\n";
	system("pause");
}

void toClipboard(HWND hwnd, const std::string &s){
	OpenClipboard(hwnd);
	EmptyClipboard();
	HGLOBAL hg=GlobalAlloc(GMEM_MOVEABLE,s.size()+1);
	if (!hg){
		CloseClipboard();
		return;
	}
	memcpy(GlobalLock(hg),s.c_str(),s.size()+1);
	GlobalUnlock(hg);
	SetClipboardData(CF_TEXT,hg);
	CloseClipboard();
	GlobalFree(hg);
}