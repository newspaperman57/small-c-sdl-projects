#include "Intro.h"

enum colors {Y=6,
				B=0,
				W=15,
				R=12,
				r=4};

Intro::Intro()
{
	/*
	"
	\n
	 #------------------------#\n
	 |                        |\n
	 |          Play          |\n
	 |                        |\n
	 |       Highscores       |\n
	 |                        |\n
	 |          Exit          |\n
	 |                        |\n
	 #------------------------#\n
	"
	*/
	menuScreen = "\n #------------------------#\n |                        |\n |          Play          |\n |                        |\n |       Highscores       |\n |                        |\n |          Exit          |\n |                        |\n #------------------------#\n";
}
				
void Intro::init()
{
	running = true;
	
	//Create highscore file
	ofstream ofs;
	ofs.open("highscores", ofstream::out);
	ofs.close();
	
	setConsoleWindowSize(31,33);
	clear();
	splash();
	cout << flush;
	do event(); while (keystate[SPACE] != 1);
	cout << flush;
	clear();
	setConsoleWindowSize(28,11);
	loop();
}

void Intro::splash()
{
/*
	string white01 = readFile("Splash\\white");
	string black02 = readFile("Splash\\black");
	string darkRed03 = readFile("Splash\\darkRed");
	string lightRed04 = readFile("Splash\\lightRed");
	string yellow05 = readFile("Splash\\yellow");
*/
/*
	white01 ="
             `......`           \n
          `............`        \n
         ................       \n
       `..................`     \n
      .....................`    \n
     `......................`   \n
     .+@@:..........'@@+.....   \n
    .@,..@#........@:..+@,....  \n
   `+:..@@@'......@...`@@@....` \n
   .@...@@@@.....;;...`@@@+.... \n
   .#....',#.....@.....,+ @.... \n
  .,+......',....@........@....`\n
  ..#......+.....@........@.....\n
  ..@@@@@@@@.....+@@@@@@@@#.....\n
  ..............................\n
  ..............................\n
  ...,,,,,,,,,,,,,,,,,,,,,......\n
  ...+@##################@:.....\n
  ....@''''''''''''''''''++....`\n
   ...@'''''''''''''''''''@.... \n
   ...++''''''''''''''''''#.... \n
   `...@'''''''''''''''''+'...` \n
    ...#+''''''''';;'''''@....  \n
    `...@'''''';`````.''+#...   \n
     ...,@'''';````````+@...`   \n
      ...:@'''````````+@....    \n
       ...,@+:``````:@+...`     \n
         ...'@@''+@@+....       \n
          `....,,,.....`        \n
             .......`           \n
	"
	black02 ="
             `......`           \n
          `............`        \n
         ................       \n
       `..................`     \n
      .....................`    \n
     `......................`   \n
     .+@@:..........'@@+.....   \n
    .@,  @#........@:  +@,....  \n
   `+:  @@@'......@   `@@@....` \n
   .@   @@@@.....;;   `@@@+.... \n
   .#    ',#.....@     ,+ @.... \n
  .,+      ',....@        @....`\n
  ..#      +.....@        @.....\n
  ..@@@@@@@@.....+@@@@@@@@#.....\n
  ..............................\n
  ..............................\n
  ...,,,,,,,,,,,,,,,,,,,,,......\n
  ...+@##################@:.....\n
  ....@''''''''''''''''''++....`\n
   ...@'''''''''''''''''''@.... \n
   ...++''''''''''''''''''#.... \n
   `...@'''''''''''''''''+'...` \n
    ...#+''''''''';;'''''@....  \n
    `...@'''''';`````.''+#...   \n
     ...,@'''';````````+@...`   \n
      ...:@'''````````+@....    \n
       ...,@+:``````:@+...`     \n
         ...'@@''+@@+....       \n
          `....,,,.....`        \n
             .......`           \n
	"
	darkRed03 ="
                                \n
                                \n
                                \n
                                \n
                                \n
                                \n
                                \n
                                \n
                                \n
                                \n
                                \n
                                \n
                                \n
                                \n
                                \n
                                \n
                                \n
                                \n
       ''''''''''''''''''       \n
       '''''''''''''''''''      \n
        ''''''''''''''''''      \n
        '''''''''''''''''       \n
         ''''''''';;'''''       \n
         '''''';`````.''        \n
          '''';````````         \n
           '''````````          \n
             :``````:           \n
                                \n
                                \n
                                \n
	"
	lightRed04 ="
                                \n
                                \n
                                \n
                                \n
                                \n
                                \n
                                \n
                                \n
                                \n
                                \n
                                \n
                                \n
                                \n
                                \n
                                \n
                                \n
                                \n
                                \n
                                \n
                                \n
                                \n
                                \n
                                \n
               ;`````.          \n
              ;````````         \n
             '````````          \n
             :``````:           \n
                                \n
                                \n
                                \n
	"
	yellow05 ="
             `......`           \n
          `............`        \n
         ................       \n
       `..................`     \n
      .....................`    \n
     `......................`   \n
     .   :..........    .....   \n
    .      ........      ,....  \n
   `+      '......        ....` \n
   .        .....          .... \n
   .        .....          .... \n
  .,        ,....          ....`\n
  ..        .....          .....\n
  ..        .....          .....\n
  ..............................\n
  ..............................\n
  ...,,,,,,,,,,,,,,,,,,,,,......\n
  ...+                     .....\n
  ....                     ....`\n
   ...                     .... \n
   ...                     .... \n
   `...                    ...` \n
    ...                   ....  \n
    `...                  ...   \n
     ...,                ...`   \n
      ...               ....    \n
       ...,            ...`     \n
         ...         ....       \n
          `....,,,.....`        \n
             .......`           \n
	"
*/
/*
             `......`           \n
          `............`        \n
         ................       \n
       `..................`     \n
      .....................`    \n
     `......................`   \n
     .+@@:..........'@@+.....   \n
    .@,  @#........@:  +@,....  \n
   `+:  @@@'......@   `@@@....` \n
   .@   @@@@.....;;   `@@@+.... \n
   .#    ',#.....@     ,+ @.... \n
  .,+      ',....@        @....`\n
  ..#      +.....@        @.....\n
  ..@@@@@@@@.....+@@@@@@@@#.....\n
  ..............................\n
  ..............................\n
  ...,,,,,,,,,,,,,,,,,,,,,......\n
  ...+@##################@:.....\n
  ....@''''''''''''''''''++....`\n
   ...@'''''''''''''''''''@....\n
   ...++''''''''''''''''''#....\n
   `...@'''''''''''''''''+'...`\n
    ...#+''''''''';;'''''@....\n
    `...@'''''';`````.''+#...\n
     ...,@'''';````````+@...`\n
      ...:@'''````````+@....\n
       ...,@+:``````:@+...`\n
         ...'@@''+@@+....\n
          `....,,,.....`\n
             .......`  \n
			 32X30
*/
	/*
	white01;
	black02;
	darkRed03;
	lightRed04;
	yellow05;
	*/
	
	
	
	/*
	setTextColor(15);
	for(int i = 0; i < white01.size(); i++)
	{
		gotoxy((i%30)-(i/30), i/30);
		  //if(white01[i] != (char)32)
			cout << white01[i];
		Sleep(1);
	}
	
	setTextColor(3);
	for(int i = 0; i < black02.size(); i++)
	{
		gotoxy((i%30)-(i/30), i/30);
		if(i==30*11+15 || i==30*11+30)
			cout << "0";
		else if(black02[i] != (char)32)
			cout << black02[i];
		Sleep(1);
	}
	/*
	setTextColor(12);
	for(int i = 0; i < white01.size(); i++)
	{
		gotoxy((i%30)-i/30, i/30);
		if(darkRed03[i] != (char)32 || (char)10)
		cout << darkRed03[i];
		Sleep(3);
	}
	setTextColor(4);
	for(int i = 0; i < white01.size(); i++)
	{
		gotoxy((i%30)-i/30, i/30);
		if(lightRed04[i] != (char)32 || (char)10)
		cout << lightRed04[i];
		Sleep(3);
	}
	setTextColor(6);
	for(int i = 0; i < white01.size(); i++)
	{
		gotoxy((i%30)-i/30, i/30);
		if(yellow05[i] != (char)32 || (char)10)
		cout << yellow05[i];
		Sleep(3);
	}*/
	
	/*
	string awsome = "AwsomeFaceAwsomeFaceAwsomeFace";
	for(int i = 0; i < 35; i++)
	{
		gotoxy(i%10,i/10);
		setTextColor(4);
		cout << awsome[i];
		if(i>=5)
		{
			setTextColor(6);
			gotoxy((i-5)%10,(i-5)/10);
			cout << awsome[i-5];
		}
		Sleep(500);
	}
	cout << endl;
	*//*
	for(int i = 0; i < white01.size()+(30*2*5); i++)
	{	
		
		if(i >= 30*2*0 && i < white01.size()+(30*2*0))
		{
			setTextColor(15);
			gotoxy(((i%30)-(i-30*2*0))/30, ((i-(30*2*0))/30));
			cout << white01[i-(30*2*0)];
		}
		
		if(i > 30*2*1 && i < white01.size()+(30*2*1))
		{
			setTextColor(1);
			gotoxy(((i%30)-(i-30*2*1))/30, ((i-(30*2*1))/30)-30);
			cout << black02[i-(30*2*1)];
		}
		/*
		if(i > 30*2*2 && i < white01.size()+(30*2*2))
		{
			setTextColor(12);
			gotoxy(((i%30)-(i-30*2*2))/30, (i-(30*2*2))/30);
			cout << darkRed03[i-(30*2*2)];
		}
		
		if(i > 30*2*3 && i < white01.size()+(30*2*3))
		{
			setTextColor(4);
			gotoxy(((i%30)-(i-30*2*3))/30, (i-(30*2*3))/30);
			cout << lightRed04[i-(30*2*3)];
		}
		
		if(i >= 30*2*4 && i < white01.size()+(30*2*4))
		{
			setTextColor(6);
			gotoxy(((i%30)-(i-30*2*4))/30, (i-(30*2*4))/30);
			cout << yellow05[i-(30*2*4)];
		}
		
		Sleep(10);
	}
	//*/
	
	vector<int> color = {Y,B,Y,B,Y,B,W,B,Y,B,W,B,Y,B,W,B,Y,B,W,B,Y,B,W,B,Y,B,W,B,Y,B,W,B,Y,B,W,B,Y,B,W,B,Y,B,W,B,Y,B,W,B,Y,B,W,B,Y,B,Y,B,Y,B,Y,B,R,B,Y,B,R,B,Y,B,R,B,Y,B,R,B,Y,B,R,B,Y,B,R,r,R,B,Y,B,R,r,B,Y,B,R,r,B,Y,B,r,B,Y,B,Y,R};
	vector<string> text = {
	"           `......`           \n        `............`        \n       ................       \n     `..................`     \n    .....................`    \n   `......................`   \n   .","+@@",":..........","'@@+",".....   \n  .","@",",..","@#","........","@",":..","+@",",....  \n `","+",":..","@@@","'......","@","...`","@@@","....` \n .","@","...","@@@@",".....",";;","...`","@@@+",".... \n .","#","....","',#",".....","@",".....,","+,@",".... \n.,","+","......","'",",....","@",",.......","@","....`\n..","#","......","+",".....","@","........","@",".....\n..","@@@@@@@@",".....","+@@@@@@@@#",".....\n..............................\n..............................\n...,,,,,,,,,,,,,,,,,,,,,......\n...","+@##################@",":.....\n....","@","''''''''''''''''''","++","....`\n ...","@","'''''''''''''''''''","@",".... \n ...","++","''''''''''''''''''","+",".... \n `...","@","'''''''''''''''''","+'","...` \n  ...","#+","''''''''';;'''''","@","....  \n  `...","@","'''''';","`````.","''","+#","...   \n   ...,","@","'''';","````````","+@","...`   \n    ...",":@","'''","````````","+@","....    \n     ...,","@+:","``````",":@+","...`     \n       ...","'@@''+@@+","....       \n        `....,,,.....`        \n           .......`","\n\n  Press Spacebar To Continue"};
	
	for(int i = 0; i < color.size(); i++)
	{
		setTextColor(color[i]);
		for(int j = 0; j < text[i].size(); j++)
		{
			cout << text[i][j];
			Sleep(3);
		}
	}
	cout << endl;
	
	setTextColor(15);
	
}

void Intro::startGame()
{
	fflush(stdin);
	getchar();
	cout << flush;
	Game snake;
	snake.go();
	clear();
	setConsoleWindowSize(28,11);
}

void Intro::highscore()
{
	//while(keystate[])
}

void Intro::event()
{
	if(GetAsyncKeyState(VK_ESCAPE))
		running = false;
	
	if(GetAsyncKeyState(VK_RETURN))
		keystate[ENTER] = 1;
	else
		keystate[ENTER] = 0;

	if(GetAsyncKeyState(VK_UP))
		keystate[UP] = 1;
	else
		keystate[UP] = 0;
	
	if(GetAsyncKeyState(VK_DOWN))
		keystate[DOWN] = 1;
	else
		keystate[DOWN] = 0;
	if(GetAsyncKeyState(VK_SPACE))
		keystate[SPACE] = 1;
	else
		keystate[SPACE] = 0;
}

void Intro::loop()
{
	menuState = 0;
	while(running)
	{
		event();
		update();
		if(keystate[ENTER] && menuState == EXIT)
			break;
		draw();
		
		Sleep(1000/5);
	}
	fflush(stdin);
	getchar();
}
///////////////////////////////////////////////////
void Intro::update()
{
	if(keystate[UP])
	{
		if(menuState != PLAY)
		menuState--;
	}
	else if(keystate[DOWN])
	{
		if(menuState != EXIT)
			menuState++;
	}
	
	if(keystate[ENTER])
	{
		if(menuState == PLAY )
			startGame();
		if(menuState == SCORE )
			highscore();
		if(menuState == EXIT )
			running = false;
	}
		
	
}
///////////////////////////////////////////////////
void Intro::draw()
{
	gotoxy(0,0);
	cout << menuScreen;
	
	switch(menuState)
	{
		case EXIT: gotoxy(11,7); cout << ">"; gotoxy(16,7); cout << "<"; break;
		case SCORE: gotoxy(8,5); cout << ">"; gotoxy(19,5); cout << "<"; break;
		case PLAY: gotoxy(11,3); cout << ">"; gotoxy(16,3); cout << "<"; break;
	}
}