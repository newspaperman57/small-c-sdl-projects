#include "Map.h"

void Map::loadMap(string path)
{
	string buffer;
	rect dimensions;
	int texture;
	bool deadly;
	
	ifstream file;
	
	file.open( path.c_str() );
	//Read Header
	file >> buffer; //WIDTH
	file >> dimen.w;
	file >> buffer; //HEIGHT
	file >> dimen.h;
	for(int i = 0; i < dimen.h; i++)
	{
		for(int j = 0; j < dimen.w; j++)
		{
			file >> buffer;
			if( atoi(buffer.c_str()) == 1)	//If player spawn
			{
				playerSpawn = {j,i};
				buffer = "0";
			}
			Tile tile;
			if( atoi(buffer.c_str()) < 0)
				tile.deadly = true;
			else
				tile.deadly = false;
			tile.texture = atoi( buffer.c_str() );
			tiles.push_back(tile);
		}
	}
	
	file.close();
}

pos2d Map::getPlayerSpawn()
{
	return playerSpawn;
}

Tile Map::getTile(int x, int y)
{
	return tiles[ (y*dimen.w)+x ];
}

rect Map::getDimen()
{
	return dimen;
}

void Map::setPlayerSpawn(pos2d spawn)
{
	playerSpawn = spawn;
}

void Map::setDimen(rect dim)
{
	dimen = dim;
}