#ifndef __POINT
#define __POINT

#include <time.h>
#include <cstdlib>
#include <vector>

#include "nolib.h"

#include "Tile.h"

using namespace std;

class Point
{
public:
	Point(int, int, vector<Tile>);
	void init();
	pos2d getPos();
	int texture = 3;
private:
	
	pos2d pos;
};

#endif