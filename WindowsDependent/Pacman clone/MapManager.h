#ifndef __MAPMANAGER
#define __MAPMANAGER

#include <vector>
#include <sstream>
#include <iostream>

#include "nolib.h"

#include "Map.h"
#include "Tile.h"

using namespace std;

class MapManager
{
public:
	MapManager(string);
private:
	vector<Map> maps;
	void loadMaps(string);
};

#endif