#ifndef __INTRO
#define __INTRO

#include <windows.h>
#include <string>
#include <iostream>
#include <stdlib.h>

#include "nolib.h"

#include "Game.h"

using namespace std;

class Intro
{
public:
	Intro();
	void init();
private:
	void splash();
	void startGame();
	void highscore();
	void menu();
	
	enum keys {UP = 0, 
			   DOWN = 1, 
			   ENTER = 2,
			   SPACE = 3
			   };
	enum menu {PLAY = 0,
			   SCORE = 1,
			   EXIT = 2	
			   };
	int menuState;
	bool running;
	int keystate[256];
	
	string menuScreen;
	
	void loop();
	void event();
	void update();
	void draw();
};

#endif

