#ifndef __GAME
#define __GAME


#include <vector>
#include <windows.h>
#include <iostream>

#include "Map.h"
#include "Player.h"
#include "Point.h"

using namespace std;

class Game
{
public:
	~Game();
	void init();
	void go();
	
private:
	enum keys {UP = 0, 
			   DOWN = 1, 
			   LEFT = 2, 
			   RIGHT = 3
			   };
			   
	int keystate[4];
	int width, height,
		score;
	vector<char> display;
	int FPS;
	bool running;
	
	
	Map map;
	Player player;
	Point* point;

	void loop();
	void event();
	void update();
	void draw();
	
};

#endif