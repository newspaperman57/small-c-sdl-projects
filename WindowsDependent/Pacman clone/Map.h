#ifndef __MAP
#define __MAP

#include <string>
#include <vector>
#include <fstream>
#include <cstdlib>

#include "nolib.h"
#include "Tile.h"

using namespace std;

class Map
{
public:
	Tile getTile(int,int);
	void loadMap(string);
	pos2d getPlayerSpawn();
	rect getDimen();
	vector<Tile> tiles;
	void setPlayerSpawn(pos2d);
	void setDimen(rect);
private:

	pos2d playerSpawn;
	rect dimen;
	
	
};

#endif