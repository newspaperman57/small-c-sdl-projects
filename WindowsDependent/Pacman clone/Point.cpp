#include "Point.h"


Point::Point(int width, int height, vector<Tile> tiles)
{
	
	pos.x = (rand() % (width-2)) +1;
	pos.y = (rand() % (height-2)) +1;
	while(tiles[(pos.y*width)+pos.x].deadly)
	{
		pos.x = (rand() % (width-2)) +1;
		pos.y = (rand() % (height-2)) +1;
	}
}

void Point::init()
{
	srand(time(NULL));
}

pos2d Point::getPos()
{
	return pos;
}