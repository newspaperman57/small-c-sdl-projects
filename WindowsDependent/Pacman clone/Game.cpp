#include "Game.h"

Game::~Game()
{
	delete point;
}

void Game::init()
{
	FPS = 2;
	score = 0;
	
	map.loadMap("map01");
	setConsoleWindowSize(map.getDimen().w+2,map.getDimen().h+2);
	player.pos = map.getPlayerSpawn();
	point->init();
	point = new Point(map.getDimen().w, map.getDimen().h, map.tiles);
	
	running = true;
}
///////////////////////////////////////////////////
void Game::go()
{
	init();
	loop();
}
///////////////////////////////////////////////////
void Game::loop()
{
	while(running)
	{
		event();
		update();
		if(!running)
			break;
		draw();
		
		Sleep(1000/FPS);
	}
}
///////////////////////////////////////////////////
void Game::event()
{
	if(GetAsyncKeyState(VK_ESCAPE))
		running = false;


	if(GetAsyncKeyState(VK_UP))
		keystate[UP] = 1;
	else
		keystate[UP] = 0;
		
	if(GetAsyncKeyState(VK_RIGHT))
		keystate[RIGHT] = 1;
	else
		keystate[RIGHT] = 0;
	
	if(GetAsyncKeyState(VK_DOWN))
		keystate[DOWN] = 1;
	else
		keystate[DOWN] = 0;
	
	if(GetAsyncKeyState(VK_LEFT))
		keystate[LEFT] = 1;
	else
		keystate[LEFT] = 0;
}
///////////////////////////////////////////////////
void Game::update()
{
	if(keystate[UP] && player.direction != DOWN)
	{
		player.pos.y--;
		player.direction = UP;
	}
	else if(keystate[DOWN] && player.direction != UP)
	{
		player.pos.y++;
		player.direction = DOWN;
	}
	else if(keystate[LEFT] && player.direction != RIGHT)
	{
		player.pos.x--;
		player.direction = LEFT;
	}
	else if(keystate[RIGHT] && player.direction != LEFT)
	{
		player.pos.x++;
		player.direction = RIGHT;
	}
	else if(player.direction == UP)
		player.pos.y--;
	else if(player.direction == DOWN)
		player.pos.y++;
	else if(player.direction == LEFT)
		player.pos.x--;
	else if(player.direction == RIGHT)
		player.pos.x++;
		
	
	if(map.getTile(player.pos.x,player.pos.y).deadly)
	{
		cout << "Head to the wall" << endl;
		running = false;
	}
	else if(player.pos.x == point->getPos().x && player.pos.y == point->getPos().y)
	{
		//new point
		point = new Point(map.getDimen().w, map.getDimen().h, map.tiles);
		score++;
		FPS++;
	}
}
///////////////////////////////////////////////////
void Game::draw()
{
	//Buffering
	string disp;
	disp += "\n ";
	for(int i = 0; i < map.getDimen().h; i++)
	{
		for(int j = 0; j < map.getDimen().w; j++)
		{
			if( j == player.pos.x && i == player.pos.y )
				disp += (char)2;
			else if( j == point->getPos().x && i == point->getPos().y )
				disp += (char)point->texture;
			else switch(map.getTile(j,i).texture)
			{
				case -7: disp += (char)186; break;
				case -6: disp += (char)205; break;
				case -5: disp += (char)188; break;
				case -4: disp += (char)200; break;
				case -3: disp += (char)187; break;
				case -2: disp += (char)201; break;
				case -1: disp += (char)178; break;
				default: disp += (char)32; break;
			}
			/*
			else if(map.getTile(j,i).texture == -1)
				disp += (char)178;
			else if(map.getTile(j,i).texture == -2)
				disp += (char)201;
			else if(map.getTile(j,i).texture == -3)
				disp += (char)187;
			else if(map.getTile(j,i).texture == -4)
				disp += (char)200;
			else if(map.getTile(j,i).texture == -5)
				disp += (char)188;
			else if(map.getTile(j,i).texture == -6)
				disp += (char)205;
			else if(map.getTile(j,i).texture == -7)
				disp += (char)186;
			else
				disp += (char)32;
			*/
		}
		disp += "\n ";
	}
	
	//Displaying
	gotoxy(0,0);
	disp += "Score: " + to_string(score) + "  ";
	cout << disp;
}
///////////////////////////////////////////////////