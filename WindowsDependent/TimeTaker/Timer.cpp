#include <iostream>
#include <string>
#include "SDL.h"
#include <windows.h>

#undef main

using namespace std;

int main(int argc, char* argv[]) {
	SDL_Init(SDL_INIT_EVERYTHING);
	cout << "Testing..." << endl;
	
	int Stop;
	int shortest, longest;
	int times = 5; // how many times the loop should run
	int timer[times + 1];
	int Start = SDL_GetTicks(); // Starts time
	timer[0] = Start;
	for(int i = 0; i < times; i++) // Does nothing but increasing a value, 'times' times
	{
		system("cd ../Snake && compile.bat");
		timer[i + 1] = SDL_GetTicks() - timer[i];
	}
	Stop = SDL_GetTicks(); // Stops time
	cout << "DONE!" << endl;
	shortest = timer[0];
	longest = timer[0];
	for(int d = 1;d <= times;d++)
	{
		if ( (timer[d] - timer[d - 1]) < shortest )
			shortest = timer[d];
		else if ((timer[d] - timer[d - 1]) > longest)
			longest = timer[d];
	}
	
	for(int e = 0; e <= times;e++)
		cout << timer[e] << endl;
	
	if((Stop - Start) / times < 1000)
		cout << endl << "Took " << (Stop - Start) / 1.0f / times << " Milliseconds each." << endl;
	else
		cout << endl << "Took " << (Stop - Start) / 1000.0f / times << " Seconds each." << endl;
	cout << "Shortest: " << shortest << endl;
	cout << "Longest: " << longest << endl;
	cout << "That's " << 1000.0f/((Stop-Start)/times) << " Times a second!" << endl;
	cout << "Took " << ( Stop - Start ) / 1000.0f << " Seconds in all" << endl;
	system("pause");
	return 0;
}