#include <iostream>

#include <SDL/SDL.h>
#include <windows.h>
#include <math.h>

#define WIDTH 640
#define HEIGHT 480

using namespace std;

class Vector {
	public:
		Vector(float x_, float y_) {x = x_; y = y_;}
		float x,y;
		
		float magn() {return sqrt((x*x) + (y*y));}
		void norm() {float magni = this->magn(); x /= magni; y /= magni;}
		void operator *= (const float mult) {x*=mult;y*=mult;}
		void operator /= (const float devi) {x/=devi;y/=devi;}
		Vector operator + (const Vector &a) {return Vector(x+a.x,y+a.y);}
		Vector operator - (const Vector &a) {return Vector(x-a.x,y-a.y);}
};

void PutPixel(SDL_Surface *surface, Vector pos, Uint8 r = 255, Uint8 g = 255, Uint8 b = 255 )
{
	//The surface must be locked before calling this!
    Uint32 pixel = SDL_MapRGB(surface->format, r,g,b);
	int bpp = surface->format->BytesPerPixel;
    Uint8 *p = (Uint8 *)surface->pixels + (int)pos.y * surface->pitch + (int)pos.x * bpp;

    switch(bpp) {
    case 1:
        *p = pixel;
        break;

    case 2:
        *(Uint32 *)p = pixel;
        break;

    case 3:
        if(SDL_BYTEORDER == SDL_BIG_ENDIAN) {
            p[0] = (pixel >> 16) & 0xff;
            p[1] = (pixel >> 8) & 0xff;
            p[2] = pixel & 0xff;
        } else {
            p[0] = pixel & 0xff;
            p[1] = (pixel >> 8) & 0xff;
            p[2] = (pixel >> 16) & 0xff;
        }
        break;

    case 4:
        *(Uint32 *)p = pixel;
        break;
    }
}

void SwitchVal( int* a, int* b) {
	int temp = *a;
	*a = *b;
	*b = temp;
}


int drawLine(SDL_Surface* display, int x0, int y0, int x1, int y1)
{
	int dx = x1 - x0;
	int dy = y1 - y0;
	
	bool revert = false;
	
	if( dx < 0 )
	{
		int terpentin = x0;
		x0 = x1;
		x1 = terpentin;
		
		terpentin = y0;
		y0 = y1;
		y1 = terpentin;
		
		dy = y1 - y0;
		dx = x1 - x0;
	}
	
	if( dy < 0 )
	{
		dy *= -1;
		revert = true;
	}
	
	if( dy > dx )
	{
		int D = 2 * dy - dx;
		PutPixel(display, Vector(x0, y0), 100, 100, 100);
		int x = x0;
		int x2, y2;
		if(y0 > y1)
		{
			int terpentin = y0;
			y0 = y1;
			y1 = terpentin;
		}
		for( int y = y0 + 1; y < y1; y++)
		{
			if(D > 0)
			{
				x += 1;
				D += (2*dx-2*dy);
			} else {
				D += (2*dx);
			}
			
			if(revert)
			{
				y2 = y - (2 * (y - ((y0+y1)/2)));
			} else {
				y2 = y;
			}
			x2 = x;
			PutPixel(display, Vector(x2, y2), 100, 100, 100);
		}
		return 0;
	} else {
		int D = 2 * dy - dx;
		PutPixel(display, Vector(x0, y0), 100, 100, 100);
		int y = y0;
		int y2, x2;
		
		for( int x = x0 + 1; x < x1; x++)
		{
			if(D > 0)
			{
				y += 1;
				D += (2*dy-2*dx);
			} else {
				D += (2*dy);
			}
			
			if(revert)
			{
				y2 = y - (2 * (y - y0));
			} else {
				y2 = y;
			}
			x2 = x;
			PutPixel(display, Vector(x2, y2), 100, 100, 100);
		}
		return 0;
	}
}
#undef main

int main()
{
	if(SDL_Init(SDL_INIT_EVERYTHING) < 0) 
		return false;
	
	SDL_Surface* display = SDL_SetVideoMode(WIDTH, HEIGHT, 8, SDL_SWSURFACE);
	
	Vector follow(10, 10);
	Vector controlled(WIDTH/2, HEIGHT/2);
	
	
	while(true)
	{	
		//Input
		if( GetAsyncKeyState(VK_UP) != 0 )
			controlled.y -= 3;
		if( GetAsyncKeyState(VK_DOWN) != 0 )
			controlled.y += 3;
		if( GetAsyncKeyState(VK_LEFT) != 0 )
			controlled.x -= 3;
		if( GetAsyncKeyState(VK_RIGHT) != 0 )
			controlled.x += 3;
		
		//Update
		Vector dir = controlled - follow;
		dir.norm();
		follow = follow + dir;
		cout << "Magnitude: " << dir.magn() << endl;
		cout << "follow.x: " << follow.x << endl;
		cout << "follow.y: " << follow.y << endl;
		
		//Display
		SDL_FillRect(display, NULL, SDL_MapRGB(display->format, 0, 0, 0));
		drawLine(display, follow.x,follow.y, controlled.x,controlled.y);
		SDL_Flip(display);
		Sleep(10);
	}
	return 0;
}