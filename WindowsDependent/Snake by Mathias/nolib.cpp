#include "nolib.h"


void gotoxy(short int x, short int y) 
{ 
	COORD pos = {x, y};
	HANDLE output = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleCursorPosition(output, pos);
}
///////////////////////////////////////////////////
void removeCursor()
{
	CONSOLE_CURSOR_INFO info;
	HANDLE hOutput = GetStdHandle (STD_OUTPUT_HANDLE);
	// turn the cursor off
	info.bVisible = FALSE;
	info.dwSize = 1;
	SetConsoleCursorInfo(hOutput,&info);
}
///////////////////////////////////////////////////
string readFile(string fileName)
{
	string fReturn;
	string temp;

	ifstream f;
	f.open(fileName);
	while(!f.eof())
	{
		getline(f,temp);
		fReturn += temp + '\n';
	}
	return fReturn;
}
///////////////////////////////////////////////////
void setTextColor(int color)
{
	HANDLE consoleHandle = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(consoleHandle, color);
}
///////////////////////////////////////////////////
void clear()
{
	system("cls");
}
//////////////////////////////////////////////////
/*
void setConsoleWindowSize(short int x, short int y)
{
	string sx,sy,sysCommand;
	sx = to_string(x);
	sy = to_string(y);
	sysCommand = "mode " + sx + "," + sy;
	system(sysCommand.c_str());   //Set mode to ensure window does not exceed buffer size
	SMALL_RECT WinRect = {0, 0, x, y};   //New dimensions for window in 8x12 pixel chars
	SMALL_RECT* WinSize = &WinRect;
	SetConsoleWindowInfo(GetStdHandle(STD_OUTPUT_HANDLE), true, WinSize);   //Set new size for window

}*/
