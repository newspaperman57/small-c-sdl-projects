#include <iostream>
#include <windows.h>
#include <string>
#include <time.h>
#include <stdlib.h>

#include "nolib.h"

using namespace std;

//////////////////// FUNCTIONS ////////////////

int getRandomNumber(int max) // Obtained and modified from wibit.net "Sorting algorithms"
{
	time_t seconds;
	time(&seconds);
	srand((unsigned int) seconds);
	return rand() % max + 1;
}

void render( int posOfSnake[][2], int applePos[], string screen )
{
	gotoxy(0, 0);
	//cout << screen;

	/*
	cout << "########################################" << endl;
	cout << "#                                      #" << endl;
	cout << "#                                      #" << endl;
	cout << "#                                      #" << endl;
	cout << "#                                      #" << endl;
	cout << "#                                      #" << endl;
	cout << "#                                      #" << endl;
	cout << "#                                      #" << endl;
	cout << "#                                      #" << endl;
	cout << "#                                      #" << endl;
	cout << "#                                      #" << endl;
	cout << "#                                      #" << endl;
	cout << "#                                      #" << endl;
	cout << "#                                      #" << endl;
	cout << "#                                      #" << endl;
	cout << "#                                      #" << endl;
	cout << "#                                      #" << endl;
	cout << "#                                      #" << endl;
	cout << "#                                      #" << endl;
	cout << "########################################" << endl;
	cout << "Welcome to SNAKE!" << endl;
	cout << "Steer with WASD" << endl;
	cout << "If you touch the borders, you die" << endl;
	*/
	screen[applePos[1]*41+applePos[0]] = 'Q';
	
	for( int cnt1 = 0; cnt1 < 512; cnt1++)
	{
		//if (posOfSnake[cnt1][0] != 0 /*|| posOfSnake[cnt1][0] != 0 - Ree cheking the same statement slows down program unnecesarryly*/)
		//{

			screen[posOfSnake[cnt1][1]*41/*screen width + newline char*/+posOfSnake[cnt1][0]] = 'O';
			
			//gotoxy(posOfSnake[cnt1][0], posOfSnake[cnt1][1]);
			//cout << "O";
		//}
	}
	cout << screen;
}

////////////////////// MAIN ///////////////////

int main()
{
	
	// INITIATE SCREEN //
	system("cls");
	string screen;
	screen = "########################################\n#                                      #\n#                                      #\n#                                      #\n#                                      #\n#                                      #\n#                                      #\n#                                      #\n#                                      #\n#                                      #\n#                                      #\n#                                      #\n#                                      #\n#                                      #\n#                                      #\n#                                      #\n#                                      #\n#                                      #\n#                                      #\n########################################\nWelcome to SNAKE!\nSteer with WASD or the arrowkeys!\nIf you touch the borders, you die\n";
	// SCREEN INITIATED //
	removeCursor();
	enum {Top, Right, Down, Left};
	

	
	// Screen is 40 X 20  INCLUDING borders. 
	int posOfSnake[512][2];
	bool WKEY, AKEY, SKEY, DKEY, quitGame;
	WKEY = AKEY = SKEY = DKEY = quitGame = false;
	int direction = 0;
	int StartLengthSnake = 3;
	int applePos[2];
	applePos[0] = getRandomNumber(36) + 2;
	applePos[1] = getRandomNumber(16) + 2;
	int point = 0;
	int sleepTime = 150;
	int lengthOfSnake = StartLengthSnake;
	
	for( int cnt1 = 0; cnt1 < 512; cnt1++)
	{
		for( int cnt2 = 0; cnt2 < 2; cnt2++ )
		{
			posOfSnake[cnt1][cnt2] = 0;
		}
	}
		
	posOfSnake[0][0] = 19;
	posOfSnake[0][1] = 9;
		
	while ( quitGame != true ) // GameLoop
	{
		/////////////////// Get Key states ////////////
		WKEY = AKEY = SKEY = DKEY = false;
		
		if (GetAsyncKeyState(0x57) != 0 || GetAsyncKeyState(VK_UP) != 0)
		{
			WKEY = true;
		} else {
			WKEY = false;
		}
		if (GetAsyncKeyState(0x41) != 0 || GetAsyncKeyState(VK_LEFT) != 0)
		{
			AKEY = true;
		} else {
			AKEY = false;
		}
		if (GetAsyncKeyState(0x53) != 0 || GetAsyncKeyState(VK_DOWN) != 0)
		{
			SKEY = true;
		} else {
			SKEY = false;
		}
		if (GetAsyncKeyState(0x44) != 0 || GetAsyncKeyState(VK_RIGHT) != 0)
		{
			DKEY = true;
		} else {
			DKEY = false;
		}
		if (GetAsyncKeyState(0x1B) != 0)
		{
			quitGame = true;
		}
		////////////// End of get key states ///////////
		// Direction
		// Top = 0, Right = 1, Down = 2, Left = 3
		
		if ( WKEY ) {
			if ( direction != Down )
				direction = Top; 
		} else if ( DKEY ) {
			if ( direction != Left )
				direction = Right;
		} else if ( SKEY ) {
			if ( direction != Top )
				direction = Down;
		} else if ( AKEY ) {
			if ( direction != Right )
				direction = Left;
		}
		
		//////////////// Calculate Snake Pos ////////
		for ( int i = lengthOfSnake; i > 0;i-- )	//This is some gret shit right here!
		{
			posOfSnake[i][0] = posOfSnake[i - 1][0];
			posOfSnake[i][1] = posOfSnake[i - 1][1];
		}
		// Movement:
		if ( direction == Top )// Up
			posOfSnake[0][1] += -1;
		else if ( direction == Right )// Right
			posOfSnake[0][0] += 1;
		else if ( direction == Down )// Down
			posOfSnake[0][1] += 1;
		else if ( direction == Left )// Left
			posOfSnake[0][0] += -1;
		
		// Loop for printing posOfSnake to screen
		//for (int i = 0;i < 512;i++)		
		//	cout << posOfSnake[i][0] << posOfSnake[i][1] << endl;
		
		// Collision:
		if( posOfSnake[0][0] < 1 || posOfSnake[0][1] < 1 || posOfSnake[0][0] > 38 || posOfSnake[0][1] > 18 )
		{
			cout << "Ending game!" << endl;
			quitGame = true; 
			continue;
		}
		
		//Collision for apples and snakehead
		
		for( int i = 1; i < 512; i++ )
		{
			if( posOfSnake[0][0] == posOfSnake[i][0] && posOfSnake[0][1] == posOfSnake[i][1] )
			{
				quitGame = true;
				continue;
			}
		}
		
		if ( posOfSnake[0][0] == applePos[0] && posOfSnake[0][1] == applePos[1] )
		{
			applePos[0] = getRandomNumber(36) + 2;
			applePos[1] = getRandomNumber(16) + 2;
			point++;
			lengthOfSnake = point + StartLengthSnake;
		}
		
		render( posOfSnake, applePos, screen );
		
		/////// Must be at end! ///////

		//gotoxy(0, 23);
		Sleep(sleepTime);
	}
	cout << "GAME OVER!\nYou got " << point << " points!" << endl;
	Sleep(2000);
	system("pause");
	return 0;
}