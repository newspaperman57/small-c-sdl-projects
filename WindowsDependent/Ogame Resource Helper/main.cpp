#include <iostream>
#include <windows.h>
#include <time.h>

using namespace std;

void gotoxy(short int x, short int y) 
{ 
	COORD pos = {x, y};
	HANDLE output = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleCursorPosition(output, pos);
}

void setConsoleWindowSize(string sx, string sy, int x, int y)
{
	string sysCommand;
	sysCommand = "mode " + sx + "," + sy;
	system(sysCommand.c_str());   //Set mode to ensure window does not exceed buffer size
	SMALL_RECT WinRect = {0, 0, x, y};   //New dimensions for window in 8x12 pixel chars
	SMALL_RECT* WinSize = &WinRect;
	SetConsoleWindowInfo(GetStdHandle(STD_OUTPUT_HANDLE), true, WinSize);   //Set new size for window
}

int main()
{
	setConsoleWindowSize("55","8", 55, 8);
	
	int speed = 0;
	int required = 0;
	int hours = 0;
	int minutes = 0;
	int seconds = 0;
	int start = 0;
	float current = 0;
	int startTime = 0;
	int currentTime = 0;
	
	time_t timer;
	time_t timer2;
	
	cout << "Please write mining speed of the resource: " << endl;
	cin >> speed;
	cout << "Please write required resources: " << endl;
	cin >> required;
	cout << "Please write current resources: " << endl;
	cin >> start;
	
	time(&timer);
	
	system("cls");
	
	while(true)
	{
		Sleep(200);
		time(&timer2);
		current = (start+((float)speed/60/60*(timer2 - timer)));
		
		hours = (required - current)/speed;
		minutes = ((required - current)/speed*60) - (hours * 60);
		seconds = ((required - current)/speed*60*60) - (minutes * 60) - (hours * 60 * 60);
		
		gotoxy(0,0);
		cout << "You need " << required - (int)current << " more units." << endl
			 << "You SHOULD have about " << (int)current << " units." << endl
			 << "Time left:" << endl
			 << "Hours: " << hours << " " << endl
			 << "Minutes: " << minutes << " " << endl
			 << "Seconds: " << seconds << " " << endl
			 << endl << "Program has been running for: " << (timer2 - timer) << " seconds";
	}
	return 0;
}
