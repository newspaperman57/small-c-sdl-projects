#include <iostream>
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h> 
//#include <conio.h>
#include <cmath>

using namespace std;

void setWindowSize( int x , int y )
{
	COORD outbuff;
	outbuff.X = x;
	outbuff.Y = y;
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleScreenBufferSize(hConsole, outbuff);
	Sleep(130);
	SMALL_RECT windowSize = {0, 0, 900, 70}; 
	SetConsoleWindowInfo(hConsole, TRUE, &windowSize);
}

float sqr( float number ) // 'number' squared
{
	float answer = number * number;
	return answer;
}

float gDirection( int mPosX , int mPosY , int pPosX , int pPosY , int what ) // Calculates what way the object (meteor) should go
//The integer called 'what' is to know what to return
{
	float distance;
	
	float middleValueX = 0;
	float middleValueY = 0;
	if(mPosX != pPosX) // If meteor x position is greater than planet x position
	{
		middleValueX = pPosX - mPosX;
	}
	else if( mPosX == pPosX ) // If meteor x position is equal to planet x position
	{
		middleValueX = 0;
	}
	if(mPosY != pPosY) // If meteor y position is greater than planet y position
	{
		middleValueY = pPosY - mPosY;
	}
	else if( mPosY == pPosY ) // If meteor y position is equal to planet y position
	{
		middleValueY = 0;
	}
	int coolNumber;
	//cout << "Coolnumber = " << middleValueX << " + " << middleValueY << "\n";
	coolNumber = abs(middleValueX)+ abs(middleValueY);
	//cout << "Coolnumber = " << coolNumber << "\n";
	float xF; 
	float yF; 
	yF = middleValueY / coolNumber; // How much it needs to go vertically
	xF = middleValueX / coolNumber; // How much it needs to go horizontally
	distance = sqrt((sqr(abs(middleValueX)) + (sqr(abs(middleValueY)))));
	//cout << "Distance = square root of " << abs(middleValueX) << " squared plus " << abs(middleValueY) << "squared = " << distance; 
	if( what == 1 )
	{
		//cout << xF << "\n";
		return xF;
	}
	else if( what == 2 )
	{
		//cout << yF << "\n";
		return yF;
	}
	else if( what == 3 )
	{
		//cout << distance << "\n";
		return distance;
	}
}

float setSpeed()
{
	
}

int gotoxy( int column, int line )
{
	COORD coord;
	coord.X = column;
	coord.Y = line;
	SetConsoleCursorPosition(
	GetStdHandle( STD_OUTPUT_HANDLE ),coord);
}

int main()
{	
	srand(time(NULL));
	float pMass; // Indicates the mass of the planet in megaton
	pMass = rand() % 3000000; // Sets the planet mass
	int shown;
	shown = 1;
	setWindowSize( 100 , 50 );
	setSpeed();
	while(true)
	{	
		float speedX = rand() % 20 -10;
		float speedY = rand() % 20 -10;
		speedX /= 1000;
		speedY /= 1000;
		float mMass; // Indicates the mass of the meteor in ton
		mMass = rand() % 200000 + 0.1; // Sets the mass of the meteor
		int mPosX = rand() % 100 + 1;
		int mPosY = rand() % 50 + 1;
		float floatMX = mPosX;
		float floatMY = mPosY;
		int pPosX = 50; //rand() % 100 + 1;
		int pPosY =	25 ; // rand() % 50 + 1;	
		while( int(floatMX) != pPosX || int(floatMY) != pPosY )
		{
			Sleep(50);
			system("cls");
			float xF = gDirection( floatMX , floatMY , pPosX , pPosY , 1 ); // Return how much it needs to go horizontally
			float yF = gDirection( floatMX , floatMY , pPosX , pPosY , 2 ); // Return how much it needs to go vertically
			float distance = gDirection( floatMX , floatMY , pPosX , pPosY , 3 ); // Return the distance from meteor to planet
			speedX += xF * pMass / 20000000;
			speedY += yF * pMass / 20000000;
			floatMX += speedX;
			floatMY += speedY;
			gotoxy( pPosX , pPosY );
			cout << "O";
			gotoxy( floatMX , floatMY );
			cout << "o";
			gotoxy( 0 , 0 );
			cout << "   Current meteor: " << shown;
			cout << "   Meteor mass: " << mMass << " ton";
			cout << "   Planet mass: " << pMass/1000000 << " Megaton";
			if( speedX < 0 )
			{
				speedX += speedX / 100;
			}
			else if( speedX > 0)
			{
				speedX -= speedX / 100;
			}
			if( speedY < 0 )
			{
				speedY += speedY / 100;
			}
			else if( speedY > 0 )
			{
				speedY -= speedY / 100;
			}
		}
		pMass += mMass;
		shown++;
	}
}