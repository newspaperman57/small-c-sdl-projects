#include "socket.h"
#include <iostream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>

using namespace std;

int main()
{
	Socket::Init();
	while(true)
	{
		char c;
		do
		{
			//Clear the screen
			system("cls");
			system("title UDP client");
			//List menu and get input
			cout << "Press A to listen on a port" << endl;
			cout << "Press B to send UDP packets" << endl;
			cout << "Press C to punch holes in a wall" << endl;
			cout << "Press D to end the program" << endl;
			c = getchar();
			//Discar the rest of the input stream
			fseek(stdin,0,SEEK_END);
		}
		while(c != 'a' && c != 'b' && c != 'c' && c != 'd' && c != 'A' && c != 'B' && c != 'C' && c != 'D');
		if(c == 'a' || c == 'A')
		{
			//Get port between 0 and 65535
			system("title UDP listener");
			cout << "Enter port (must be between 0 and 65535)" << endl;
			string port_string;
			getline(cin,port_string);
			int port = atoi( port_string.c_str() );
			
			Socket socket_listen;
			if(socket_listen.Open(port))
			{
				system("cls");
				cout << "Press Esc to return to the menu" << endl;
				cout << "Now listening on port " << port << "..." << endl << endl;
				
				while(true)
				{
					
					if( GetAsyncKeyState(VK_ESCAPE) != 0)
						break;
					unsigned char buffer[256];
					Address sender;
					int bytes_read = socket_listen.Receive(sender, buffer, sizeof( buffer ));
					if ( bytes_read > 0)
					{
						//display sender
						SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 0x06);//Yellow
						cout << (short)sender.GetA() << "." << (short)sender.GetB() << "." << (short)sender.GetC() << "." << (short)sender.GetD() << ":" << sender.GetPort() << endl;
						SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 0x08);//Grey
						cout << bytes_read << " bytes read:" << endl;
						//display data
						SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 0x0F);//White
						cout << buffer << endl << endl;
					}
					Sleep(10);
				}
			}
			else
			{
				Sleep(1000);
				cout << " .";
				Sleep(500);
				cout << ".";
				Sleep(500);
				cout << ".";
				Sleep(1000);
			}
			
		}
		else if(c == 'b' || c == 'B')
		{
			//THIS SYSTEM IS NOT BUILT FOR INVALID OUTPUT!!!
			system("cls");
			system("title UDP messenger");
			//Get port between 0 and 65535
			cout << "choose OUT port (must be between 0 and 65535): ";
			string input;
			getline(cin,input);
			int port_out = atoi( input.c_str() );
			
			Socket socket_sender;
			if(socket_sender.Open(port_out))
			{
				cout << "Socket opened on port " << port_out << "..." << endl << endl;
				cout << "choose recievers IP: ";//endl is added by return key
				getline(cin, input);
				//subdevide string
				int IP[4];
				unsigned short pos[4];
				pos[0] = input.find('.');
				pos[1] = input.find('.',pos[0]+1);
				pos[2] = input.find('.',pos[1]+1);
				pos[3] = input.find('\n',pos[2]+1);
				IP[0] = atoi(input.substr(0,pos[0]).c_str());
				IP[1] = atoi(input.substr(pos[0]+1,pos[1]).c_str());
				IP[2] = atoi(input.substr(pos[1]+1,pos[2]).c_str());
				IP[3] = atoi(input.substr(pos[2]+1,pos[3]).c_str());
				cout << "Choose receiver port: ";
				getline(cin,input);
				int port_in = atoi( input.c_str() );
				Address receiver(IP[0],IP[1],IP[2],IP[3],port_in);
				
				system("cls");
				cout << "Enter packet data." << endl;
				cout << "Enter 'quit' to return to menu." << endl << endl;
					
				while(true)
				{
					SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 0x0F);//White
					
					//Get packet data
					getline(cin,input);
					if(input == "quit")
						break;
					char data[input.length()];
					strcpy(data, input.c_str());
					if(!sizeof(data))
						continue;
					socket_sender.Send(receiver, data, sizeof(data));
					
					//tell packet size
					SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 0x08);//Grey
					cout << sizeof(data) << " bytes sent" << endl; 
					//display reciever
					SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 0x06);//Yellow
					cout << (short)receiver.GetA() << "." << (short)receiver.GetB() << "." << (short)receiver.GetC() << "." << (short)receiver.GetD() << ":" << receiver.GetPort() << endl;
					cout << endl;
				}
			}
			Sleep(1000);
			cout << " .";
			Sleep(500);
			cout << ".";
			Sleep(500);
			cout << ".";
			Sleep(1000);
		}
		else if(c == 'c' || c == 'C')
		{
			//THIS SYSTEM IS NOT BUILT FOR INVALID OUTPUT!!!
			system("cls");
			system("title UDP hole in-the-damned puncher");
			//Get port between 0 and 65535
			cout << "choose punch port (must be between 0 and 65535): ";
			string input;
			getline(cin,input);
			int port_punch = atoi( input.c_str() );
			
			Socket socket_puncher;
			if(socket_puncher.Open(port_punch))
			{
				cout << "Socket opened on port " << port_punch << "..." << endl << endl;
				cout << "choose recievers IP: ";//endl is added by return key
				getline(cin, input);
				//subdevide string
				int IP[4];
				unsigned short pos[4];
				pos[0] = input.find('.');
				pos[1] = input.find('.',pos[0]+1);
				pos[2] = input.find('.',pos[1]+1);
				pos[3] = input.find('\n',pos[2]+1);
				IP[0] = atoi(input.substr(0,pos[0]).c_str());
				IP[1] = atoi(input.substr(pos[0]+1,pos[1]).c_str());
				IP[2] = atoi(input.substr(pos[1]+1,pos[2]).c_str());
				IP[3] = atoi(input.substr(pos[2]+1,pos[3]).c_str());
				
				Address receiver(IP[0],IP[1],IP[2],IP[3],port_punch);
				
				system("cls");
				cout << "Punching holes" << endl << endl;
				int packetNumber = 1;
				while(true)
				{
					input = to_string(packetNumber);
					packetNumber++;
					//Send packet
					SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 0x0F);//White
					cout << input << endl;
					int bytes_sent = socket_puncher.Send(receiver, input.c_str(), input.length());
					//tell packet size
					SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 0x08);//Grey
					cout << bytes_sent << " bytes sent" << endl; 
					//display reciever
					SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 0x06);//Yellow
					cout << (short)receiver.GetA() << "." << (short)receiver.GetB() << "." << (short)receiver.GetC() << "." << (short)receiver.GetD() << ":" << receiver.GetPort() << endl;
					cout << endl;
					
					
					//Recieve respons
					while(true)
					{
						unsigned char buffer[256];
						Address sender;
						int bytes_read = socket_puncher.Receive(sender, buffer, sizeof( buffer ));
						if ( bytes_read > 0)
						{
							cout << "CONNECTION!" << endl;
							//display sender
							SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 0x06);//Yellow
							cout << (short)sender.GetA() << "." << (short)sender.GetB() << "." << (short)sender.GetC() << "." << (short)sender.GetD() << ":" << sender.GetPort() << endl;
							SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 0x08);//Grey
							cout << bytes_read << " bytes read:" << endl;
							//display data
							SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 0x0F);//White
							cout << buffer << endl << endl;
						}
						else
						{
							cout << "nothing from " << ntoh(sender.GetAddress()) << endl;
							break;
						}
					}
					Sleep(300);
					
				}
			}
			Sleep(1000);
			cout << " .";
			Sleep(500);
			cout << ".";
			Sleep(500);
			cout << ".";
			Sleep(1000);
		}
		else if(c == 'd' || c == 'D')
		{
			break;
		}
	}
	system("cls");
	system("title C:\\Windows\\system32\\cmd.exe");
	Socket::Quit();
	return 0;
}