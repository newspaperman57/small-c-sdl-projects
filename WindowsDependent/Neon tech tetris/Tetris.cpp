#include "Tetris.h"

Tetris::Tetris()
{
	setConsoleWindowTitle("Tech Tetris v0.1");
	//setConsoleFontSize();
	removeCursor();
	setConsoleWindowSize(24, 16);
	setTextColor(GREY);
	clear();
	
	background = readFile("background");
	cout << background;
	
	running = true;
	FPS = 3;
	srand(time(NULL));
	
	for(int x = 0; x < 10; x++ )
		for(int y = 0; y < 16; y++)
			map[x][y] = GREY;
}

Tetris::~Tetris()
{
	setConsoleWindowSize(80, 25);
	system("color 0F"); //setTextColor(0x0F);
	flushInputBuffer();
	setConsoleFontSize(7, 12);
}

void Tetris::go()
{
	newPiece();
	loop();
}

void Tetris::loop()
{
	while(running)
	{
		event();
		update();
		render();
		
		Sleep(1000/FPS);
	}
}

void Tetris::event()
{
	if(GetAsyncKeyState(VK_ESCAPE))
		running = false;

	if(GetAsyncKeyState(VK_SPACE))
		keystate[SPACE] = 1;
	else
		keystate[SPACE] = 0;
		
	if(GetAsyncKeyState(VK_RIGHT))
		keystate[RIGHT] = 1;
	else
		keystate[RIGHT] = 0;
		
	if(GetAsyncKeyState(VK_LEFT))
		keystate[LEFT] = 1;
	else
		keystate[LEFT] = 0;
	/*
	if(GetAsyncKeyState(VK_DOWN))
		keystate[DOWN] = 1;
	else
		keystate[DOWN] = 0;
	*/
}

void Tetris::update()
{
	if(keystate[SPACE])
		rotateBlock();
	if(keystate[LEFT])
		move(-1,0);
	if(keystate[RIGHT])
		move(1,0);
	
	move(0,1);
}

void Tetris::render()
{
	gotoxy(0,0);
	setTextColor(8);
	
	
	//draw the map
	for(int x = 0; x < 10; x++ )
		for(int y = 0; y < 16; y++ )
				drawBlock(x, y, map[x][y]);

	//draw moving block
	for(int x = 0; x < 4; x++ )
		for(int y = 0; y < 4; y++ )
			if(piece.size[x][y] != NONE)
				drawBlock(piece.x+x, piece.y+y, piece.size[x][y]);
}

void Tetris::newPiece()
{
	for(int x = 0; x < 4; x++ )
		for(int y = 0; y < 4; y++ )
			piece.size[x][y]=NONE;
			
	piece.x = 10 / 2 - 2;
	piece.y = -1;

	int newblock = rand()%7;

	switch (newblock)
	{
	case 0: //Tower!
		{
			piece.size[1][0]=RED;
			piece.size[1][1]=RED;
			piece.size[1][2]=RED;
			piece.size[1][3]=RED;
			piece.y=0;
		}break;
	case 1: //Box!
		{
			piece.size[1][1]=BLUE;
			piece.size[1][2]=BLUE;
			piece.size[2][1]=BLUE;
			piece.size[2][2]=BLUE;
		}break;
	case 2: //Pyramid!
		{
			piece.size[1][1]=GREEN;
			piece.size[0][2]=GREEN;
			piece.size[1][2]=GREEN;
			piece.size[2][2]=GREEN;
		}break;
	case 3://Left Leaner
		{
			piece.size[0][1]=WHITE;
			piece.size[1][1]=WHITE;
			piece.size[1][2]=WHITE;
			piece.size[2][2]=WHITE;
		}break;
	case 4://Right Leaner
		{
			piece.size[2][1]=AQUA;
			piece.size[1][1]=AQUA;
			piece.size[1][2]=AQUA;
			piece.size[0][2]=AQUA;
		}break;
	case 5://Left Knight
		{
			piece.size[1][1]=YELLOW;
			piece.size[2][1]=YELLOW;
			piece.size[2][2]=YELLOW;
			piece.size[2][3]=YELLOW;
		}break;
	case 6://Right Knight
		{
			piece.size[2][1]=PINK;
			piece.size[1][1]=PINK;
			piece.size[1][2]=PINK;
			piece.size[1][3]=PINK;
		}break;
	}
}

void Tetris::drawBlock(int x, int y, int color)
{
	gotoxy( 2*x+2, y);
	setTextColor(color);
	char out;
	switch(color)
	{
		case GREY:   out = '0'; break;
		case RED:    out = '1'; break;
		case BLUE:   out = '2'; break;
		case GREEN:  out = '3'; break;
		case WHITE:  out = '4'; break;
		case AQUA:   out = '5'; break;
		case YELLOW: out = '6'; break;
		case PINK:   out = '7'; break;
	}
	
	cout << out;
}

void Tetris::move(int x, int y)
{
	if(collision(x, y))
	{
		if(y == 1)
		{
			if(piece.y < 1)
			{
				//you lose!  new game.
				running = false;
			}
			else
			{
				bool killblock=false;
				int i,j;
				//new block time! add this one to the list!
				for(i=0; i<4; i++)
					for(j=0; j<4; j++)
						if(piece.size[i][j] != NONE)
							map[piece.x+i][piece.y+j] = piece.size[i][j];

				//check for cleared row!
				for(j=0; j< 16; j++)
				{
					bool filled=true;
					for(i=0; i< 10; i++)
						if(map[ i ][j] == GREY)
							filled=false;

					if(filled)
					{
						removeRow(j);
						killblock=true;
					}
				}
				if(killblock)
				{
					for(i=0; i<4; i++)
						for(j=0; j<4; j++)
							piece.size[ i ][j]=NONE;
				}
				newPiece();
			}
		}

	}
	else
	{
	piece.x += x;
	piece.y += y;
	}
}

bool Tetris::collision(int nx, int ny)
{
	int px=piece.x+nx;
	int py=piece.y+ny;

	int i,j,x,y;

	for(i=0; i< 4; i++)
		for(j=0; j< 4; j++)
			if(piece.size[i][j] != NONE)
				if(px + i < 0 || px + i > 10 -1||
					py + j < 0 || py + j > 16 -1)
					return true;

	for(x=0; x< 10; x++)
		for(y=0; y< 16; y++)
			if(x >= px && x < px + 4)
				if(y >= py && y < py +4)
					if(map[x][y] != GREY)
						if(piece.size[x - px][y - py] != NONE)
							return true;

	return false;
}

void Tetris::removeRow(int row)
{
	int x,y;
	int counter=0;

	for(x=0; x< 10; x++)
		for(y=row; y>0; y--)
			map[x][y]=map[x][y-1];

}

void Tetris::rotateBlock()
{
int i, j, temp[4][4];

	//copy &rotate the piece to the temporary array
	for(i=0; i<4; i++)
		for(j=0; j<4; j++)
			temp[3-j][i]=piece.size[i][j];

	//check collision of the temporary array with map borders
	for(i=0; i<4; i++)
		for(j=0; j<4; j++)
			if(temp[i][j] != NONE)
				if(piece.x + i < 0 || piece.x + i > 10 - 1 ||
					piece.y + j < 0 || piece.y + j > 16 - 1)
					return;

	//check collision of the temporary array with the blocks on the map
	for(int x=0; x< 10; x++)
		for(int y=0; y< 16; y++)
			if(x >= piece.x && x < piece.x + 4)
				if(y >= piece.y && y < piece.y +4)
					if(map[x][y] != GREY)
						if(temp[x - piece.x][y - piece.y] != NONE)
							return;

	//end collision check

	//successful!  copy the rotated temporary array to the original piece
	for(i=0; i<4; i++)
		for(j=0; j<4; j++)
			piece.size[i][j]=temp[i][j];
}