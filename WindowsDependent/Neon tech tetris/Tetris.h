#ifndef __TETRIS
#define __TETRIS

#include "nolib.h"
#include <string>
#include <iostream>
#include <time.h>
#include <windows.h>

using namespace std;

struct Piece
{
	int size[4][4];
	int x, y;
};

enum { NONE = 0x0,
	   GREY = 0x87,
	   RED = 0x8C,
	   YELLOW = 0x8E,
	   PINK = 0x8D,
	   BLUE = 0x89,
	   AQUA = 0x8B,
	   GREEN = 0x8A,
	   WHITE = 0x8F
	 };
	 
enum { SPACE = 0, LEFT = 1, RIGHT = 2 };

class Tetris
{
	public:
		Tetris();
		~Tetris();
		void go();
		
	private:
		void loop();
		void render();
		void event();
		void update();
		
		void newPiece();
		void drawBlock(int, int, int);
		void move(int, int);
		bool collision(int, int);
		void removeRow(int);
		void rotateBlock();
		
		Piece piece;
		int map[10][16];
		string background;
		int keystate[3];
		bool running;
		int FPS;
		
};

#endif