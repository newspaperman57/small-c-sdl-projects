#ifndef __NOLIB
#define __NOLIB

#include <windows.h>
#include <fstream>
#include <string>

using namespace std;

void gotoxy(short int, short int) ;
void removeCursor();
void setTextColor(int);
void clear();
void setConsoleWindowSize(short int, short int);
void flushInputBuffer();
void setConsoleWindowTitle(LPCTSTR);
void setConsoleFontSize(int, int);

string readFile(string);

struct pos2d
{
	int x,y;
};

struct rect
{
	int x,y,w,h;
};


#endif