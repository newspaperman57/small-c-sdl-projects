#include "nolib.h"


void gotoxy(short int x, short int y) 
{ 
	COORD pos = {x, y};
	HANDLE output = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleCursorPosition(output, pos);
}
///////////////////////////////////////////////////
void removeCursor()
{
	CONSOLE_CURSOR_INFO info;
	HANDLE hOutput = GetStdHandle (STD_OUTPUT_HANDLE);
	// turn the cursor off
	info.bVisible = FALSE;
	info.dwSize = 1;
	SetConsoleCursorInfo(hOutput,&info);
}
///////////////////////////////////////////////////
string readFile(string fileName)
{
	string fReturn;
	string temp;

	ifstream f;
	f.open(fileName);
	while(!f.eof())
	{
		getline(f,temp);
		if(!f.eof())
			fReturn += temp + '\n';
		else
			fReturn += temp;
	}
	return fReturn;
}
///////////////////////////////////////////////////
void setTextColor(int color)
{
	HANDLE consoleHandle = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(consoleHandle, color);
}
///////////////////////////////////////////////////
void clear()
{
	system("cls");
}
///////////////////////////////////////////////////
void setConsoleWindowSize(short int x, short int y)
{
	string sx,sy,sysCommand;
	sx = to_string(x);
	sy = to_string(y);
	sysCommand = "mode " + sx + "," + sy;
	system(sysCommand.c_str());   //Set mode to ensure window does not exceed buffer size
	SMALL_RECT WinRect = {0, 0, x, y};   //New dimensions for window in 8x12 pixel chars
	SMALL_RECT* WinSize = &WinRect;
	SetConsoleWindowInfo(GetStdHandle(STD_OUTPUT_HANDLE), true, WinSize);   //Set new size for window

}

void flushInputBuffer()
{
	HANDLE hConsoleInput = GetStdHandle(STD_INPUT_HANDLE);
	FlushConsoleInputBuffer(hConsoleInput);
}

void setConsoleWindowTitle(LPCTSTR title)
{
	SetConsoleTitle(title);
}

void setConsoleFontSize(int x, int y)
{
	/*
	HANDLE outcon = GetStdHandle(STD_OUTPUT_HANDLE);//you don't have to call this function every time
	//NOT WORKING
    _CONSOLE_FONT_INFO font;//CONSOLE_FONT_INFOEX is defined in some windows header
    GetCurrentConsoleFontEx(outcon, false, &font);//PCONSOLE_FONT_INFOEX is the same as CONSOLE_FONT_INFOEX*
    font.dwFontSize.X = x;
    font.dwFontSize.Y = y;
    SetCurrentConsoleFontEx(outcon, false, &font);
	*/
}
