#include <iostream>
#include <windows.h>
#include "Window.h"

using namespace std;

class Player
{
	private:
		
	public:
		void Print(int x, int y);
};

void Player::Print(int x, int y)
{
	COORD pos = {x, y - 1};
	HANDLE output = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleCursorPosition(output, pos);
	
	cout << "O";
	
	pos = {x - 1, y};
	SetConsoleCursorPosition(output, pos);
	
	cout << "/T\\";
	
	pos = {x, y + 1};
	SetConsoleCursorPosition(output, pos);
	
	cout << "^";
	
}

int main()
{
	Player player;
	Window window;
	
	int screenX, screenY, screenW, screenH;
	float playerX, playerY, speed;
	bool running = true;
	
	window.GetMainMonitorRes(screenH, screenW);
	
	screenX = 50;
	screenY = 50;
	
	playerX = (screenH / 2) / 8 - screenX / 2;
	playerY = (screenW / 2) / 8 - screenY / 2;
	
	speed = 1;
	
	window.Resize(screenX, screenY);
	window.RemoveCursor();
	
	while(running)
	{
		window.GotoXY(0,0);
		if( GetAsyncKeyState(VK_UP) != 0)
		{
			cout << "UP" << endl;
			playerY -= speed;
		}
		if( GetAsyncKeyState(VK_RIGHT) != 0)
		{
			cout << "RIGHT" << endl;
			playerX += speed;
		}
		if( GetAsyncKeyState(VK_LEFT) != 0)
		{
			cout << "LEFT" << endl;
			playerX -= speed;
		}
		if( GetAsyncKeyState(VK_DOWN) != 0)
		{
			cout << "DOWN" << endl;
			playerY += speed;
		}
		if( GetAsyncKeyState(VK_ESCAPE) != 0 )
		{
			running = false;
		}
		cout << playerX << " X " << playerY << endl;
		
		window.Move( playerX * 8, playerY * 8);
		
		player.Print( screenX / 2, screenY / 2 );
		window.SetTextColor(1);
		for(int i = 100; i <= 110; i++)
			window.Print(i,50, 'O');
		for(int i = 45; i <= 55; i++)
			window.Print(105, i, 'O');
		
		window.SetTextColor(2);
		
		Sleep(1000/10);
		system("cls");
	}
	
	window.GotoXY(0, 0);
	return 0;
}