#ifndef __WINDOW
#define __WINDOW

#ifndef _WIN32_WINNT
#define _WIN32_WINNT 0x0500
#endif /*_WIN32_WINNT*/

#include <Windows.h>
#include <string>
#include <iostream>

using namespace std;

class Window
{
	private:
		HWND windowHandle;
		COORD size;
		int screenX, screenY, screenSizeX, screenSizeY;
	public:
		Window();
		bool Move(int x, int y);
		bool Resize(short int x, short int y);
		void RemoveCursor();
		bool GotoXY(int x, int y);
		bool Print(int, int, char);
		void GetMainMonitorRes(int &X, int &Y);
		void SetTextColor(int);
		HWND GetHandle();
};

#endif