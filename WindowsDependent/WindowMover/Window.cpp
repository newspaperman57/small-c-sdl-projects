#include "Window.h"

HWND Window::GetHandle()
{
	return windowHandle;
}

bool Window::Move(int x, int y)
{
	if(x < 0 or y < 0)
	{
		return 0;
	}
	screenX = x;
	screenY = y;
	
	SetWindowPos( windowHandle, HWND_TOP, x, y, 0, 0, SWP_NOSIZE );
	return 1;
}

bool Window::Resize(short int x, short int y)
{
	if(x < 0 or y < 0)
	{
		return 0;
	}
	screenSizeX = x;
	screenSizeY = y;
	string sx,sy,sysCommand;
	sx = to_string(x);
	sy = to_string(y);
	sysCommand = "mode " + sx + "," + sy;
	system(sysCommand.c_str());   //Set mode to ensure window does not exceed buffer size
	SMALL_RECT WinRect = {0, 0, x, y};   //New dimensions for window in 8x12 pixel chars
	SMALL_RECT* WinSize = &WinRect;
	SetConsoleWindowInfo(GetStdHandle(STD_OUTPUT_HANDLE), true, WinSize);   //Set new size for window
	return 1;
}

bool Window::Print(int x, int y, char Char)
{
	if(x < screenX / 8 or x > screenX / 8 + screenSizeX - 1 or y < screenY / 8 or y > screenY / 8 + screenSizeY - 1)
		return false;
	if(x + 1 == ( screenX / 8 ) + screenSizeX and y + 1 == (screenY / 8) + screenSizeY)
		return false;
	GotoXY(x - screenX/8, y - screenY/8);
	cout << Char;
	return true;
}

bool Window::GotoXY(int x, int y)
{
	COORD pos = {x, y};
	HANDLE output = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleCursorPosition(output, pos);
	return 1;
}

void Window::RemoveCursor()
{
	CONSOLE_CURSOR_INFO info;
	HANDLE hOutput = GetStdHandle (STD_OUTPUT_HANDLE);
	info.bVisible = FALSE;
	info.dwSize = 1;
	SetConsoleCursorInfo(hOutput,&info);
}

Window::Window()
{
	#define MY_BUFSIZE 1024
	char pszNewWindowTitle[MY_BUFSIZE];
	char pszOldWindowTitle[MY_BUFSIZE];
	GetConsoleTitle(pszOldWindowTitle, MY_BUFSIZE);
	wsprintf(pszNewWindowTitle,"%d/%d", GetTickCount(), GetCurrentProcessId());
	SetConsoleTitle(pszNewWindowTitle);
	Sleep(40);// Ensure window title has been updated.
	windowHandle=FindWindow(NULL, pszNewWindowTitle);
	SetConsoleTitle(pszOldWindowTitle);
}