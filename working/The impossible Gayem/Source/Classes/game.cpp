#include "game.h"

#include "SDL_rotozoom.h"

using namespace std;


Game::Game()
{
	screenDimension.w = 1280; 	// Width  of the initialized window
	screenDimension.h = 720; 	// Height of the initialized window
	screenDimension.x = 0; 		// initialises x coordinate as 0 so to better work with collision detection
	screenDimension.y = 0; 		// initialises y coordinate as 0 so to better work with collision detection

	ground.w = screenDimension.w;
	ground.h = 800;
	ground.x = screenDimension.x;
	ground.y = screenDimension.h - 8; 	//Places the ground rect 8 above the bottom of the screen

	useFrameCap = false; 		// Tells the frame cap wether to activate or not
	quitGame = false; 			// Tells the main loop to quit if true
	FPS = 60; 					// Frames per second. Used by the frame cap.	
	
	cout << "Game: instance created" << endl;

	initialize(); // Initializes game
}

Game::~Game()
{
	cout << "Destructing Game" << endl;
	
	SDL_FreeSurface( background1 );
	SDL_FreeSurface( background2 );
	SDL_FreeSurface( display );	// Deletes the Surface 'Display'
	SDL_Quit(); 				// Quits SDL. Removes the window.
	cout << "Game: instace destroyed" << endl;
}

void Game::initialize()
{
	cout << "Game: Game.Initialize started" << endl;

	SDL_Init( SDL_INIT_EVERYTHING );					// Initializes SDL

	display = SDL_SetVideoMode( screenDimension.w, 
								screenDimension.h, 
								32, 
								SDL_HWSURFACE | SDL_DOUBLEBUF 
							  );						// Initializes a window, and saves the Adress to 'display'

	cout << "Game: SetvideoMode to " 
		 << screenDimension.w 
		 << " X " 
		 << screenDimension.h 
		 << endl;

	SDL_WM_SetCaption( "The Impossible Gayem", NULL ); 	// Sets the caption to the window
	
	player.setPos( screenDimension.w / 2, screenDimension.h - 83 );

	imageManager.init( screenDimension );
	loadAssets();
	compose();

	cout << "Game: Game.initialize ended!" << endl;
}

void Game::loadAssets()
{
	// Background graphics
	imageManager.load_image("Assets/sky.png");		// #0
	imageManager.load_image("Assets/fence.png");	// #1
	imageManager.load_image("Assets/grass.png");	// #2

	// Player associated sprites
	imageManager.load_image("Assets/player.png");	// #3

	// Throwables
	imageManager.load_image("Assets/cheese.png");	// #4
	
	// Enemies
	imageManager.load_image("Assets/triangle.png");	// #5
	imageManager.load_image("Assets/square.png");	// #6
	imageManager.load_image("Assets/pentagon.png");	// #7
	imageManager.load_image("Assets/hexagon.png");	// #8

	//Logos
	imageManager.load_image("Assets/Logos/logo-red.png");	// #9

	imageManager.load_image("Assets/face-left.png");	// #10
	imageManager.load_image("Assets/face-right.png");	// #11
	
}

void Game::compose()
{
	Uint32 rmask, gmask, bmask, amask;
    #if SDL_BYTEORDER == SDL_BIG_ENDIAN
        rmask = 0xff000000;
        gmask = 0x00ff0000;
        bmask = 0x0000ff00;
        amask = 0x000000ff;
    #else
        rmask = 0x000000ff;
        gmask = 0x0000ff00;
        bmask = 0x00ff0000;
        amask = 0xff000000;
    #endif

    background1 = SDL_CreateRGBSurface(0, screenDimension.w, screenDimension.h, 32, rmask, gmask, bmask, amask);
    background2 = SDL_CreateRGBSurface(0, screenDimension.w, screenDimension.h, 32, rmask, gmask, bmask, amask);

	for( int i = 0; i < (screenDimension.w / imageManager.sprites[SKY]->w); i ++ )
	{
		imageManager.apply_surface( i * imageManager.sprites[SKY]->w, screenDimension.h - imageManager.sprites[SKY]->h, imageManager.sprites[SKY], background1, NULL );	
	}
	for( int i = 0; i < (screenDimension.w / imageManager.sprites[FENCE]->w); i ++ )
	{
		imageManager.apply_surface( i * imageManager.sprites[FENCE]->w, screenDimension.h - imageManager.sprites[FENCE]->h, imageManager.sprites[FENCE], background1, NULL );	
	}
	for( int i = 0; i < (screenDimension.w / imageManager.sprites[GRASS]->w); i ++ )
	{
		imageManager.apply_surface( i * imageManager.sprites[GRASS]->w, screenDimension.h - imageManager.sprites[GRASS]->h, imageManager.sprites[GRASS], background2, NULL );
	}
}	



void Game::go()
{
	cout << "Game: Game.go started" << endl;

	while( !quitGame )
	{
		processEvents();
		update();
		render();

		// Limit the FPS
		if (useFrameCap) // In case you don't want the frame cap.
		{
			cout << "capping frames" << endl;
			if ( timer.Get_ticks() < 1000 / FPS ) // Checks if 1 cycle runs faster than a [FPS]th part of a second
			{
				SDL_Delay( ( 1000 / FPS ) - timer.Get_ticks() ); // Wait the missing FPSth part of a second
			}
			timer.Start(); // Starts a timer for the Framecap
		}
	}
	
	cout << "Game: Game.go ended!" << endl;
}

void Game::processEvents()
{
	SDL_Event event; 					// Creates a placeholder for the eventHandler
	while ( SDL_PollEvent( &event ) ) 	// Begins to go trough Events. Polls 1 event
	{
		if ( event.type == SDL_QUIT ) 	// If event is "Kill the goddamn game"
		{
			cout << "Game: SDL_QUIT event occured!" << endl;
			quitGame = true; 			// Quit the game in the end of this cycle
		}
	}

}

void Game::update()
{
	box.update();
	player.update( ground ); // Update player Position

	// Main game calculation
		
	if ( player.getLife() <= 0 ) // If player has 0 or less life. Kill the game
	{
		cout << "Game: Player died" << endl;
		quitGame = true;
	}
	
	// Main game calculation !END!

}

void Game::render()
{
	//SDL_FillRect(display, NULL,  SDL_MapRGB( display->format, 61, 216, 255));	// Fills the surface with lightblue

	imageManager.drawRect( screenDimension, 0x04, 0xA6,0xF7 );

	imageManager.apply_surface( NULL, background1, imageManager.getBuffer(), NULL );
	imageManager.drawImage( 9, ( screenDimension.w / 2 ) - ( imageManager.sprites[9]->w / 2), 20 );	//logo


	/////////////////////// WARNING !!! BLODDY MESS BELOW SHOULD BE CLEANED UP ////////////////////////////////////////


	SDL_Surface* temp;
	int i = 0;
	if (player.getxspeed() > 0)
	{
		i = 11;
	}
	else if ( player.getxspeed() < 0 )
	{
		i = 10;
	}
	else
	{
		i = 3;
	}
	temp = rotozoomSurface( imageManager.sprites[i], player.getAngle(), 1.0, 0 );
	
	//SDL_BlitSurface( rotozoomSurface( imageManager.sprites[PLAYER], player.getAngle(), 1.0, 0 ) , player.getPos(), imageManager.getBuffer(), NULL );
	imageManager.apply_surface( player.getPos().x - (temp->w/2 - imageManager.sprites[i]->w/2), player.getPos().y - (temp->h/2 - imageManager.sprites[i]->h/2), temp, imageManager.getBuffer(), NULL );
	SDL_FreeSurface( temp );


	//imageManager.drawImage( PLAYER, player.getPos() );

	///////////////////// END OF BLODDY MESS!!! ////////////////////////////

	imageManager.drawImage( TRIANGLE, box.getPos() );
	imageManager.apply_surface( NULL, background2, imageManager.getBuffer(), NULL );

	SDL_BlitSurface( imageManager.getBuffer(), NULL, display, NULL );
	SDL_Flip( display ); // Flips the screen.
}