#include "player.h"

using namespace std;

Player::Player()
{
	cout << "Player: instance will now be greated" << endl;
	pos.x = 0;
	pos.y = 0;
	pos.w = 75;
	pos.h = 75;
	move.x = 0;
	move.y = 0;
	life = 100;


	grav = 1;
	weight = 3;
	yspeed = 0;
	jumpForce = 10;
	jumpPower = 0;

	acceleration = 0.3;
	xspeed = 0;
	startSpeed = 5;

	maxSpeed = 15;

	angle = 0;


}

Player::~Player()
{
	cout << "Player: Instance got destroyed!" << endl;
}

int Player::getLife()
{
	return life; // Returns life of player
}

int Player::getAngle()
{
	return angle;
}

void Player::update( SDL_Rect ground )
{
	Uint8* keystate = SDL_GetKeyState( NULL );
	if ( collision.isCollision( pos, ground ) ) // If is on  ground
	{
		if ( jumpPower != 0 )
		{
			pos.y = ground.y - pos.h;
			jumpPower = 0;
			yspeed = 0;
		}
		if ( keystate[ SDLK_UP ] || keystate[ SDLK_w ] )
		{
			jumpPower += jumpForce;
			yspeed -= jumpPower;
		}
	}
	else	// If in the air
	{
		if ( jumpPower > -1 * maxSpeed || jumpPower < maxSpeed )
		{
			jumpPower -= grav * weight;
			yspeed -= jumpPower;
		}
	}

	if ( keystate[ SDLK_RIGHT ] || keystate[ SDLK_d ] )	// Vertical right button pressed
	{
		if ( xspeed == 0 )
		{
			xspeed += startSpeed;
		}
		else if ( xspeed < 0 )
		{
			xspeed += acceleration * 5;
		}
		else if ( xspeed < maxSpeed )
		{
			xspeed += acceleration;
		}
	}
	if ( keystate[ SDLK_LEFT ] || keystate[ SDLK_a ] )	// Vertical left button pressed
	{
		if ( xspeed == 0 )
		{
			xspeed -= startSpeed;
		}
		else if ( xspeed > 0 )
		{
			xspeed -= acceleration * 5;
		}
		else if ( xspeed > maxSpeed * (-1) )
		{
			xspeed -= acceleration;
		}
	}
	if ( !( keystate[ SDLK_RIGHT ] || keystate[ SDLK_d ] || keystate[ SDLK_LEFT ] || keystate[ SDLK_a ] ) )	//No vertical movement button was pressed
	{
		if ( xspeed < 0 )	// If moving left
		{
			if ( xspeed > -3 )
			{
				xspeed = 0;
			}
			else
			{
				xspeed += ( xspeed * ( -1 ) / 5);
			}
		}
		else 	// Else moving right
		{
			if ( xspeed < 3 )
			{
				xspeed = 0;
			}
			else
			{
				xspeed -= xspeed / 5;
			}
		}
	}

	angle -= xspeed;

	pos.x += xspeed;
	pos.y += yspeed;
	move.x = move.y = 0;
}

SDL_Rect Player::getPos()
{
	return pos; //Returns position of player
}

float Player::getxspeed()
{
	return xspeed;
}

void Player::setPos( SDL_Rect posi )
{
	pos.x = posi.x;
	pos.y = posi.y;
}

void Player::setPos( int x, int y )
{
	pos.x = x;
	pos.y = y;
}

void Player::setMove( SDL_Rect movem )
{
	move.x = move.x + movem.x;
	move.y = move.y + movem.y;
}

void Player::setMove( int x, int y )
{
	move.x = move.x + x;
	move.y = move.y + y;
}