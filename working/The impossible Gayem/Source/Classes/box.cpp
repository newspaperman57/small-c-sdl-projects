#include "box.h"

using namespace std;



Box::Box()
{
	cout << "Box: Initiating instance..." << endl;
	
	pos.y = -200;
	pos.x = 800;
	
	cout << "Box: Initiated instance. pos of box: " << endl;
}

Box::~Box()
{
	cout << "Box: instance got destroyed!" << endl;
}

void Box::init()
{
	cout << "Box: init function called" << endl;
}

void Box::update()
{
	move( 1, 5 );
}

void Box::move( int x, int y )
{
	pos.x += x;
	pos.y += y;
}

SDL_Rect Box::getPos()
{
	return pos;
}