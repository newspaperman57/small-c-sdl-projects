#include "imagemanager.h"

using namespace std;

ImageManager::ImageManager()
{

}

ImageManager::~ImageManager()
{
	cout << "Imagemanager: Instance will be destroyed!" << endl;
    for( unsigned int i = 0; i < sprites.size(); i++ )
    {
        SDL_FreeSurface( sprites[i] );
    }

    SDL_FreeSurface( buffer );
	cout << "Imagemanager: Instance is now destroyed!" << endl;
}

void ImageManager::init( SDL_Rect displayDimen )
{
    int width = displayDimen.w;
    int height = displayDimen.h;

    Uint32 rmask, gmask, bmask, amask;
    #if SDL_BYTEORDER == SDL_BIG_ENDIAN
        rmask = 0xff000000;
        gmask = 0x00ff0000;
        bmask = 0x0000ff00;
        amask = 0x000000ff;
    #else
        rmask = 0x000000ff;
        gmask = 0x0000ff00;
        bmask = 0x00ff0000;
        amask = 0xff000000;
    #endif

    buffer = SDL_CreateRGBSurface(0, width, height, 32, rmask, gmask, bmask, amask);
}

void ImageManager::load_image( string filename ) {
	cout << "Imagemanager: load_image was asked to load " << filename << endl;

    SDL_Surface* loadedImage = NULL;
    SDL_Surface* optimizedImage = NULL;
    loadedImage = IMG_Load( filename.c_str() );
    
    if( loadedImage != NULL )
    {
        cout << "Optimizing image" << endl;
        cout << "Loaded image is " << loadedImage->w << " X " << loadedImage->h << endl;
        optimizedImage = SDL_DisplayFormat( loadedImage );
        SDL_FreeSurface( loadedImage );

        if( optimizedImage != NULL )
        {
            Uint32 colorkey = SDL_MapRGB( optimizedImage->format, 0xFF, 0x00, 0xFF );
            SDL_SetColorKey( optimizedImage, SDL_SRCCOLORKEY, colorkey );
        }
    }

	cout << "Imagemanager: Loaded image is " << optimizedImage->w << " X " << optimizedImage->h << endl;
    sprites.push_back( optimizedImage );
}

int ImageManager::apply_surface( int x, int y, SDL_Surface* source, SDL_Surface* destination, SDL_Rect* clip )
{
    //Make a temporary rectangle to hold the offsets
    SDL_Rect offset;
	
	if (source == NULL || destination == NULL)
	{
		cout << "Imagemanager: One of the applied Surfaces was EMPTY!" << endl;
	}
	
    //Give the offsets to the rectangle
    offset.x = x;
    offset.y = y;
    //Blit the surface
    return SDL_BlitSurface( source, clip, destination, &offset );
}
int ImageManager::apply_surface( SDL_Rect* offset, SDL_Surface* source, SDL_Surface* destination, SDL_Rect* clip )
{   
    if (source == NULL || destination == NULL)
    {
        cout << "Imagemanager: One of the applied Surfaces was EMPTY!" << endl;
    }
    
    //Blit the surface
    return SDL_BlitSurface( source, clip, destination, offset );
}


void ImageManager::drawImage( int index, SDL_Rect position )
{
    SDL_Rect temp;
    temp.x = 0;
    temp.y = 0;
    temp.w = sprites[index]->w;
    temp.h = sprites[index]->h;

    SDL_Rect offset;
    offset.x = position.x;
    offset.y = position.y;


    SDL_BlitSurface( sprites[index], &temp, buffer, &offset );
}
void ImageManager::drawImage( int index, int x, int y )
{
    SDL_Rect temp;
    temp.x = 0;
    temp.y = 0;
    temp.w = sprites[index]->w;
    temp.h = sprites[index]->h;

    SDL_Rect offset;
    offset.x = x;
    offset.y = y;


    SDL_BlitSurface( sprites[index], &temp, buffer, &offset );
}

void ImageManager::drawRect( SDL_Rect rect, int r, int g, int b )
{
    SDL_FillRect( buffer, NULL,  SDL_MapRGB( buffer->format, r, g, b));// Fills the surface with lightblue
}

SDL_Surface* ImageManager::getBuffer()
{
    return buffer;
}