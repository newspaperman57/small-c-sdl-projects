#ifndef _IMAGEMANAGER
#define _IMAGEMANAGER

#include "SDL/SDL.h"
#include "SDL/SDL_image.h"

#include <string>
#include <iostream>
#include <vector>

using namespace std;

class ImageManager
{
private:
	SDL_Surface* buffer;
	
public:
	ImageManager();
	~ImageManager();
	void init( SDL_Rect );

	void load_image( string filename ); // Loads images

	vector<SDL_Surface*> sprites;
	
	
	//Apply a surface to another. Often used for applying surface to display
	int apply_surface( int x, int y, SDL_Surface* source, SDL_Surface* destination, SDL_Rect* clip );
	int apply_surface( SDL_Rect* offset, SDL_Surface* source, SDL_Surface* destination, SDL_Rect* clip );
	void drawImage( int, SDL_Rect );
	void drawImage( int, int, int );
	void drawRect( SDL_Rect, int, int, int );
	SDL_Surface* getBuffer();
};

#endif