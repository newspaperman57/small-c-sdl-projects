#include "collisionDetect.h"

using namespace std;

CollisionDetect::CollisionDetect()
{
	cout << "CollisionDetect: A collisionDetect inctance got initialized" << endl;
}

CollisionDetect::~CollisionDetect()
{
	cout << "CollisionDetect: Instance got destroyed!" << endl;
}

bool CollisionDetect::isCollision( SDL_Rect a, SDL_Rect b )
{
	if ( a.y + a.h < b.y || a.y > b.y + b.h || a.x + a.w < b.x || a.x > b.x + b.w )
	{
		return false;
	}
	else
	{
		return true;
	}
}

bool CollisionDetect::isInside( SDL_Rect content, SDL_Rect container )
{
	if ( content.x < container.x || content.y < container.y || content.x + content.w > container.x + container.w || content.y + content.h > container.y + container.h )
	{
		return false;
	}
	else
	{
		return true;
	}
}