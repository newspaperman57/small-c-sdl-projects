#include "timer.h"

using namespace std;

Timer::Timer()
{
	//Initialize the variables	
	cout << "Timer instance will now be greated" << endl;
	startTicks = 0;
	pausedTicks = 0;
	paused = false;
	started = false;
}

Timer::~Timer()
{
	cout << "Timer: Instance got destroyed!" << endl;
}

void Timer::Start()
{
	//Start the timer
	started = true;

	//Unpause the timer
	paused = false;

	//Get the current clock time
	startTicks = SDL_GetTicks();
}

void Timer::Stop()
{
	//Stop the timer
	started = false;

	//Unpause the timer
	paused = false;
}

int Timer::Get_ticks()
{
	//If runnning
	if( started == true )
	{
		//If paused
		if( paused == true )
		{
			//Return Ticks at paused time
			return pausedTicks;
		}
		else
		{
			//Return current time minus starting time
			return SDL_GetTicks() - startTicks;
		}
	}
	//If timer isn't running
	return 0;
}

void Timer::Pause()
{
	//If timer is running and isn't paused
	if( (started == true ) && ( paused == false ) )
	{
		//Pause timer
		paused = true;

		//Calculate pause ticks
		pausedTicks = SDL_GetTicks() - startTicks;
	}
}

void Timer::Unpause()
{
	//If timer is paused
	if( paused == true )
	{
		//unpause
		paused = false;

		// reset the starting ticks
		startTicks = SDL_GetTicks() - pausedTicks;

		//Reset pausedTicks
		pausedTicks = 0;
	}
}

bool Timer::is_started()
{
	return started;
}

bool Timer::is_paused()
{
	return paused;
}