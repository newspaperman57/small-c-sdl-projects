#ifndef _GAME_
#define _GAME_

#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "player.h"
#include "imagemanager.h"
#include "timer.h"
#include "box.h"
#include "collisionDetect.h"
#include <iostream>

class Game
{
private:
	enum { 
			SKY = 0, 
			FENCE = 1, 
			GRASS = 2,
			PLAYER = 3,
			CHEESE = 4,
			TRIANGLE = 5,
			SQUARE = 6,
			PENTAGON = 7,
			HEXAGON = 8 
		  };

	SDL_Surface* display;
	SDL_Surface* background1;
	SDL_Surface* background2;

	SDL_Rect screenDimension;
	SDL_Rect ground;

	bool quitGame;
	bool useFrameCap;
	int FPS;
	

	ImageManager imageManager;
	Timer timer;
	Player player;
	Box box;
	CollisionDetect collisionDetect;

	void initialize();

	void loadAssets();
	void compose();

	void processEvents();
	void update();
	void render();

public:
	Game();
	~Game();

	void go();

};

#endif