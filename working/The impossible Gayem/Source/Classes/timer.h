#ifndef _TIMER
#define _TIMER

#include "SDL/SDL.h"

#include <iostream>

class Timer
{
private:
	//Clock time when timer started
	int startTicks;

	//Ticks stored when timer was paused
	int pausedTicks;

	//Timer statuses
	bool paused;
	bool started;

public:
	//Initializes variables
	Timer();
	~Timer();

	//Clock actions
	void Start();
	void Stop();
	void Pause();
	void Unpause();

	//Gets Timer - time
	int Get_ticks();

	//Checks the status of the timer
	bool is_started();
	bool is_paused();
};

#endif