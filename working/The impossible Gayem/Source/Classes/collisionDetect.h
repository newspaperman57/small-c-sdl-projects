#ifndef _COLLISION_DETECT
#define _COLLISION_DETECT

#include "SDL/SDL.h"

#include <iostream>

class CollisionDetect
{

public:
	CollisionDetect();
	~CollisionDetect();
	bool isCollision( SDL_Rect, SDL_Rect );
	bool isInside( SDL_Rect, SDL_Rect );
};

#endif