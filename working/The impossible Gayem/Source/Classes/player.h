#ifndef _PLAYER
#define _PLAYER

#include "SDL/SDL.h"

#include <iostream>

#include "collisionDetect.h"


class Player
{
private:
	int life;
	SDL_Rect move; // For moving the player.
	SDL_Rect pos; // Player Position.

	float grav;
	float weight;
	float acceleration;
	float xspeed;
	float yspeed;
	float startSpeed;
	float maxSpeed;
	float jumpForce;
	float jumpPower;

	int angle;


	CollisionDetect collision;
public:

	Player();
	~Player();

	int getLife();
	int getAngle();
	float getxspeed();
	SDL_Rect getPos();
	
	void update( SDL_Rect );
	
	void setPos( SDL_Rect posi );
	void setPos( int x, int y );

	void setMove( SDL_Rect movem );
	void setMove( int x, int y );
};

#endif