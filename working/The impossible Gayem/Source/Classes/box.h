#ifndef _BOX
#define _BOX

#include "SDL/SDL.h"

#include <iostream>

class Box
{
	private:
		void move( int, int );
		SDL_Rect pos;

	public:
		Box();
		~Box();
	
		void init();
		void update();
		SDL_Rect getPos();
};

#endif