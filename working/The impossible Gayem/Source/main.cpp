#include "Classes/game.h"

#undef main

int main( int argc, char* args[] ) 
{
	Game game; 		// Iniitalizes the class 'Game' as 'game'
	game.go(); 		// Starts the game
	return 0; 		// Ends the program
}