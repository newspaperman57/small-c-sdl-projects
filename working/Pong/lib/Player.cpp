#include "../include/Player.h"

Player::Player( int up, int down, int x )
{
	int SCREENHIGHT = 800;
	int SCREENWIDTH = 1200;
	playerDimen.w = 20;
	playerDimen.h = SCREENHIGHT/5;
	playerDimen.x = x;
	playerDimen.y = ( SCREENHIGHT - playerDimen.h ) / 2;

	RGB[0] = 0;
	RGB[1] = 255;
	RGB[2] = 100;

	minSpeed = 1;
	maxSpeed = 5;
	veloc = minSpeed;
	acceleration = 0.2;

	moving = false;
	keyUp = up;
	KeyDown = down;

}
Player::~Player()
{
	cout << "Destructed Player" << endl;
}

SDL_Rect Player::getPlayerDimen()
{
	return playerDimen;
}
int* Player::getColor()
{
	int* p_color;
	p_color = RGB;
	return p_color;
}
void Player::update( Uint8* keystates )
{
	if ( keystates[ keyUp ])
	{
		if (moving == true )
		{
			if ( veloc < maxSpeed )
			{
				veloc += acceleration;
			}	
		}
		playerDimen.y -= veloc;
		moving = true;
	}
	else if ( keystates[ KeyDown ])
	{
		if (moving == true )
		{
			if ( veloc < maxSpeed )
			{
				veloc += acceleration;
			}
		}
		playerDimen.y += veloc;
		moving = true;
	}

	else if ( keystates[ KeyDown ] == false && keystates[ keyUp ] == false )
	{
		
		moving = false;
		veloc = minSpeed;
	}

	if ( playerDimen.y + playerDimen.h > 600 )
	{
		playerDimen.y = 600 - playerDimen.h;
		moving = false;
		veloc = minSpeed;
	}
	if ( playerDimen.x + playerDimen.w > 800 )
	{
		playerDimen.x = 800 - playerDimen.w;
		moving = false;
		veloc = minSpeed;
	}
	if ( playerDimen.y < 0 )
	{
		playerDimen.y = 0;
		moving = false;
		veloc = minSpeed;
	}
	if ( playerDimen.x < 0 )
	{
		playerDimen.x = 0;
		moving = false;
		veloc = minSpeed;
	}
}