#include "../include/Ball.h"

Ball::Ball()
{
	dimen.w = 20;
	dimen.h = 20;
	dimen.x = 400; - (dimen.w / 2);
	dimen.y = 300; - (dimen.h / 2);

	RGB[0] = 255;
	RGB[1] = 255;
	RGB[2] = 255;

	veloX = -7;
	veloY = 0.2;
}
Ball::~Ball()
{

}

SDL_Rect Ball::getDimen()
{
	return dimen;
}
int* Ball::getColor()
{
	int* p_color;
	p_color = RGB;
	return p_color;
}
void Ball::update()
{
	dimen.x += veloX;
	dimen.y += veloY;

}
void Ball::returnBall()
{
	veloX = veloX * (-1);
}