#include "../include/Engine.h"

bool isCollision( SDL_Rect a, SDL_Rect b )
{
	if ( a.y + a.h < b.y )
	{
		return false;
	}
	else if ( a.y > b.y + b.h )
	{
		return false;
	}
	else if ( a.x + a.w < b.x )
	{
		return false;
	}
	else if ( a.x > b.x + b.w )
	{
		return false;
	}
	else
	{
		return true;
	}

}


//Public
Engine::Engine()
{
	SDL_Init( SDL_INIT_EVERYTHING );
	TTF_Init();
	screen = SDL_SetVideoMode( screenW, screenH, 32, SDL_SWSURFACE );
	SDL_WM_SetCaption( "Engine By Anton Christensen - Pong", NULL );
	gameRunning = true;
	cout << "initilised engine" << endl;
}
Engine::~Engine()
{
	//Destruct objects here
	graphics->~Graphics();
	playerA.~Player();
	playerB.~Player();

	//Free surfaces here
	SDL_FreeSurface( screen );

	TTF_Quit();
	SDL_Quit();
	cout << "Desctructed engine" << endl;
}
void Engine::go()
{
	graphics->loadImage("assets/img/player_big.png");
	//Here should be a load assets function witch later should be in the map loader object



	fps.start();
	updateCaption.start();
	mainLoop();
}
//Private
void Engine::mainLoop()
{
	while( gameRunning )
	{
		processEvents();
		update();
		render();
	}
}
void Engine::processEvents()
{
	SDL_Event event;
	if (SDL_PollEvent( &event ))
	{
		if ( event.type == SDL_QUIT )
		{
			gameRunning = false;
			cout << "gameRunning = false" << endl;
		}
		//Events go here
	}
	playerA.update( SDL_GetKeyState( NULL ) );
	playerB.update( SDL_GetKeyState( NULL ) );
	ball->update();

}
void Engine::update()
{
	if ( ball->getDimen().x < 1 )
	{
		cout << "point for right" << endl;
		ball->~Ball();
		ball = new Ball;
		
		scoreR++;
	}
	if ( ball->getDimen().x + ball->getDimen().w > 799 )
	{
		cout << "point for left" << endl;
		ball->~Ball();
		ball = new Ball;

		scoreL++;
	}
	if ( isCollision(playerA.getPlayerDimen(), ball->getDimen() ) || isCollision(playerB.getPlayerDimen(), ball->getDimen() ) )
	{
		ball->returnBall();
	}

	
}
void Engine::render()
{
	SDL_FillRect( screen, NULL,SDL_MapRGB(screen->format, 50, 055,255 )); //Fill with bluescreen color
	SDL_FillRect( graphics->getBuffer(), NULL,SDL_MapRGB(screen->format, 50, 055,255 ));
	dubleBackBuffer();	
	SDL_BlitSurface( graphics->getBuffer(), NULL, screen, NULL );

	SDL_Flip( screen );

	frame++;
	if( ( cap == true ) && ( fps.get_ticks() < 1000 / FPS ) )
    {
        //Sleep the remaining frame time
        SDL_Delay( ( 1000 / FPS ) - fps.get_ticks() );
    }

	//If a second has passed since the caption was last updated
    if( updateCaption.get_ticks() > 1000 )
    {
        //The frame rate as a string
        stringstream caption;
        
        //Calculate the frames per second and create the string
        caption << "Average Frames Per Second: " << frame / ( fps.get_ticks() / 1000.f );
        
        //Reset the caption
        SDL_WM_SetCaption( caption.str().c_str(), NULL );
        
        //Restart the update timer
        updateCaption.start();    
    }
}
void Engine::dubleBackBuffer()
{
	graphics->drawImage( 0,0,0 );
	graphics->drawRect( playerA.getPlayerDimen(), playerA.getColor() );
	graphics->drawRect( playerB.getPlayerDimen(), playerB.getColor() );
	graphics->drawRect( ball->getDimen(), ball->getColor() );

    stringstream time;
    time << "Timer: " << fps.get_ticks() / 1000.f;
    drawText( graphics->getBuffer(), time.str(), 0, 0, 25, {255,0,0});

	string score;
	stringstream out;
	out << scoreL;
	score = out.str();
	drawText( graphics->getBuffer(), score, 350, 20, 50, {255,0,0});
	out.str( std::string() );
	out.clear();
	out << scoreR;
	score = out.str();
	drawText( graphics->getBuffer(), score, 450, 20, 50, {255,0,0});
}


void Engine::drawText( SDL_Surface* destination, string msg, int x, int y, int size, SDL_Color color)
{
	TTF_Font* font;
	font = TTF_OpenFont("assets/font/Eurosti.TTF", size);
	if(!font)
		cout << "Error loading font" << endl;

	SDL_Rect coordinates;
	coordinates.x = x;
	coordinates.y = y;

	SDL_Surface* message = NULL;
	message = TTF_RenderText_Solid(font, msg.c_str(), color);

	SDL_BlitSurface(message, NULL, destination, &coordinates);

	TTF_CloseFont(font);
	SDL_FreeSurface(message);
}