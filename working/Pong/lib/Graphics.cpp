#include "../include/Graphics.h"

#if SDL_BYTEORDER == SDL_BIG_ENDIAN
    int rmask = 0xff000000;
    int gmask = 0x00ff0000;
    int bmask = 0x0000ff00;
    int amask = 0x000000ff;
#else
    int rmask = 0x000000ff;
    int gmask = 0x0000ff00;
    int bmask = 0x00ff0000;
    int amask = 0xff000000;
#endif

//Public
Graphics::Graphics( int bufferW, int bufferH)
{
	buffer = SDL_CreateRGBSurface( SDL_HWSURFACE , bufferW, bufferH, 32, rmask, gmask, bmask, amask );
}
Graphics::~Graphics()
{
	//Free surfaces
	SDL_FreeSurface( buffer );

	for( int i = 0; i < sprites.size(); i++ )
	{
		SDL_FreeSurface( sprites[i] );
	}

	cout << "Destructed graphics" << endl;
}
void Graphics::loadImage( string filename )
{
	cout << "Loading " << filename << endl;

    SDL_Surface* loadedImage = NULL;
    SDL_Surface* optimizedImage = NULL;
    loadedImage = IMG_Load( filename.c_str() );

    //If the image loaded
   if( loadedImage != NULL )
    {
        optimizedImage = SDL_DisplayFormat( loadedImage );
        SDL_FreeSurface( loadedImage );
        if( optimizedImage != NULL )
        {
            //Map the color key
            Uint32 colorkey = SDL_MapRGB( optimizedImage->format, 0xFF, 0x00, 0xFF );
            //Set all pixels of color R 0xFF, G 0, B 0xFF to be transparent
            SDL_SetColorKey( optimizedImage, SDL_SRCCOLORKEY, colorkey );
        }
    }
    sprites.push_back( optimizedImage );
}
void Graphics::drawImage( int index, int x, int y )
{
	SDL_Rect temp;
	temp.x = x;
	temp.y = y;
	temp.w = sprites[index]->w;
	temp.h = sprites[index]->h;
	SDL_BlitSurface( sprites[index], &temp, getBuffer(), NULL );
}
void Graphics::drawRect( SDL_Rect rect, int* color )
{
    SDL_FillRect( buffer, &rect,SDL_MapRGB( buffer->format, color[0], color[1], color[2] ));
}
SDL_Surface* Graphics::getBuffer()
{
	return buffer;
}



void Graphics::apply_surface( int x, int y, SDL_Surface* source, SDL_Surface* destination, SDL_Rect* clip )
{
    //Make a temporary rectangle to hold the offsets
    SDL_Rect offset;

    //Give the offsets to the rectangle
    offset.x = x;
    offset.y = y;

    //Blit the surface
    SDL_BlitSurface( source, clip, destination, &offset );
}