#ifndef _BALL
#define _BALL

#include <SDL/SDL.h>
#include "iostream"

using namespace std;

class Ball
{
private:
	int RGB[3]; //color
	float veloX, veloY;

public:
	SDL_Rect dimen;
	Ball();
	~Ball();
	SDL_Rect getDimen();
	int* getColor();
	void update();
	void returnBall();
};

#endif