#ifndef _PLAYER
#define _PLAYER

#include <SDL/SDL.h>
#include <iostream>

using namespace std;

class Player
{
private:
	int RGB[3]; //color
	float minSpeed;
	float maxSpeed;
	float veloc;
	float acceleration;
	bool moving;

	int keyUp;
	int KeyDown;

public:
	SDL_Rect playerDimen;
	Player( int, int, int );
	~Player();
	SDL_Rect getPlayerDimen();
	int* getColor();
	void update( Uint8* );
	
};

#endif