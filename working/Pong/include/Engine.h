#ifndef _ENGINE
#define _ENGINE

#include <iostream>
#include <string>
#include <sstream>

#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>
#include "../lib/Timer.cpp"
#include "../lib/Graphics.cpp"
#include "../lib/Player.cpp"
#include "../lib/Ball.cpp"

using namespace std;

class Engine
{
private:
	void mainLoop();
	void processEvents();
	void update();
	void render();
	void dubleBackBuffer();

	void drawText(SDL_Surface*, string, int, int, int, SDL_Color);

	bool gameRunning;
	SDL_Surface* screen = NULL;
	Timer fps;
	Timer updateCaption;
	Graphics* graphics = new Graphics( screenW, screenH );
	Player playerA = Player( 119, 115, 20 );
	Player playerB = Player( 273, 274, screenW - 20 );
	Ball* ball = new Ball;

	int scoreL = 0;
	int scoreR = 0;


	//Keep track of the current frame
    int frame = 0;
    
    //Whether or not to cap the frame rate
    bool cap = true;
    const int FPS = 60;
	

public:
	static const int screenW = 1200;
	static const int screenH = 800;
	//Graphics graphics;
	Engine();
	~Engine();
	void go();
	
};

#endif