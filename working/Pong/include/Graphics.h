#ifndef _GRAPHICS
#define _GRAPHICS

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>

#include <vector>
#include <iostream>

using namespace std;

class Graphics
{
private:
	int spriteindex;

    SDL_Surface* buffer;		//Duble back buffering thats why!

    

public:
	Graphics( int, int );
	~Graphics();

	vector<SDL_Surface*> sprites;

	void loadImage( string filename );
	void drawImage( int index, int x, int y );
	void drawRect( SDL_Rect, int* );
	SDL_Surface* getBuffer();
	void apply_surface( int x, int y, SDL_Surface* source, SDL_Surface* destination, SDL_Rect* clip );
};

#endif