#include <SDL/SDL.h>

#include "lib/Engine.cpp"

#undef main

int main( int argc, char* args[] )
{

	Engine engine;
	engine.go();
	//engine.~Engine();

	return 0;
}