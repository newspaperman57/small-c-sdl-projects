#include "../Include/GStateGame.h"
#include "../Include/GStateManager.h"

GStateGame GStateGame::Instance;
 
GStateGame::GStateGame() {
	cout << "[GSTATE_GAME] Constructing..." << endl;
	
	CfgReader cfgReader;
	cfgReader.LoadFile("config");
	WWIDTH = cfgReader.GetInt("Screen_Width");
	WHEIGHT = cfgReader.GetInt("Screen_Height");
	TileSize = cfgReader.GetInt("Tile_Size");
	BorderSize = cfgReader.GetInt("Tile_Border");
	currentTile.gradient = cfgReader.GetInt("Gradient");
	xTiles = WWIDTH / (TileSize + BorderSize);
	if(xTiles*(TileSize + BorderSize)+BorderSize > WWIDTH)
		xTiles--;
	yTiles = WHEIGHT / (TileSize + BorderSize);
	if(yTiles*(TileSize + BorderSize)+BorderSize > WHEIGHT)
		yTiles--;
	
	srand((unsigned)time(0));
}

GStateGame::~GStateGame() {
	if(background != NULL)
		SDL_FreeSurface(background);
	SDL_FreeSurface(info_surf);
}

Tile* GStateGame::GetTile(int x, int y)
{
	return &tiles[y*xTiles+x];
}

void GStateGame::OnKeyDown(SDLKey sym, SDLMod mod, Uint16 unicode)
{
	switch(sym){
		case SDLK_0:
			currentTile.color = white;
			break;
		case SDLK_1:
			currentTile.color = red;
			break;
		case SDLK_2:
			currentTile.color = green;
			break;
		case SDLK_3:
			currentTile.color = blue;
			break;
		case SDLK_q:
			Debug::Dbug("Toggle render");
			Debug::ToggleRender();
			break;
		case SDLK_c:
			GStateManager::SetActiveGState(GSTATE_GAME);
			break;
		case SDLK_RIGHT:
			GStateManager::SetActiveGState(GSTATE_LIFE);
			break;
		case SDLK_LEFT:
			GStateManager::SetActiveGState(GSTATE_LIFE);
			break;
			
			//OnActivate();
	}
}

void GStateGame::OnMouseMove(int mX, int mY, int relX, int relY, bool Left, bool Right, bool Middle) 
{
	int x = (mX-BorderSize) / (TileSize+BorderSize);
	int y = (mY-BorderSize) / (TileSize+BorderSize);
	tileIndex = -1;
	if(x < xTiles && y < yTiles)
	{
		tileIndex = y * xTiles + x;
		currentTile.x = tiles[tileIndex%xTiles].x;
		currentTile.y = tiles[tileIndex-tileIndex%xTiles].y;
	}
	if(Left)
	{
		tiles[tileIndex] = currentTile;
	}
	//mapEdit.OnMouseMove(mX, mY);
}
void GStateGame::OnLButtonDown(int mX, int mY)
{
	tiles[tileIndex] = currentTile;
}

void GStateGame::OnMouseWheel(bool Up, bool Down)
{
	switch(currentTile.color){
		case white:
			if(Up)
				currentTile.color = blue;
			if(Down)
				currentTile.color = red;
			break;
		case red:
			if(Up)
				currentTile.color = white;
			if(Down)
				currentTile.color = green;
			break;
		case green:
			if(Up)
				currentTile.color = red;
			if(Down)
				currentTile.color = blue;
			break;
		case blue:
			if(Up)
				currentTile.color = green;
			if(Down)
				currentTile.color = white;
			break;
	}
}

void GStateGame::OnActivate() 
{
	info.padding = true;
	info.xPadding = 5;
	info.yPadding = 5;
	info_surf = info.RenderText("#--------------------------#\n|       Game of Life       |\n#--------------------------#\n| 1:Red                    |\n| 2:Green                  |\n| 3:Blue                   |\n| Arrows: Change Automaton |\n#--------------------------#");

/*
#--------------------------#
|       Game of Life       |
#--------------------------#
| 1:Red                    |
| 2:Green                  |
| 3:Blue                   |
| Arrows: Change Automaton |
#--------------------------#
*/
	
	
	currentTile.color = red;
	currentTile.changed = true;
	tileIndex = -1;
	background = Surface::MakeEmptySurface(xTiles*TileSize+(xTiles+1)*BorderSize,yTiles*TileSize+(yTiles+1)*BorderSize);
	Surface::FillRect(background, NULL,0x00000000);
	for(int y = 0; y < yTiles; y++)
	{
		for(int x = 0; x < xTiles; x++)
		{
			Tile temp;
			temp.gradient = 0;
			temp.color = white;
			temp.x = x;
			temp.y = y;
			temp.changed = true;
			tiles.push_back(temp);
		}
	}
}

void GStateGame::OnDeactivate() {
	if(tiles.size() > 0)
		tiles.clear();
}

void GStateGame::OnLoop()
{
	for(int i = 0; i < tiles.size(); i++)
	{
		if(tiles[i].gradient < 1)
		{
			tiles[i].color = white;
			tiles[i].gradient = 0;
		}
		if(tiles[i].gradient > currentTile.gradient)
			tiles[i].gradient = currentTile.gradient;
		
		int x,y;
		int foo;
		foo = rand()%8;
		Tile* bar;
		//Get random neighbor
		switch(foo)
		{
			case 0:
				x = tiles[i].x-1;
				y = tiles[i].y-1;
				break;
			case 1:
				x = tiles[i].x;
				y = tiles[i].y-1;
				break;
			case 2:
				x = tiles[i].x+1;
				y = tiles[i].y-1;
				break;
			case 3:
				x = tiles[i].x-1;
				y = tiles[i].y;
				break;
			case 4:
				x = tiles[i].x+1;
				y = tiles[i].y;
				break;
			case 5:
				x = tiles[i].x-1;
				y = tiles[i].y+1;
				break;
			case 6:
				x = tiles[i].x;
				y = tiles[i].y+1;
				break;
			case 7:
				x = tiles[i].x+1;
				y = tiles[i].y+1;
				break;
		}
		if(x >= 0 && x <= xTiles && y >= 0 && y <= yTiles ) {
			bar = GetTile(x,y);
			if(bar->color != white && bar->gradient > 1 && bar->color != tiles[i].color)
			{
				switch(tiles[i].color)
				{
				case white:
					if(bar->gradient > 1)
					{
						tiles[i].color = bar->color;
						tiles[i].gradient = bar->gradient -1;
						tiles[i].changed = true;
					}
					break;
				case red:
					if(bar->color == blue)
					{
						tiles[i].gradient = currentTile.gradient;
						bar->gradient = 0;
						bar->color = white;
						bar->changed = true;
					}
					break;
				case green:
					if(bar->color == red)
					{
						tiles[i].gradient = currentTile.gradient;
						bar->gradient = 0;
						bar->color = white;
						bar->changed = true;
					}
					break;
				case blue:
					if(bar->color == green)
					{
						tiles[i].gradient = currentTile.gradient;
						bar->gradient = 0;
						bar->color = white;
						bar->changed = true;
					}
					break;
				}
			}
		}
		else
			continue;
	}
}
	
void GStateGame::OnRender(SDL_Surface* Surf_Display) {
	int renderings = 0;
	SDL_Rect temp;
	temp.w = TileSize;
	temp.h = TileSize;
	for(int i = 0; i < tiles.size(); i++)
	{
		if(!tiles[i].changed)
			continue;
		temp.x = BorderSize+tiles[i].x*(TileSize+BorderSize);
		temp.y = BorderSize+tiles[i].y*(TileSize+BorderSize);
		Surface::FillRect(background, &temp,tiles[i].color);
		tiles[i].changed = false;
		renderings++;
	}
	Debug::Dbug("Renderings this loop: " + to_string(renderings));
	
	temp.x = BorderSize+currentTile.x*(TileSize+BorderSize);
	temp.y = BorderSize+currentTile.y*(TileSize+BorderSize);
	
	Surface::OnDraw(Surf_Display, background,0,0);
	Surface::FillRect(Surf_Display, &temp,currentTile.color );
	Surface::OnDraw(Surf_Display, info_surf,WWIDTH-info_surf->w-15,15);
}

GStateGame* GStateGame::GetInstance() {
    return &Instance;	
}