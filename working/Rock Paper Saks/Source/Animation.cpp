#include "../Include/Animation.h"

Animation::Animation() {
    CurrentFrame    = 0;
    MaxFrames       = 0;
    FrameInc        = 1;
	
	FrameDelay		= 0;
	CurrentDelay 	= 0;
 
    Oscillate       = false;
}

void Animation::OnAnimate() {
	if(CurrentDelay == FrameDelay)
	{
		CurrentFrame += FrameInc;
	 
		if(Oscillate) {
			if(FrameInc > 0) {
				if(CurrentFrame >= MaxFrames) {
					FrameInc = -FrameInc;
				}
			}else{
				if(CurrentFrame <= 0) {
					FrameInc = -FrameInc;
				}
			}
		}else{
			if(CurrentFrame >= MaxFrames) {
				CurrentFrame = 0;
			}
		}
	CurrentDelay = 0;
	}
	else
	{
		CurrentDelay++;
	}
}
 
void Animation::SetCurrentFrame(int Frame) {
    if(Frame < 0 || Frame >= MaxFrames) 
		cout << "[ANIMATION] ERROR: Invalid frame!" << endl; return;
 
    CurrentFrame = Frame;
}
 
int Animation::GetCurrentFrame() {
    return CurrentFrame;
}