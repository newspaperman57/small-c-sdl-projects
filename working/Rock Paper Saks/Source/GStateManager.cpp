#include "../Include/GStateManager.h"
 
// Refer to Other Game States Here
#include "../Include/GStateGame.h"
#include "../Include/GStateLife.h"
 
GState* GStateManager::ActiveGState = 0;
 
void GStateManager::OnEvent(SDL_Event* EventHolder) {
    if(ActiveGState) ActiveGState->OnEvent(EventHolder);
}
 
void GStateManager::OnLoop() {
    if(ActiveGState) ActiveGState->OnLoop();
}
 
void GStateManager::OnRender(SDL_Surface* Surf_Display) {
    if(ActiveGState) ActiveGState->OnRender(Surf_Display);
}
 
void GStateManager::SetActiveGState(int GStateID) {
    if(ActiveGState) ActiveGState->OnDeactivate();
 
    // Also, add your Game State Here so that the Manager can switch to it
    if(GStateID == GSTATE_NONE)     ActiveGState = 0;
    if(GStateID == GSTATE_GAME)     ActiveGState = GStateGame::GetInstance();
    if(GStateID == GSTATE_LIFE)     ActiveGState = GStateLife::GetInstance();
	
    if(ActiveGState) ActiveGState->OnActivate();
}
 
GState* GStateManager::GetActiveGState() {
    return ActiveGState;
}