#include "../Include/GStateLife.h"
#include "../Include/GStateManager.h"

GStateLife GStateLife::Instance;
 
GStateLife::GStateLife() {
	cout << "[GSTATE_GAME] Constructing..." << endl;
	
	CfgReader cfgReader;
	cfgReader.LoadFile("config");
	WWIDTH = cfgReader.GetInt("Screen_Width");
	WHEIGHT = cfgReader.GetInt("Screen_Height");
	TileSize = cfgReader.GetInt("Tile_Size");
	BorderSize = cfgReader.GetInt("Tile_Border");
	xTiles = WWIDTH / (TileSize + BorderSize);
	if(xTiles*(TileSize + BorderSize)+BorderSize > WWIDTH)
		xTiles--;
	yTiles = WHEIGHT / (TileSize + BorderSize);
	if(yTiles*(TileSize + BorderSize)+BorderSize > WHEIGHT)
		yTiles--;
	srand((unsigned)time(0)); 
}

GStateLife::~GStateLife() {
	if(background != NULL)
		SDL_FreeSurface(background);
}

Tile_Life* GStateLife::GetTile(int x, int y)
{
	if(y*xTiles+x >= 0 && y*xTiles+x < analyst.size())
		return &analyst[y*xTiles+x];
	else
	{
		Tile_Life* temp = (Tile_Life*)malloc(sizeof(Tile_Life));
		temp->life=false;
		temp->x = 0;
		temp->y = 0;
		temp->changed = false;
		return temp;
	}
}

void GStateLife::OnKeyDown(SDLKey sym, SDLMod mod, Uint16 unicode)
{
	switch(sym){
		case SDLK_q:
			Debug::Dbug("Toggle render");
			Debug::ToggleRender();
			break;
		case SDLK_c:
			GStateManager::SetActiveGState(GSTATE_LIFE);
			break;
		case SDLK_RIGHT:
			GStateManager::SetActiveGState(GSTATE_GAME);
			break;
		case SDLK_LEFT:
			GStateManager::SetActiveGState(GSTATE_GAME);
			break;
		case SDLK_SPACE:
			paused = !paused;
			break;
			
	}
}

void GStateLife::OnMouseMove(int mX, int mY, int relX, int relY, bool Left, bool Right, bool Middle) 
{
	int x = (mX-BorderSize) / (TileSize+BorderSize);
	int y = (mY-BorderSize) / (TileSize+BorderSize);
	tileIndex = -1;
	if(x < xTiles && y < yTiles)
	{
		tileIndex = y * xTiles + x;
		currentTile.x = tiles[tileIndex%xTiles].x;
		currentTile.y = tiles[tileIndex-tileIndex%xTiles].y;
	}
	if(Left)
	{
		tiles[tileIndex] = currentTile;
	}
	//mapEdit.OnMouseMove(mX, mY);
}
void GStateLife::OnLButtonDown(int mX, int mY)
{
	tiles[tileIndex] = currentTile;
}

void GStateLife::OnActivate() 
{
	paused = false;
	currentTile.life = true;
	currentTile.changed = true;
	tileIndex = -1;
	background = Surface::MakeEmptySurface(xTiles*TileSize+(xTiles+1)*BorderSize,yTiles*TileSize+(yTiles+1)*BorderSize);
	Surface::FillRect(background, NULL,0x00000000);
	
	Tile_Life temp_late;
	temp_late.life = false;
	temp_late.changed = true;
	
	for(int y = 0; y < yTiles; y++)
	{
		for(int x = 0; x < xTiles; x++)
		{
			temp_late.x = x;
			temp_late.y = y;
			Tile_Life temp;
			temp = temp_late;
			tiles.push_back(temp);
		}
	}
	analyst = tiles;
}

void GStateLife::OnDeactivate() {
	if(tiles.size() > 0)
		tiles.clear();
}

void GStateLife::OnLoop()
{
	if(paused)
		return;
	analyst = tiles;
	for(int i = 0; i < analyst.size(); i++)
	{
		int x,y;
		int neighbors = 0;
		//count neighbors
		for(int j = 0; j < 8; j++)
		{
			switch(j)
			{
				case 0:
					if(GetTile(analyst[i].x-1,analyst[i].y-1)->life)
						neighbors++;
					break;
				case 1:
					if(GetTile(analyst[i].x,analyst[i].y-1)->life)
						neighbors++;
					break;
				case 2:
					if(GetTile(analyst[i].x+1,analyst[i].y-1)->life)
						neighbors++;
					break;
				case 3:
					if(GetTile(analyst[i].x-1,analyst[i].y)->life)
						neighbors++;
					break;
				case 4:
					if(GetTile(analyst[i].x+1,analyst[i].y)->life)
						neighbors++;
					break;
				case 5:
					if(GetTile(analyst[i].x-1,analyst[i].y+1)->life)
						neighbors++;
					break;
				case 6:
					if(GetTile(analyst[i].x,analyst[i].y+1)->life)
						neighbors++;
					break;
				case 7:
					if(GetTile(analyst[i].x+1,analyst[i].y+1)->life)
						neighbors++;
					break;
			}
		}
		switch(tiles[i].life)
		{
			case true:
				if(neighbors <= 1 || neighbors >= 4)
				{
					tiles[i].life = false;
					tiles[i].changed = true;
				}
				break;
			case false:
				if(neighbors == 3)
				{
					tiles[i].life = true;
					tiles[i].changed = true;
				}
				break;
		}
	}
}
	
void GStateLife::OnRender(SDL_Surface* Surf_Display) {
	int renderings = 0;
	SDL_Rect temp;
	temp.w = TileSize;
	temp.h = TileSize;
	for(int i = 0; i < tiles.size(); i++)
	{
		if(!tiles[i].changed)
			continue;
		temp.x = BorderSize+tiles[i].x*(TileSize+BorderSize);
		temp.y = BorderSize+tiles[i].y*(TileSize+BorderSize);
		if(tiles[i].life) {Surface::FillRect(background, &temp,0x222222FF);}
		else {Surface::FillRect(background, &temp,0xFFFFFFFF);}
		
		tiles[i].changed = false;
		renderings++;
	}
	Debug::Dbug("Renderings this loop: " + to_string(renderings));
	
	temp.x = BorderSize+currentTile.x*(TileSize+BorderSize);
	temp.y = BorderSize+currentTile.y*(TileSize+BorderSize);
	
	Surface::OnDraw(Surf_Display, background,0,0);
	Surface::FillRect(Surf_Display, &temp, 0x222222FF );
}

GStateLife* GStateLife::GetInstance() {
    return &Instance;	
}