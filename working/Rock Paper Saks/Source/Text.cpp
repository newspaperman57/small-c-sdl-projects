#include "../Include/Text.h"

Text::Text()
{
	TTF_Font* font = NULL;
	xShadow = yShadow = xPadding = yPadding = 0;
	transparent = padding = shadow = false;
	fontPath = "Fonts/anonymous.ttf";
	textColor = {0xFF,0xFF,0xFF};
	fontSize = 10;
}

Text::~Text()
{
	TTF_CloseFont( font );
}

SDL_Surface* Text::RenderText(const char* text)
{
	
	//subdevide text
	vector<size_t> newLines;
	size_t temp = 0;
	newLines.push_back(temp);
	string search = text;
	int i = 0;
	
	//infinite loop
	for(;;){
		size_t temp = search.find("\n", newLines[i]);
		i++;
		if(temp == string::npos)
			break;
		temp++;
		newLines.push_back(temp);
	}
	vector<string> strings;
	for(int i = 0; i < newLines.size(); i++)
	{
		strings.push_back(search.substr(newLines[i], newLines[i+1]-newLines[i]-1));
	}
	
	
	ResetFont();
	int w,h;
	SDL_Surface* return_surf = NULL;
	//const char* constText = text.c_str();
	
	for(int i = 0, temW = 0, temH = 0; i < newLines.size(); i++)
	{
		if( TTF_SizeText(font, strings[i].c_str(), &temW,&temH) == -1 )
			cout << "[TEXT] [ERROR] " << __LINE__ << endl;
		//text = strings[i].c_str();
		h += temH;
		if(temW > w)
			w = temW;
	}
	
	if(shadow)
	{
		SDL_Surface* surf_text;
		SDL_Surface* surf_shadow;
		//Make surface
		return_surf = Surface::MakeEmptySurface(w+xShadow,h+yShadow);
		//Make transparent background
		Surface::FillRect(return_surf, NULL, 0xFF, 0x00, 0xFF);
		Surface::SetColorKey(return_surf, 0xFF, 0x00, 0xFF);
		
		//Render text shadow
		surf_shadow = Surface::MakeEmptySurface(w,h);
		surf_shadow = TTF_RenderText_Solid( font, text, {0,0,0} );
		
		//This might be unnecesary
		surf_text = Surface::MakeEmptySurface(w,h);
		if ( surf_text == NULL )
			cout << "[TEXT] [ERROR] " << __LINE__ << endl;
		
		surf_text = TTF_RenderText_Solid( font, text, textColor );
		if ( surf_text == NULL)
			cout << "[TEXT] [ERROR] " << __LINE__ << endl;
			
		//Write to surface
		if(xShadow >= 0 && yShadow >= 0){
			Surface::OnDraw(return_surf, surf_shadow, xShadow,yShadow);
			Surface::OnDraw(return_surf, surf_text, 0,0);
		}
		else if(xShadow >= 0 && yShadow < 0) {
			Surface::OnDraw(return_surf, surf_shadow, xShadow,0);
			Surface::OnDraw(return_surf, surf_text, 0,abs(yShadow));
		}
		else if(xShadow < 0 && yShadow >= 0) {
			Surface::OnDraw(return_surf, surf_shadow, 0,yShadow);
			Surface::OnDraw(return_surf, surf_text, abs(xShadow),0);
		}
		else {
			Surface::OnDraw(return_surf, surf_shadow, 0,0);
			Surface::OnDraw(return_surf, surf_text, abs(xShadow),abs(yShadow));
		}
		
		SDL_FreeSurface(surf_text);
		SDL_FreeSurface(surf_shadow);
		
		return return_surf;
	}
	else if(padding)
	{
		vector<SDL_Surface*> surf_text;
		//Make surface
		return_surf = Surface::MakeEmptySurface(w+xPadding*2,h+yPadding*2);
		//Render background
		Surface::FillRect(return_surf, NULL, 0x00, 0x00, 0x00);
		
		
		
		for(int i = 0; i < strings.size(); i++)
		{
			//This might be unnecesary
			surf_text.push_back(Surface::MakeEmptySurface(w,h));
			if ( surf_text[i] == NULL )
				cout << "[TEXT] [ERROR] " << __LINE__ << endl;
			
			surf_text[i] = TTF_RenderText_Solid( font, strings[i].c_str(), textColor );
			if ( surf_text[i] == NULL)
				cout << "[TEXT] [ERROR] " << __LINE__ << endl;
		}	
		//Write to surface
		for(int i = 0; i < strings.size(); i++) {
			Surface::OnDraw(return_surf, surf_text[i], xPadding,yPadding+i*fontSize);
			//Free extra surfaces
			SDL_FreeSurface(surf_text[i]);
		}
		
		return return_surf;
	}
	
	return_surf = Surface::MakeEmptySurface(w,h);
	if ( return_surf == NULL )
		cout << "[TEXT] [ERROR] " << __LINE__ << endl;
	return_surf = TTF_RenderText_Solid( font, text, textColor );
	if ( return_surf == NULL)
		cout << "[TEXT] [ERROR] " << __LINE__ << endl;
		
	return return_surf;
}


void Text::SetFont(char* fontFilePath, int fonSize)
{
	if(fontFilePath != NULL)
		fontPath = fontFilePath;
	if(fonSize == 0)
		fontSize = fonSize;
	ResetFont();
}

void Text::ResetFont()
{
	if(font != NULL)
		TTF_CloseFont( font );
	font = TTF_OpenFont( fontPath, fontSize );
	if(font == NULL)
	{
		cout << "[TEXT] [ERROR] Unable to reset font" << endl;
	}
}