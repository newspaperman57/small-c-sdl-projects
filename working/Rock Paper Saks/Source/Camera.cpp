#include "../Include/Camera.h"

Camera Camera::CameraControl;

Camera::Camera() {
	cout << "[CAMERA] Constructing..." << endl;
	CfgReader cfgReader;
	cfgReader.LoadFile("config");
	WHEIGHT = cfgReader.GetInt("Screen_Width");
	WWIDTH = cfgReader.GetInt("Screen_Height");
	
    x = y = 0;
 
    targetX = targetY = NULL;
 
    targetMode = TARGET_MODE_NORMAL;
}
 
void Camera::OnMove(int MoveX, int MoveY) {
    x += MoveX;
    y += MoveY;
}
 
int Camera::GetX() {
    if(targetX != NULL) {
        if(targetMode == TARGET_MODE_CENTER) {
            return *targetX - (WWIDTH / 2);
        }
 
        return *targetX;
    }
 
    return x;
}
 
int Camera::GetY() {
    if(targetY != NULL) {
        if(targetMode == TARGET_MODE_CENTER) {
            return *targetY - (WHEIGHT / 2);
        }
 
        return *targetY;
    }
 
    return y;
}
 
void Camera::SetPos(int X, int Y) {
    this->x = X;
    this->y = Y;
}
 
void Camera::SetTarget(int* X, int* Y) {
    targetX = X;
    targetY = Y;
}