#ifndef __TEXT
#define __TEXT

#include <SDL/SDL_ttf.h>

#include "Surface.h"

#include <iostream>
#include <string>
#include <vector>

using namespace std;

enum {
	TEXT_NORMAL,
	TEXT_READABLE
};

class Text {
	private:
		TTF_Font* font;
		int fontSize;
		char* fontPath;

	public:
		Text();
		~Text();
		SDL_Color textColor;
		bool shadow, padding, transparent;
		int xShadow, yShadow, xPadding, yPadding;
		
		SDL_Surface* RenderText(const char* text);
		void SetFont(char* fontFilePath, int fonSize = 0);
		
	private:
		void ResetFont();
};

#endif