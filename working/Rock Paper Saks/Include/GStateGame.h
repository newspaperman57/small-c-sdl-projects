#ifndef __GSTATEGAME
#define __GSTATEGAME



#include <SDL/SDL.h>
#include <vector>
#include <string>
#include <cstdlib> 
#include <ctime> 
#include <iostream>

#include "Event.h" 
#include "GState.h"
#include "Surface.h"
#include "Text.h"
#include "Debug.h"
#include "CfgReader.h"

using namespace std;

enum {
	//RGBA color format
	white = 0xFFFFFFFF,
	red = 0xFF0000FF,
	green = 0x00FF00FF,
	blue = 0x0000FFFF
};

struct Tile {
	int gradient;
	Uint32 color;
	int x,y;
	bool changed;
};

class GStateGame : public GState {
    private:
        static GStateGame Instance;
		int WWIDTH, WHEIGHT, TileSize, BorderSize;
		vector<Tile> tiles;
		Tile currentTile;
		Text info;
		SDL_Surface* info_surf;
		int tileIndex, xTiles, yTiles;
		SDL_Surface* background;

		Text text;
		GStateGame();
		~GStateGame();
		
		Tile* GetTile(int x, int y);
		
		void OnKeyDown(SDLKey sym, SDLMod mod, Uint16 unicode);
		void OnMouseMove(int mX, int mY, int relX, int relY, bool Left,bool Right,bool Middle);
		void OnLButtonDown(int mX, int mY);
		void OnMouseWheel(bool Up, bool Down); 
		
    public:
        void OnActivate();
 
        void OnDeactivate();
 
        void OnLoop();
 
        void OnRender(SDL_Surface* Surf_Display);
		
        static GStateGame* GetInstance();
};
 
#endif