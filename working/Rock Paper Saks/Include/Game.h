#ifndef __GAME
#define __GAME
 
#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>
#include <iostream>

#include "GStateManager.h"
#include "CfgReader.h"
#include "FPS.h"
#include "Event.h"
#include "Surface.h"

#include "Debug.h"

using namespace std;
 
class Game : public Event {
    private:
        bool Running, FPSCap;
        SDL_Surface*    Surf_Display;
		int FRAME_CAP, WWIDTH, WHEIGHT, waitTurns, lastFPS, TimesSinceLastTest;
		float FPS_Unstability;
    public:
        Game();
 
        int OnExecute();
 
        bool OnInit();
 
        void OnEvent(SDL_Event* Event);

        void OnExit();
 
        void OnLoop();
 
        void OnRender();
 
        void OnCleanup();
};
 
#endif
