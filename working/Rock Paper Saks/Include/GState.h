#ifndef __GSTATE
#define __GSTATE
 
#include "Event.h"
 
class GState : public Event {
    public:
        GState();
 
    public:
        virtual void OnActivate() = 0;
 
        virtual void OnDeactivate() = 0;
 
        virtual void OnLoop() = 0;
 
        virtual void OnRender(SDL_Surface* Surf_Display) = 0;
};
 
#endif