#ifndef __FPS
#define __FPS
 
#include <iostream>
#include <SDL/SDL.h>
 
class FPS {
    public:
        static FPS FPSControl;
 
    private:
        int OldTime;
        int LastTime;
		int startTime;
 
        float SpeedFactor;
 
        int NumFrames;
        int Frames;
 
    public:
        FPS();
        void    OnLoop();
		void Start();
		float GetTicks();
        int     GetFPS();
        float   GetSpeedFactor();
};
 
#endif
