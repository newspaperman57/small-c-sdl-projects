#ifndef __CAMERA
#define __CAMERA

#include <SDL/SDL.h>
#include "CfgReader.h"

enum {
	TARGET_MODE_NORMAL = 0,
	TARGET_MODE_CENTER
};

class Camera {
	private:
		int x, y, WWIDTH, WHEIGHT;
		int* targetX;
		int* targetY;
		
	public:
		static Camera CameraControl;
		int targetMode;
		
		Camera();
		void OnMove(int moveX, int moveY);
		
		int GetX();
		int GetY();
		
		void SetPos(int x, int y);
		void SetTarget(int* x, int* y);
};

#endif
