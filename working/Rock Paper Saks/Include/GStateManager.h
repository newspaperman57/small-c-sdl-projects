#ifndef __GSTATEMANAGER
#define __GSTATEMANAGER
 
#include "GState.h"
 
enum {
    // Add your Other App States Here
    GSTATE_NONE,
    GSTATE_GAME,
	GSTATE_LIFE
};
 
class GStateManager {
    private:
		
        static GState* ActiveGState;
		
    public:
	
        static int GetActiveGStateID();
		
		static void OnEvent(SDL_Event* Event);
 
        static void OnLoop();
 
        static void OnRender(SDL_Surface* Surf_Display);
 
        static void SetActiveGState(int GStateID);
 
        static GState* GetActiveGState();
};
 
#endif