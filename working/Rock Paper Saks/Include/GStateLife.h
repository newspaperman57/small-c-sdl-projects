#ifndef __GSTATELIFE
#define __GSTATELIFE



#include <SDL/SDL.h>
#include <vector>
#include <string>
#include <cstdlib> 
#include <ctime> 
#include <iostream>

#include "Event.h" 
#include "GState.h"
#include "Surface.h"
#include "Text.h"
#include "Debug.h"
#include "CfgReader.h"

using namespace std;

struct Tile_Life {
	bool life;
	int x,y;
	bool changed;
};

class GStateLife : public GState {
    private:
        static GStateLife Instance;
		int WWIDTH, WHEIGHT, TileSize, BorderSize;
		vector<Tile_Life> tiles, analyst;
		Tile_Life currentTile;
		int tileIndex, xTiles, yTiles;
		bool paused;
		SDL_Surface* background;

		Text text;
		GStateLife();
		~GStateLife();
		
		Tile_Life* GetTile(int x, int y);
		
		void OnKeyDown(SDLKey sym, SDLMod mod, Uint16 unicode);
		void OnMouseMove(int mX, int mY, int relX, int relY, bool Left,bool Right,bool Middle);
		void OnLButtonDown(int mX, int mY);
		
    public:
        void OnActivate();
 
        void OnDeactivate();
 
        void OnLoop();
 
        void OnRender(SDL_Surface* Surf_Display);
		
        static GStateLife* GetInstance();
};
 
#endif