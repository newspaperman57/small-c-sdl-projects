#ifndef __ANIMATION
#define __ANIMATION

#include <SDL/SDL.h>
#include <iostream>

using namespace std;

class Animation {

	private:
        int CurrentFrame;
        int FrameInc;
        int CurrentDelay;
		
	 public:
        int  MaxFrames;
        int FrameDelay;
		bool Oscillate;
 
        Animation();
		
        void OnAnimate();
        void SetFrameRate(int Rate);
        void SetCurrentFrame(int Frame); 
        int  GetCurrentFrame();

};

#endif