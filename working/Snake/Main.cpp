#include "Include/Game.h"
#include <iostream>

#undef main

int main(int argc, char* argv[]) {
	cout << "[MAIN] Game class initiating..." << endl;
    Game snake;
    return snake.OnExecute();
}