#include "../Include/Snack.h"

Snack::Snack()
{
	cout << "[SNACK] Constructing..." << endl;
}

void Snack::OnActivate(int max1X, int max1Y)
{
	maxW = max1X;
	maxH = max1Y;
	CfgReader cfgReader;
	cfgReader.LoadFile("config");
	TileSize = cfgReader.GetInt("Tile_Size");
	points = 0;
	snackImage = Surface::OnLoad("Graphics/tile_sprite.png", TileSize*2,0,TileSize,TileSize);
	Surface::SetColorKey(snackImage);
}

void Snack::OnDeactivate()
{
	SDL_FreeSurface(snackImage);
}

void Snack::OnLoop()
{
	
}

void Snack::OnCollision()
{
	setNewRandPos();
	points+= 100;
	cout << "[POINTS] " << points << endl;
}

void Snack::OnRender(SDL_Surface* display)
{
	Surface::OnDraw(display, snackImage, pos.x, pos.y);
}

void Snack::setNewRandPos()
{
	pos.x = getRandomInt(maxW) * TileSize;
	pos.y = getRandomInt(maxH) * TileSize;
}	

int Snack::getRandomInt(int max)
{
	//time_t seconds;
	srand(SDL_GetTicks());
	//srand((unsigned int) seconds);
	return rand() % max;
}

SDL_Rect Snack::GetPos()
{
	return {(Sint16)pos.x, (Sint16)pos.y,(Uint16)TileSize,(Uint16)TileSize};
}

int Snack::GetPoints()
{	return points; }