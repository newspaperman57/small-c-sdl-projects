#include "../Include/Area.h"
 
Area Area::AreaControl;
 
Area::Area() {
	cout << "[AREA] Constructing..." << endl;
	CfgReader cfgReader;
	cfgReader.LoadFile("config");
	TileSize = cfgReader.GetInt("Tile_Size");
    AreaSize = 0;
}
 
bool Area::OnLoad(char* File, int MAP_WIDTH1, int MAP_HEIGHT1) {
	MAP_WIDTH = MAP_WIDTH1;
	MAP_HEIGHT = MAP_HEIGHT1;
    MapList.clear();
    FILE* FileHandle = fopen(File, "r");
 
    if(FileHandle == NULL) {
        return false;
    }
 
    char TilesetFile[255];
 
    fscanf(FileHandle, "%s\n", TilesetFile);
 
    if((Surf_Tileset = Surface::OnLoad(TilesetFile)) == NULL) {
        fclose(FileHandle);
 
        return false;
    }
 
    fscanf(FileHandle, "%d\n", &AreaSize);
 
    for(int X = 0;X < AreaSize;X++) {
        for(int Y = 0;Y < AreaSize;Y++) {
            char MapFile[255];
 
            fscanf(FileHandle, "%s ", MapFile);
 
            Map tempMap;
            if(tempMap.OnActivate(MapFile, "Graphics/tileset.png") == false) {
                fclose(FileHandle);
 
                return false;
            }
 
            tempMap.surf_Tileset = Surf_Tileset;
 
            MapList.push_back(tempMap);
        }
        fscanf(FileHandle, "\n");
    }
 
    fclose(FileHandle);
 
    return true;
}
 
void Area::OnRender(SDL_Surface* Surf_Display, int CameraX, int CameraY) {
    int MapWidth  = MAP_WIDTH * TileSize;
    int MapHeight = MAP_HEIGHT * TileSize;
 
    int FirstID = -CameraX / MapWidth;
        FirstID = FirstID + ((-CameraY / MapHeight) * AreaSize);
 
    for(int i = 0;i < 4;i++) {
        int ID = FirstID + ((i / 2) * AreaSize) + (i % 2);
 
        if(ID < 0 || ID >= MapList.size()) continue;
 
        int X = ((ID % AreaSize) * MapWidth) + CameraX;
        int Y = ((ID / AreaSize) * MapHeight) + CameraY;
 
        MapList[ID].OnRender(Surf_Display/*, X, Y*/);
    }
}
 
void Area::OnCleanup() {
    if(Surf_Tileset) {
        SDL_FreeSurface(Surf_Tileset);
    }
 
    MapList.clear();
}