#include "../Include/GStateIntro.h"
 
#include "../Include/GStateManager.h"
 
GStateIntro GStateIntro::Instance;
 
GStateIntro::GStateIntro() {
	GStateID = 3;
	cout << "[GSTATE_INTRO] Constructing..." << endl;
    Surf_Logo = NULL;
	CfgReader cfgReader;
	cfgReader.LoadFile("config");
	waitTime = cfgReader.GetInt("Time_Before_Continue");
	splashDir = cfgReader.GetCharStar("Intro_Splash_png");
}

int GStateIntro::GetGStateID()
{
	return GStateID;
}
 
void GStateIntro::OnActivate() {
    // Load Simple Logo
    Surf_Logo = Surface::OnLoad( splashDir );
	cout << "[GStateIntro] OnActivate" << endl;
    StartTime = SDL_GetTicks();
	Debug::Dbug("Intro initialised");
}
 
void GStateIntro::OnDeactivate() {
    if(Surf_Logo) {
        SDL_FreeSurface(Surf_Logo);
        Surf_Logo = NULL;
    }
}
 
void GStateIntro::OnLoop() {
    if(StartTime + waitTime < SDL_GetTicks()) {
        GStateManager::SetActiveGState(GSTATE_MENU);
    }
}
 
void GStateIntro::OnRender(SDL_Surface* Surf_Display) {
    if(Surf_Logo) {
        Surface::OnDraw(Surf_Display, Surf_Logo, 0, 0);
    }
}
 
GStateIntro* GStateIntro::GetInstance() {
    return &Instance;
}