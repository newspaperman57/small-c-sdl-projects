#include "../Include/Pause.h"

Pause::Pause() {
	cout << "[PAUSE] Constructing..." << endl;
	
}

Pause::~Pause() {
	SDL_FreeSurface( PauseSprite );
}

void Pause::OnLoop() {
	pauseAnimation.OnAnimate();
}

void Pause::OnRender( SDL_Surface* display)
{
	Surface::OnDraw(display, pauseScreen, 0,0);
	Surface::OnDraw(display, PauseSprite, (ScreenSize.w - PauseSprite->w)/2,(ScreenSize.h - PauseSprite->h/3)/2,0,pauseAnimation.GetCurrentFrame() * (PauseSprite->h/3),293,136);
}

void Pause::OnActivate() {
	pauseScreen = NULL;
	PauseSprite = NULL;
	
	CfgReader cfgReader;
	cfgReader.LoadFile("config");
	char* spriteDir = cfgReader.GetCharStar("Pause_Sprite");
	PauseSprite = Surface::OnLoad( spriteDir );
	if ( PauseSprite == NULL )
		cout << "[Pause] [ERROR] Couldn't load PauseSprite" << endl;
	if ( !Surface::SetColorKey( PauseSprite ) )
		cout << "[Pause] [ERROR] Couldn't set Colorkey" << endl;
	ScreenSize.w = cfgReader.GetInt("Screen_Width");
	ScreenSize.h = cfgReader.GetInt("Screen_Height");
	FrameCap = cfgReader.GetInt("Frame_Cap"); // Anton din fisk!
	
	pauseScreen = Surface::MakeEmptySurface(ScreenSize.w, ScreenSize.h);
	Surface::FillRect(pauseScreen, &ScreenSize, 0,0,0);
	Surface::SetAlpha(pauseScreen, 255*0.9);
	
	pauseAnimation.MaxFrames = 3; // This does:
	pauseAnimation.FrameDelay = 4/FrameCap/*60*/;
}

void Pause::OnDeactivate() {
	
}