#include "../Include/GStateHighscore.h"
#include "../Include/GStateManager.h"

GStateHighscore GStateHighscore::Instance;
 
GStateHighscore::GStateHighscore() {
	cout << "[GSTATE_HIGHSCORE] Constructing..." << endl;
}

void GStateHighscore::LoadList(char* filePath)
{
	string buffer;
	ifstream file;
	file.open( filePath );
	/*
	NAME1 SCORE
	NAME2 SCORE
	NAME3 SCORE
	...
	...
	*/
	for(int i = 0; i < 10; i++)
	{
		if(file.good())
		{
			highscore temp;
			file >> temp.name;
			file >> temp.score;
			hscores.push_back(temp);
		}
		else
			break;
	}
	file.close();
}

void GStateHighscore::OnKeyDown(SDLKey sym, SDLMod mod, Uint16 unicode) {
	if( sym == SDLK_p )
	{
		GStateManager::SetActiveGState(GSTATE_MENU); 
	}
}

void GStateHighscore::OnActivate() {
	hscores.clear();
	LoadList("Highscores.txt");
	font = TTF_OpenFont( "Fonts\\calibri.ttf", 24 );
}

void GStateHighscore::OnDeactivate() {
	hscores.clear();
	TTF_CloseFont( font );
}

void GStateHighscore::OnLoop() {
	
}
	
void GStateHighscore::OnRender(SDL_Surface* Surf_Display) {
	for(int i = 0; i < hscores.size(); i++)
	{
		string tempStr = hscores[i].name + to_string(hscores[i].score);
		SDL_Surface* temp = TTF_RenderText_Solid( font, tempStr.c_str(), { 255, 255, 255 } );
		Surface::OnDraw(Surf_Display, temp,50,50*i);
		SDL_FreeSurface(temp);
	}
}

GStateHighscore* GStateHighscore::GetInstance() {
    return &Instance;	
}