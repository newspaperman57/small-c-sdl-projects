#include "../Include/Game.h"

Game::Game() {
	cout << "[GAME] Constructing..." << endl;
	Running = true;
	
	CfgReader cfgReader;
	cfgReader.LoadFile("config");
	
	FPSCap = cfgReader.GetBool("Use_FPS_Cap");
	FRAME_CAP = cfgReader.GetInt("Frame_Cap");
	WWIDTH = cfgReader.GetInt("Screen_Width");
	WHEIGHT = cfgReader.GetInt("Screen_Height"); 
}

int Game::OnExecute() {
    if(OnInit() == false) {
        return -1;
    }
	
    SDL_Event Event;
	FPS_Unstability = 0;
    while(Running) {
		FPS::FPSControl.Start();	//Start FPS counter
        while(SDL_PollEvent(&Event)) {
            OnEvent(&Event);
        }
		
        OnLoop();
        OnRender();
		
		if( ( FPSCap == true ) && ( FPS::FPSControl.GetTicks() < 1000 / FRAME_CAP ) )//Execute frame cap
		{
			TimesSinceLastTest++;
			if( FPS::FPSControl.GetFPS() > 45 && TimesSinceLastTest > 60 && FPS::FPSControl.GetFPS() != FRAME_CAP && FPS::FPSControl.GetFPS() != 0 )
			{
				TimesSinceLastTest = 0;
				FPS_Unstability += FPS::FPSControl.GetTicks() * (FPS::FPSControl.GetFPS() - FRAME_CAP)/FRAME_CAP;
			}
			//Sleep the remaining frame time
			if( (( 1000 / FRAME_CAP ) - FPS::FPSControl.GetTicks() + FPS_Unstability) > 0 )
				SDL_Delay( ( 1000 / FRAME_CAP ) - FPS::FPSControl.GetTicks() + FPS_Unstability );
			//else
				//cout << "[GAME] HURRAYY!!!!!!!!!!!!!!!!!!!!!" << endl;
		}
    }
 
    OnCleanup();
 
    return 0;
}

bool Game::OnInit() {
	cout << "INITIALIZEING SDL" << endl;
    if(SDL_Init(SDL_INIT_EVERYTHING) < 0) {
        return false;
    }
	cout << "INITIALIZEING SDL_TTF" << endl;
	if( TTF_Init() == -1 )
    {
        return false;    
    }
 
    if((Surf_Display = SDL_SetVideoMode(WWIDTH, WHEIGHT, 32, SDL_SWSURFACE)) == NULL) {
        return false;
    }
	Debug::Init();
 
    //SDL_EnableKeyRepeat(1, SDL_DEFAULT_REPEAT_INTERVAL / 3);

    GStateManager::SetActiveGState(GSTATE_INTRO);
 
    return true;
}

void Game::OnEvent(SDL_Event* Event) {
    Event::OnEvent(Event);
	
	if( Event->key.keysym.sym == SDLK_F4 && (Event->key.keysym.mod == KMOD_LALT || Event->key.keysym.mod == KMOD_RALT) ) OnExit(); 
    
	GStateManager::OnEvent(Event);
}
 
void Game::OnExit() {
    Running = false;
}

void Game::OnLoop() {
    GStateManager::OnLoop();
 
    FPS::FPSControl.OnLoop();
 
    char Buffer[255];
    sprintf(Buffer, "FPS: %d", FPS::FPSControl.GetFPS());
    SDL_WM_SetCaption(Buffer, Buffer);
	
	Debug::OnLoop();
}

void Game::OnRender() {
	//Fill screen with black to avoid potential ghosting
	SDL_FillRect( Surf_Display, NULL,  SDL_MapRGB( Surf_Display->format, 0, 0, 0));
	//Blit everything from current GState to screen
    GStateManager::OnRender(Surf_Display);
	Debug::OnRender(Surf_Display);
	

	SDL_Flip(Surf_Display);
}

void Game::OnCleanup() {
    GStateManager::SetActiveGState(GSTATE_NONE);
 
    SDL_FreeSurface(Surf_Display);
	
	TTF_Quit();
    SDL_Quit();
}
