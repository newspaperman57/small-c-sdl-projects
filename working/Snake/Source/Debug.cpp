#include "../Include/Debug.h"

TTF_Font* Debug::font = NULL;
SDL_Color Debug::textColor = { 255, 255, 255 };
string Debug::debugText = "";
ofstream Debug::logger;
vector<DbugMsg> Debug::dbugMessages;
SDL_Surface* Debug::surf_debug = NULL;
int Debug::displayTime = 0;
int Debug::listMaxSize = 0;
int Debug::fadeCycleAmount = 0;
int Debug::startAlpha = 0;
bool Debug::render = false;
//Initialize static member variables 
//*/
Debug::Debug()
{
	cout << "[DEBUG] Constructing..." << endl;

}

Debug::~Debug() {
	logger.close();
	TTF_CloseFont( font );
	font = NULL;
	SDL_FreeSurface(surf_debug);
	surf_debug = NULL;
	for(int i = 0; i < dbugMessages.size(); i++)
	{
		SDL_FreeSurface(dbugMessages[i].surf);
		dbugMessages[i].surf = NULL;
	}
	dbugMessages.clear();
}

void Debug::Init()
{
	CfgReader cfgReader;
	cfgReader.LoadFile("config");
	displayTime = cfgReader.GetInt("Display_Time");
	listMaxSize = cfgReader.GetInt("List_Max_size");
	cout << "[Debug] Will show each line for " << displayTime / 1000.0f << " seconds and max " << listMaxSize << " at a time" << endl;
	fadeCycleAmount = (int)(255*cfgReader.GetFloat("Debug_Text_Fading"));
	startAlpha = (int)(255*cfgReader.GetFloat("Debug_Text_Transparency"));
	render = cfgReader.GetBool("Default_Active_State");
	
	surf_debug = NULL;
	for(int i = 0; i < listMaxSize; i++)
	{
		DbugMsg temp;
		temp.surf = NULL;
		temp.time = 0;
		temp.alpha = 0;
		dbugMessages.push_back(temp);
	}
	
	CfgReader cfgReader2; // For some reason the file in cfgReader gets closed when getting here.
	cfgReader2.LoadFile("config");
	logger.open( cfgReader2.GetCharStar("Log_Output") );
	cout << "[DEBUG] Initialize ttf" << endl;
	if( TTF_Init() != -1 )
		font = TTF_OpenFont( cfgReader2.GetCharStar("Debug_Font"), 12 );
	else
		cout << "[Debug] TTF_Init Didn't load properly!" << endl;
	if( font == NULL )
		cout << "[Debug] Font didn't load properly!" << endl;
	else
		cout << "[Debug] Font loaded correctly!" << endl;
	
	SDL_Color textColor = { 255, 255, 255 };
	string debugText = "";
	
	cout << "[DEBUG] Rendering = " << render << endl;
}

void Debug::OnLoop()
{
	float time = SDL_GetTicks();
	for(int i = 0; i < listMaxSize; i++)
	{
		if( dbugMessages[i].surf != NULL && time - dbugMessages[i].time > displayTime )
		{
			dbugMessages[i].alpha -= (int)fadeCycleAmount;
			if( dbugMessages[i].alpha < 1 )
			{
				dbugMessages[i].surf = NULL;
				dbugMessages[i].time = 0;
				dbugMessages[i].alpha = 0;
				continue;
			}
			Surface::SetAlpha(dbugMessages[i].surf, dbugMessages[i].alpha);
		}
	}
}

void Debug::OnRender(SDL_Surface* display)
{
	int w,h,listSize;
	w = 0;
	listSize = 0;
	for(int i = 0; i < listMaxSize; i++)
	{
		if(dbugMessages[i].surf != NULL)
		{
			listSize++;
			if(dbugMessages[i].w > w)
				w = dbugMessages[i].w;
		}
	}
	
	if(render)
	{
		int offset = 0;
		for(int i = 0; i < listSize; i++)
		{
			Surface::OnDraw(display, dbugMessages[i].surf, 32, 32+offset);
			offset += dbugMessages[i].surf->h;
		}
	}
}

void Debug::ToggleRender()
{
	switch(render){
		case true: 
			render = false;
			cout << "[Debug] Debugger deactivated!" << endl;
			break;
		case false: 
			render = true;
			cout << "[Debug] Debugger activated!" << endl;
			break;
	}
}

void Debug::Log( std::string file, int line, string message)
{
    //Write message to file
    logger << file << ", " << line << ": " << message << endl;
}

void Debug::Log( std::string message )
{
    logger << message << endl;
}

void Debug::Dbug( string message, int type )
{
	switch(type) {
		case DBUG_NORMAL:	SetTextColor(255,255,255);	break;
		case DBUG_ALERT:	SetTextColor(255,255,0);	break;
		case DBUG_ERROR: 	SetTextColor(255,0,0);		break;
	}
	
	for(int i = listMaxSize-1; i > 0; i--)
	{
		if(dbugMessages[i-1].surf != NULL)
		{
			dbugMessages[i] = dbugMessages[i-1];
		}
	}
	int w,h;
	if( TTF_SizeText(font, message.c_str(), &w,&h) == -1 )
		cout << "[Debug] [ERROR] Couldn't calculate size of text!" << endl;
	dbugMessages[0].w = w;
	dbugMessages[0].h = h;
	dbugMessages[0].surf = Surface::MakeEmptySurface(w+8,h);
	
	if ( dbugMessages[0].surf == NULL )
		cout << "[Debug] [ERROR] Couldn't make Empty surface!" << endl;
	
	if ( !Surface::FillRect(dbugMessages[0].surf, NULL, 0,0,0) )
		cout << "[Debug] [ERROR] Couldn't complete FillRect!" << endl;
	
	SDL_Surface* temp = TTF_RenderText_Solid( font, message.c_str(), textColor );
	if ( temp == NULL)
		cout << "[Debug] [ERROR] Couldn't render text!" << endl;
	
	if ( !Surface::OnDraw(dbugMessages[0].surf, temp,4,0) )
		cout << "[Debug] [ERROR] Couldn't blit message!" << endl;
	
	SDL_FreeSurface(temp);
	dbugMessages[0].time = SDL_GetTicks();
	dbugMessages[0].alpha = startAlpha;
	if ( !Surface::SetAlpha(dbugMessages[0].surf, dbugMessages[0].alpha) )
		cout << "[Debug] [ERROR] Couldn't set alpha value!" << endl;
}

void Debug::SetTextColor( Uint8 R, Uint8 G, Uint8 B )
{
	textColor = { R, G, B };
}

SDL_Surface* Debug::GetDebugSurf()
{
	return surf_debug;
}

//log( __FILE__, __LINE__ );