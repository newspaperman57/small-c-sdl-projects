#include "../Include/Player.h"

int Player::AddPlayerS(SDL_Surface* playerPart)
{
	playerSurfaces.push_back(playerPart);
	return playerSurfaces.size() - 1;
}

SDL_Surface* Player::GetPlayerS(int wantedPart)
{
	return playerSurfaces[wantedPart];
}

Player::Player()
{
	cout << "[PLAYER] Constructing..." << endl;
}

Player::~Player()
{

}

void Player::addTail(int directiony)
{
	playerPos.push_back({directiony, 0, 0, 0});
}

void Player::OnSnackCollision()
{
	addTail(direction);
	playerPos[playerPos.size() - 1].d = playerPos[playerPos.size() - 2].d;
	playerPos[playerPos.size() - 1].tileConstraintBuffer = playerPos[playerPos.size() - 2].tileConstraintBuffer;
	switch(playerPos[playerPos.size() - 2].d){
		
		case UP:
		playerPos[playerPos.size() - 1].y = playerPos[playerPos.size() - 2].y + TileSize;
		playerPos[playerPos.size() - 1].x = playerPos[playerPos.size() - 2].x;
		break;
		
		case LEFT:
		playerPos[playerPos.size() - 1].x = playerPos[playerPos.size() - 2].x + TileSize;
		playerPos[playerPos.size() - 1].y = playerPos[playerPos.size() - 2].y;
		break;
		
		case DOWN:
		playerPos[playerPos.size() - 1].y = playerPos[playerPos.size() - 2].y - TileSize;
		playerPos[playerPos.size() - 1].x = playerPos[playerPos.size() - 2].x;
		break;
		
		case RIGHT:
		playerPos[playerPos.size() - 1].x = playerPos[playerPos.size() - 2].x - TileSize;
		playerPos[playerPos.size() - 1].y = playerPos[playerPos.size() - 2].y;
		break;
	}
}

void Player::OnKeyDown(SDLKey sym)
{
	switch(sym){
		case SDLK_w: if (OLDdirection != DOWN) direction = UP; break;
		case SDLK_d: if (OLDdirection != LEFT) direction = RIGHT; break;
		case SDLK_s: if (OLDdirection != UP) direction = DOWN; break;
		case SDLK_a: if (OLDdirection != RIGHT) direction = LEFT; break;
		default: break;
	}
}

bool Player::OnTileCollision(int snakePart, int tileType, vector<Tile> tiles )
{	//The 32 should be map width
	if (tiles[(int) (playerPos[snakePart].x / TileSize) + 
					(playerPos[snakePart].y / TileSize)*32].TypeID == tileType || 
		tiles[(int)((playerPos[snakePart].x + TileSize - 1 ) / TileSize) + 
				   ((playerPos[snakePart].y + TileSize - 1 ) / TileSize)*32].TypeID == tileType )
	{		
		return true;
	}
	return false;
}

void Player::OnActivate(int StartPosX, int StartPosY, int StartPosD, int StartLengthy )
{
	bodyP StartPos;
	
	CfgReader cfgReader;
	cfgReader.LoadFile("config");
	TileSize = cfgReader.GetInt("Tile_Size");
	speed = cfgReader.GetInt("Player_Speed");
	
	StartPos.x = StartPosX;
	StartPos.y = StartPosY;
	StartPos.d = StartPosD;
	startLength = StartLengthy;
	
	isDead = false;
	OLDdirection = StartPos.d;
	direction = StartPos.d;
	snakeHead = StartPos.d;
	
	/////////// LOADING OF PLAYER TILESHEET //////////
	
	OnLoad("Graphics/snake_sprite.png", TileSize, TileSize, 3, 30);
	Anim_Control.Oscillate = true;
	SDL_Surface* PlayerTileSheet = Surface::OnLoad("Graphics/snake_sprite.png");
	if( PlayerTileSheet == NULL ) 
		cout << endl << "PlayerTileSheet was not loaded correctly!" << endl << endl;

	h1 = h2 = h22 = h12 = 246;
	//Makes pink
	//Surface::ReplaceColor(PlayerTileSheet, 37,28,109,  109,28,91);	//Set outline color of snake
	//Surface::ReplaceColor(PlayerTileSheet, 69,52,203,  203,52,168);	//Set inner color of snake
	
	
	for(int i = 0; i < PlayerTileSheet->h/TileSize;i++ )
	{
		SDL_Surface* Surf_Temp = Surface::MakeEmptySurface(TileSize, TileSize);
		Surface::OnDraw(Surf_Temp, PlayerTileSheet, 0, 0, 0, i*TileSize, TileSize, TileSize);
		Surface::SetColorKey(Surf_Temp);
		if( GetPlayerS(AddPlayerS(Surf_Temp)) == NULL ) cout << endl << "PlayerSurface was not loaded correctly into vector!" << endl << endl;
		//SDL_FreeSurface( Surf_Temp );
	}
	//////// DONE LOADING OF PLAYER TILESHEET ////////
	for(int i = 0; i < startLength; i++ ) 
	{
		addTail(direction);
		switch(direction) {
			case HEAD0:
				playerPos[i].x = StartPos.x * TileSize;
				playerPos[i].y = StartPos.y * TileSize + (i*TileSize);
				break;
			case HEAD90:
				playerPos[i].x = StartPos.x * TileSize - (i*TileSize);
				playerPos[i].y = StartPos.y * TileSize;
				break;
			case HEAD180:
				playerPos[i].x = StartPos.x * TileSize;
				playerPos[i].y = StartPos.y * TileSize - (i*TileSize);
				break;
			case HEAD270:
				playerPos[i].x = StartPos.x * TileSize + (i*TileSize);
				playerPos[i].y = StartPos.y * TileSize;
				break;
		}
		playerPos[i].d = StartPos.d;
	}
	
	SDL_FreeSurface( PlayerTileSheet );
}

void Player::OnDeactivate()
{
	for (int i = 0; i < playerSurfaces.size();i++)
		SDL_FreeSurface(playerSurfaces[i]);
	playerSurfaces.clear();
	playerPos.clear();
}

bool Player::OnLoop(vector<Tile> tiles)
{
	Anim_Control.OnAnimate();
	for(int i = 0; i < playerSurfaces.size(); i++)
	{
		int s1 = 59, l1 = 27;
		int s2 = 59, l2 = 27;
		int H1 = h1, H2 = h2;
		Surface::HSLToRGB(&H1,&s1,&l1);
		Surface::HSLToRGB(&H2,&s2,&l2);
		Surface::ReplaceColor(playerSurfaces[i], H2,s2,l2,  H1,s1,l1);	//Set outline color of snake
		s1 = 59; l1 = 50;
		s2 = 59; l2 = 50;
		H1 = h12; H2 = h22;
		Surface::HSLToRGB(&H1,&s1,&l1);
		Surface::HSLToRGB(&H2,&s2,&l2);
		Surface::ReplaceColor(playerSurfaces[i], H2,s2,l2,  H1,s1,l1);	//Set inner color of snake
	}
	h2 = h1;
	//h1++;
	h1 = h1 % 360;
	
	h22 = h12;
	//h12++;
	h12 = h12 % 360;
	
	//////////// Calculating snake tail ///////////
	
	for ( int i = playerPos.size() - 1; i > 0;i-- )
	{
		playerPos[i].tileConstraintBuffer += speed;
		switch(playerPos[i].d) {
			case UP: 	playerPos[i].y -= speed; break;
			case RIGHT: playerPos[i].x += speed; break;                     
			case DOWN:	playerPos[i].y += speed; break;
			case LEFT:	playerPos[i].x -= speed; break;
			default: break;
		}
	}
	playerPos[0].tileConstraintBuffer += speed;
	///////// Done Calculating snake tail /////////
	
	switch(OLDdirection){
		case UP: playerPos[0].y -= speed; break;
		case RIGHT: playerPos[0].x += speed; break;
		case DOWN: playerPos[0].y +=  speed; break;
		case LEFT: playerPos[0].x -= speed; break;
		default: break;
	}
	
	switch(OLDdirection){
		case UP: snakeHead = UP; break;
		case RIGHT: snakeHead = RIGHT; break;
		case DOWN: snakeHead = DOWN; break;
		case LEFT: snakeHead = LEFT; break;
		default: break;
	}
	
	if ( playerPos[0].tileConstraintBuffer >= TileSize + 1)
	{
		OLDdirection = direction;
		
		for ( int i = playerPos.size() - 1; i >= 0;i-- )
		{
			playerPos[i].tileConstraintBuffer -= TileSize;
			switch(playerPos[i].d) 
			{
				case UP: playerPos[i].y += playerPos[i].tileConstraintBuffer; break;
				case RIGHT: playerPos[i].x -= playerPos[i].tileConstraintBuffer; break;
				case DOWN: playerPos[i].y -=  playerPos[i].tileConstraintBuffer; break;
				case LEFT: playerPos[i].x += playerPos[i].tileConstraintBuffer; break;
			}

			if (i != 0) 
				playerPos[i].d = playerPos[i - 1].d;
		}
			
		playerPos[0].d = direction;
		
		for ( int i = playerPos.size() - 1; i >= 0;i-- )
		{
			switch(playerPos[i].d) 
			{
				case UP: playerPos[i].y -= playerPos[i].tileConstraintBuffer; break;
				case RIGHT: playerPos[i].x += playerPos[i].tileConstraintBuffer; break;
				case DOWN: playerPos[i].y +=  playerPos[i].tileConstraintBuffer; break;
				case LEFT: playerPos[i].x -= playerPos[i].tileConstraintBuffer; break;
			}
			
			if ( i > 3 && playerPos[0].x / TileSize == playerPos[i].x / TileSize && playerPos[0].y / TileSize == playerPos[i].y / TileSize )
			{
				isDead = true;
			}
		}
		
		newTile = true;
	}
	else
		newTile = false;
}

void Player::OnRender(SDL_Surface* Surf_Display)
{
	if(Surf_Entity == NULL || Surf_Display == NULL) return;
	
	if (Surface::OnDraw(Surf_Display, GetPlayerS(snakeHead), playerPos[0].x, playerPos[0].y) == false)
		cout << "Player sprite not rendered correctly!" << endl;
	
	for(int i = 1; i < playerPos.size();i++)
	{
		//Surface::OnDraw(Surf_Display, Surf_Entity, X, Y, AnimState * Width, Anim_Control.GetCurrentFrame() * Height, Width, Height);
		if (Surface::OnDraw(Surf_Display, Surf_Entity, playerPos[i].x, playerPos[i].y, (Anim_Control.GetCurrentFrame() + i) % Anim_Control.MaxFrames * TileSize, 4 * TileSize, TileSize, TileSize) == false) 
		//if (Surface::OnDraw(Surf_Display, GetPlayerS(BODY), playerPos[i].x, playerPos[i].y) == false) 
			cout << "Player sprite not rendered correctly!" << endl; 
	}
}

vector<bodyP> Player::getPos()
{
	return playerPos;
}

SDL_Rect Player::getPos(int part)
{
	return { (Sint16)playerPos[part].x, (Sint16)playerPos[part].y, (Uint16)TileSize, (Uint16)TileSize };
}

int Player::GetPlayerLength()
{
	return playerPos.size();
}
