#include "../Include/GStateMenu.h"
#include "../Include/GStateManager.h"
 
GStateMenu GStateMenu::Instance;
 
GStateMenu::GStateMenu() {
	GStateID = 2;
	cout << "[GSTATE_MENU] Constructing..." << endl;
	CfgReader cfgReader;
	cfgReader.LoadFile("config");
	WWIDTH = cfgReader.GetInt("Screen_Width");
	WHEIGHT = cfgReader.GetInt("Screen_Height");
	StartingMenuStateS = cfgReader.GetString("Menu_State_At_Start");
		
	if( StartingMenuStateS == "PLAY" ) StartingMenuState = PLAY;
	else if( StartingMenuStateS == "HIGHSCORE" ) StartingMenuState = HIGHSCORE;
	else if( StartingMenuStateS == "EXIT" ) StartingMenuState = EXIT;

	menuState = StartingMenuState;
}
 
void GStateMenu::OnKeyDown(SDLKey sym, SDLMod mod, Uint16 unicode)
{
	switch(sym){
		case SDLK_w:
			switch(menuState) {
				case HIGHSCORE: menuState = PLAY; break;
				case EXIT: menuState = HIGHSCORE; break;
				default: break;
			}
			SDL_Delay(150);
			break;
		case SDLK_s: 
			switch(menuState) {
				case HIGHSCORE: menuState = EXIT; break;
				case PLAY: menuState = HIGHSCORE; break;
				default: break;
			}
			SDL_Delay(150); 
			break;
		case SDLK_RETURN:
			switch(menuState)
			{
				case PLAY: 
					GStateManager::SetActiveGState(GSTATE_GAME); 
					break;
				case HIGHSCORE: 
					GStateManager::SetActiveGState(GSTATE_HIGHSCORE); 
					break;
				case EXIT: 
					GStateManager::SetActiveGState(GSTATE_NONE); 
					SDL_Event quitEvent;
					quitEvent.type = SDL_QUIT;
					SDL_PushEvent(&quitEvent);
					break;
				default: break;
			}
		default: break;
	}
}
 
int GStateMenu::GetGStateID()
{
	return GStateID;
}

void GStateMenu::OnActivate() {
	
	SDL_Surface* tileset = Surface::OnLoad("Graphics/start_menu_sprite.png");
	//SDL_Surface* tileset = Surface::MakeEmptySurface(1000, 1000);
	SDL_Surface* temp = NULL;
	
	cout << "[GStateMenu] Clipping Menu Items" << endl;
	temp = Surface::MakeEmptySurface(tileset->w, 115);
	Surface::OnDraw(temp, tileset, 0, 0, 0, 0, tileset->w, 115 );
	Surface::SetColorKey(temp);
	menuItems.push_back(temp);
	//SDL_FillRect(temp, NULL, 0x000000);
	
		temp = Surface::MakeEmptySurface(tileset->w, 115);
	Surface::OnDraw(temp, tileset, 0, 0, 0, 331, tileset->w, 115 );
	Surface::SetColorKey(temp);
	menuItems.push_back(temp);
	//SDL_FillRect(temp, NULL, 0x000000);
	
	temp = Surface::MakeEmptySurface(tileset->w, 113);
	Surface::OnDraw(temp, tileset, 0, 0, 0, 115, tileset->w, 113 );
	Surface::SetColorKey(temp);
	menuItems.push_back(temp);
	//SDL_FillRect(temp, NULL, 0x000000);
	
	temp = Surface::MakeEmptySurface(tileset->w, 113);
	Surface::OnDraw(temp, tileset, 0, 0, 0, 446, tileset->w, 113 );
	Surface::SetColorKey(temp);
	menuItems.push_back(temp);
	//SDL_FillRect(temp, NULL, 0x000000);
	
	temp = Surface::MakeEmptySurface(tileset->w, 103);
	Surface::OnDraw(temp, tileset, 0, 0, 0, 228, tileset->w, 103 );
	Surface::SetColorKey(temp);
	menuItems.push_back(temp);
	//SDL_FillRect(temp, NULL, 0x000000);
	
	temp = Surface::MakeEmptySurface(tileset->w, 103);
	Surface::OnDraw(temp, tileset, 0, 0, 0, 559, tileset->w, 103 );
	Surface::SetColorKey(temp);
	
	menuItems.resize(6);
	menuItems[5] = Surface::MakeEmptySurface(tileset->w, 103);
	Surface::OnDraw(menuItems[5], tileset, 0, 0, 0, 559, tileset->w, 103);
	Surface::SetColorKey(menuItems[5]);
	
	cout << "[GStateMenu] MenuItems Size: " << menuItems.size() << endl;
	
	// menuItems.push_back(temp);
	//SDL_FillRect(temp, NULL, 0x000000);
	
	/* temp = Surface::OnLoad("Graphics\\menu_start_idle.png");
	Surface::SetColorKey(temp);
	menuItems.push_back(temp);
	
	temp = Surface::OnLoad("Graphics\\menu_start_active.png");
	Surface::SetColorKey(temp);
	menuItems.push_back(temp);
	
	temp = Surface::OnLoad("Graphics\\menu_highscore_idle.png");
	Surface::SetColorKey(temp);
	menuItems.push_back(temp);
	
	temp = Surface::OnLoad("Graphics\\menu_highscore_active.png");
	Surface::SetColorKey(temp);
	menuItems.push_back(temp);
	
	temp = Surface::OnLoad("Graphics\\menu_quit_idle.png");
	Surface::SetColorKey(temp);
	menuItems.push_back(temp);
	
	temp = Surface::OnLoad("Graphics\\menu_quit_active.png");
	Surface::SetColorKey(temp);
	menuItems.push_back(temp); */
	
	
	SDL_FreeSurface(temp);
	SDL_FreeSurface(tileset);
	
	/*for(int i = 0; i < menuItems.size(); i++)
	{
		Surface::SetColorKey(menuItems[i]);
	}*/
	
	cout << "[GStateMenu] Done clipping Menu Items" << endl;
	
}
 
void GStateMenu::OnDeactivate() {
	for(int i = 0; i < menuItems.size(); i++)
	{
		SDL_FreeSurface(menuItems[i]);
	}
	menuItems.clear();
}
 
void GStateMenu::OnLoop() {

}
 
void GStateMenu::OnRender(SDL_Surface* Surf_Display) {
	///*
	switch(menuState)
	{	
		case PLAY: 
			Surface::OnDraw(Surf_Display, menuItems[0], WWIDTH/2 - menuItems[1]->w/2, 0 + 100); 
			Surface::OnDraw(Surf_Display, menuItems[3], WWIDTH/2 - menuItems[2]->w/2, 120 + 100); 
			Surface::OnDraw(Surf_Display, menuItems[5], WWIDTH/2 - menuItems[4]->w/2, 240 + 100); 
			break;
			
		case HIGHSCORE:
			Surface::OnDraw(Surf_Display, menuItems[1], WWIDTH/2 - menuItems[0]->w/2, 0 + 100); 
			Surface::OnDraw(Surf_Display, menuItems[2], WWIDTH/2 - menuItems[3]->w/2, 120 + 100); 
			Surface::OnDraw(Surf_Display, menuItems[5], WWIDTH/2 - menuItems[4]->w/2, 240 + 100); 
			break;

		case EXIT:
			Surface::OnDraw(Surf_Display, menuItems[1], WWIDTH/2 - menuItems[0]->w/2, 0 + 100); 
			Surface::OnDraw(Surf_Display, menuItems[3], WWIDTH/2 - menuItems[2]->w/2, 120 + 100); 
			Surface::OnDraw(Surf_Display, menuItems[4], WWIDTH/2 - menuItems[5]->w/2 , 240 + 100); 
			break;
		default:
			break;
	}
	//*/
	//Surface::OnDraw(Surf_Display, menuItems[0], 0, 0);
}
 
GStateMenu* GStateMenu::GetInstance() {
    return &Instance;
}
