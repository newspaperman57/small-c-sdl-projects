#include "../Include/CfgReader.h"

CfgReader::CfgReader() {
	cout << "[CFG_READER] Constructing..." << endl;
	fileIsOpen = false;
	intVariable = 0;
	stringVariable = "ERROR";
	boolVariable = false;
}

CfgReader::~CfgReader() {
	cout << "[CfgReader] Closing file..." << endl;
	file.close();
}

bool CfgReader::FileIsOpen()
{
	return file.is_open();
}
char* CfgReader::GetCharStar( string wantedCharStar )
{
	file.seekg( 0, ios::beg );
	string buffer = "ERROR!";
	string lastBuffer = "ERROR!";
	//charStarVariable = "ERROR!";
	bool reachedEndOfFile = false;
	file >> buffer;
	if( buffer == "ERROR!" )
	{
		if(!file.is_open())
			cout << "[CONFIG] \"" << wantedCharStar << "\". File isn't open." << endl;
		else
			cout << "[CONFIG] Couldn't read " << wantedCharStar << " from file" << endl;
		return 0;
	}
	while(!reachedEndOfFile)
	{
		if(buffer[0] != '#' )
		{
			lastBuffer = buffer;
			file >> buffer;
			while( buffer[0] != '@' and buffer[0] != '#' )
			{
				if( buffer == "=" && lastBuffer == wantedCharStar )
				{
					file >> buffer;
					
					// Convert char* to string here
					/*
					cout << "[CONFIG] Now trying to convert string to char*" << endl;
					char* charStarVariable = const_cast<char*>(buffer.c_str());
					*/
					
					char *charStarVariable = new char[buffer.length() + 1];
					strcpy(charStarVariable, buffer.c_str());
					
					// End
					cout << "[CONFIG] Found " << wantedCharStar << " As " << charStarVariable << endl;
					return charStarVariable;
				}
				lastBuffer = buffer;
				file >> buffer;
			}
			if( buffer == "@END" )
			{
				reachedEndOfFile = true; 
				break;
			}
		} else {
			do
			{
				lastBuffer = buffer;
				file >> buffer;
			} while( buffer[0] != '@' );
		}
	}
	cout << "[CONFIG] Didn't find a SHIT about " << wantedCharStar << endl;
	return "ERROR!";
}

string CfgReader::GetString( string wantedString )
{
	file.seekg( ios::beg );
	string buffer = "ERROR!";
	string lastBuffer = "ERROR!";
	bool reachedEndOfFile = false;
	file >> buffer;
	if(!file.is_open())
	{
		cout << "[CONFIG] File not opened!" << endl;
		return 0;
	}
	while(!reachedEndOfFile)
	{
		if(buffer[0] != '#' )
		{
			lastBuffer = buffer;
			file >> buffer;
			while( buffer[0] != '@' and buffer[0] != '#' )
			{
				if( buffer == "=" && lastBuffer == wantedString )
				{
					cout << "[CONFIG] Found " << wantedString << endl;
					file >> buffer;
					stringVariable = "";
					while( buffer[0] != '@' and buffer[0] != '#' )
					{
						stringVariable += buffer;
						file >> buffer;
						if( buffer[0] != '@' and buffer[0] != '#' )
							stringVariable += " ";
					}
					return stringVariable;
				}
				lastBuffer = buffer;
				file >> buffer;
			}
			if( buffer == "@END" )
			{
				reachedEndOfFile = true; 
				break;
			}
		} else {
			do
			{
				lastBuffer = buffer;
				file >> buffer;
			} while( buffer[0] != '@' );
		}
	}
	cout << "[CONFIG] Didn't find a SHIT about " << wantedString << endl;
	return stringVariable; // Potential bug here. This will return the last string this function found!
}

int CfgReader::GetInt( string wantedInt )
{	
	file.seekg( ios::beg );
	string buffer = "ERROR!";
	string lastBuffer = "ERROR!";
	bool reachedEndOfFile = false;
	file >> buffer;
	if( buffer == "ERROR!" )
		{
		cout << "[CONFIG] Couldn't read from file. Check if opened" << endl;
		return 0;
	}
	while(!reachedEndOfFile)
	{
		if(buffer[0] != '#' )
		{
			lastBuffer = buffer;
			file >> buffer;
			while( buffer[0] != '@' and buffer[0] != '#' )
			{
				if( buffer == "=" && lastBuffer == wantedInt )
				{
					cout << "[CONFIG] Found " << wantedInt << endl;
					file >> intVariable;
					return intVariable;
				}
				lastBuffer = buffer;
				file >> buffer;
			}
			if( buffer == "@END" )
			{
				reachedEndOfFile = true; 
				break;
			}
		} else {
			do
			{
				lastBuffer = buffer;
				file >> buffer;
			} while( buffer[0] != '@' );
		}
	}
	cout << "[CONFIG] Didn't find a SHIT about " << wantedInt << endl;
	return 0;
}


bool CfgReader::GetBool( string wantedBool )
{
	file.seekg(ios::beg);
	string buffer = "ERROR!";
	string lastBuffer = "ERROR!";
	string boolAsString;
	bool reachedEndOfFile = false;
	file >> buffer;
	if( buffer == "ERROR!" )
	{
		cout << "[CONFIG] Couldn't read from file. Check if opened" << endl;
		return 0;
	}
	while(!reachedEndOfFile)
	{
		if(buffer[0] != '#' )
		{
			lastBuffer = buffer;
			file >> buffer;
			while( buffer[0] != '@' and buffer[0] != '#' )
			{
				if( buffer == "=" && lastBuffer == wantedBool )
				{
					cout << "[CONFIG] Found " << wantedBool << endl;
					file >> boolAsString;
					if(boolAsString == "true")
						return true;
					else if(boolAsString == "false")
						return false;
				}
				lastBuffer = buffer;
				file >> buffer;
			}
			if( buffer == "@END" )
			{
				reachedEndOfFile = true; 
				break;
			}
		} else {
			do
			{
				lastBuffer = buffer;
				file >> buffer;
			} while( buffer[0] != '@');
		}
	}
	cout << "[CONFIG] Didn't find a SHIT about " << wantedBool << endl;
	return 0;
}

float CfgReader::GetFloat( string wantedFloat)
{
	file.seekg( ios::beg );
	string buffer = "ERROR!";
	string lastBuffer = "ERROR!";
	bool reachedEndOfFile = false;
	float foundFloat = 0;
	file >> buffer;
	if( buffer == "ERROR!" )
	{
		cout << "[CONFIG] Couldn't read from file. Check if opened" << endl;
		return 0;
	}
	while(!reachedEndOfFile)
	{
		if(buffer[0] != '#' )
		{
			lastBuffer = buffer;
			file >> buffer;
			while( buffer[0] != '@' and buffer[0] != '#' )
			{
				if( buffer == "=" && lastBuffer == wantedFloat )
				{
					cout << "[CONFIG] Found " << wantedFloat << endl;
					file >> foundFloat;
					return foundFloat;
				}
				lastBuffer = buffer;
				file >> buffer;
			}
			if( buffer == "@END" )
			{
				reachedEndOfFile = true; 
				break;
			}
		} else {
			do
			{
				lastBuffer = buffer;
				file >> buffer;
			} while( buffer[0] != '@');
		}
	}
	cout << "[CONFIG] Didn't find a SHIT about " << wantedFloat << endl;
	return 0;
}

void CfgReader::LoadFile( char* filePath )
{
	cout << "[CfgReader] Opening file..." << endl;
	file.open( filePath );
}