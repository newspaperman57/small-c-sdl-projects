#include "../Include/Entity.h"

std::vector<Entity*> Entity::EntityList;

Entity::Entity() {
	Surf_Entity = NULL;
	
	X = Y = 0.0f;

	Width = Height = 0;

	AnimState = 0;
}

Entity::~Entity() {
}

bool Entity::OnLoad(char* File, int Width, int Height, int MaxFrames, int FrameDelay) {
	if((Surf_Entity = Surface::OnLoad(File)) == NULL) {
		return false;
	}
	
	Surface::SetColorKey(Surf_Entity);

	this->Width = Width;
	this->Height = Height;
	
	Anim_Control.MaxFrames = MaxFrames;
	Anim_Control.FrameDelay = FrameDelay;
	
	return true;
}

void Entity::OnLoop() {
	Anim_Control.OnAnimate();
}

void Entity::OnRender(SDL_Surface* Surf_Display) {
	if(Surf_Entity == NULL || Surf_Display == NULL) return;
	
	Surface::OnDraw(Surf_Display, Surf_Entity, X, Y, AnimState * Width, Anim_Control.GetCurrentFrame() * Height, Width, Height);
}

void Entity::OnCleanup() {
	if(Surf_Entity) {
		SDL_FreeSurface(Surf_Entity);
	}
	
	Surf_Entity = NULL;
}