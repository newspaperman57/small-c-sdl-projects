#include "../Include/Map.h"

Map::Map(){
	cout << "[MAP] Constructing..." << endl;
	CfgReader cfgReader;
	cfgReader.LoadFile("config");
	TileSize = cfgReader.GetInt("Tile_Size");
}

void Map::OnDeactivate()
{
	SDL_FreeSurface( surf_Tileset );
}

bool Map::OnActivate(char* filePath, char* tilesPath) 
{
	surf_Tileset = NULL;
	surf_Map = NULL;
	
	cout << "[MAP] Loading Map " << filePath << "..." << endl;
	tiles.clear();

	string buffer;
	ifstream file;
	file.open( filePath );
	//Read Header
	file >> buffer; //WIDTH
	file >> dimensions.w;
	file >> buffer; //HEIGHT
	file >> dimensions.h;
	file >> buffer; //PLAYERX
	file >> PlayerX;
	file >> buffer; //PLAYERY
	file >> PlayerY;
	file >> buffer; //PLAYERD
	file >> PlayerD;
	file >> buffer; //PlayerStartLength
	file >> PlayerStartLength;
	
	cout << "[MAP] Dimensions of Map: " << dimensions.w << "X" << dimensions.h << endl;
	
	file >> buffer;	//TILEID
	for(int y = 0; y < dimensions.h; y++) {
		for(int x = 0; x < dimensions.w; x++) {
			Tile temp;
			file >> buffer;
			temp.TileID = atoi(buffer.c_str());

			tiles.push_back(temp);
		}
	}
	file >> buffer; //TYPEID
	for(int y = 0; y < dimensions.h; y++) {
		for(int x = 0; x < dimensions.w; x++) {
			file >> buffer;
			tiles[y * dimensions.w + x].TypeID = atoi(buffer.c_str());
		}
	}
	file.close();
	surf_Tileset = Surface::OnLoad(tilesPath);
	if(!surf_Tileset)
	{
		cout << "[MAP] Tiles did not load" << endl;
		return false;
	}
	
	TilesToSurface();
	
	
	return true;
}

int Map::GetPlayerStartPos(string wanted)
{
	if( wanted == "PlayerD" ) return PlayerD;
	else if ( wanted == "PlayerX" ) return PlayerX;
	else if ( wanted == "PlayerY" ) return PlayerY;
	else if ( wanted == "StartLength" ) return PlayerStartLength;
}

void Map::TilesToSurface() {
	surf_Map = Surface::MakeEmptySurface(dimensions.w * TileSize, dimensions.h * TileSize);

	if(surf_Tileset == NULL) return;
	int TilesetWidth = surf_Tileset->w / TileSize;
	int TilesetHeight = surf_Tileset->h / TileSize;
	
	int ID = 0;
	
	for(int y = 0; y < dimensions.h; y++) {
		for(int x = 0; x < dimensions.w; x++) {
			/*
			if(tiles[ID].TypeID == TILE_TYPE_NONE) {
				ID++;
				continue;
			}
			*/
			int tX = x * TileSize;
			int tY = y * TileSize;
			
			int tilesetX = tiles[ID].TileID * TileSize;
			int tilesetY = 0;

			Surface::OnDraw(surf_Map, surf_Tileset, tX, tY, tilesetX, tilesetY, TileSize, TileSize);
			
			ID++;
		}
	}
}

void Map::OnRender(SDL_Surface* surf_Display) {
	Surface::OnDraw(surf_Display, surf_Map, 0, 0, 0, 0, surf_Map->w, surf_Map->h);
}

vector<Tile> Map::GetTiles()
{
	return tiles;
}

SDL_Rect Map::GetDimen() { return dimensions; }