#include "../Include/GModeMapEditor.h"

GModeMapEditor::GModeMapEditor() {
	size = 32;
}

GModeMapEditor::~GModeMapEditor() {

}

void GModeMapEditor::OnActivate() {
	surf_cursor = NULL;
	surf_tilePicker = NULL;
	
	//Setup cursor
	SDL_Rect temp = {1,1,(Uint16)(size-2),(Uint16)(size-2)};
	surf_cursor = Surface::MakeEmptySurface(size,size);
	Surface::FillRect(surf_cursor, NULL, 225,225,225);
	Surface::FillRect(surf_cursor, &temp, 255,0,255);
	Surface::SetColorKey(surf_cursor);
	
	//Setup tile picker
	surf_tilePicker = Surface::OnLoad("Graphics/TilePicker.png");
	Surface::SetColorKey(surf_tilePicker);
}

void GModeMapEditor::OnDeactivate() {
	SDL_FreeSurface(surf_cursor);
	SDL_FreeSurface(surf_tilePicker);
}

void GModeMapEditor::OnLoop() {
	
}

void GModeMapEditor::OnRender( SDL_Surface* surf_display ) {
	Surface::OnDraw(surf_display, surf_cursor, tileX*size,tileY*size);
	Surface::OnDraw(surf_display, surf_tilePicker, size/2,size/2);
}

void GModeMapEditor::OnMouseMove(int X, int Y) {
	tileX = X / size;
	tileY = Y / size;
}