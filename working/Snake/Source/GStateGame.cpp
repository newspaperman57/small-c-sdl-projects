#include "../Include/GStateGame.h"

GStateGame GStateGame::Instance;
 
GStateGame::GStateGame() {
	GStateID = 4;
	surf_score = NULL;
	cout << "[GSTATE_GAME] Constructing..." << endl;
	
	CfgReader cfgReader;
	cfgReader.LoadFile("config");
	WWIDTH = cfgReader.GetInt("Screen_Width");
	WHEIGHT = cfgReader.GetInt("Screen_Height");
	TileSize = cfgReader.GetInt("Tile_Size");
}

GStateGame::~GStateGame()
{
	SDL_FreeSurface(surf_score);
	SDL_FreeSurface(Surf_dieSplash);
}

void GStateGame::OnKeyDown(SDLKey sym, SDLMod mod, Uint16 unicode)
{
	if( gamemode == PLAYING )
	{
		player01.OnKeyDown(sym);
	}
	else if( gamemode == PAUSED )
	{
		//Pause event
	}
	else if( gamemode == MAP_EDITOR )
	{
		//Map editor event
	}
	
	
	if( sym == SDLK_p ) {	
		if(gamemode == PAUSED) {
			lastGamemode = gamemode;
			gamemode = PLAYING;
		}
		else {
			lastGamemode = gamemode;
			gamemode = PAUSED;
		}
	}
	else if( sym == SDLK_o )
	{
		if(gamemode == MAP_EDITOR)
		{
			gamemode = PLAYING;
		}
		else
		{
			gamemode = MAP_EDITOR;
			Debug::Dbug("Map_Edit mode!");
		}
	}
	if( sym == SDLK_q ) {	
		Debug::ToggleRender();
	}
}

void GStateGame::OnMouseMove(int mX, int mY, int relX, int relY, bool Left, bool Right, bool Middle) 
{	
	mapEdit.OnMouseMove(mX, mY);
}

void GStateGame::OnInputBlur()
{
	gamemode = PAUSED;
}

void GStateGame::OnActivate() 
{	
	/*
	if(Area::AreaControl.OnLoad("./Areas/area01", map01.GetDimen().w, map01.GetDimen().h) == false) {
		return;
	}
	*/
	
	
	Surf_dieSplash = Surface::OnLoad("Graphics/died.png");
	Surface::SetColorKey(Surf_dieSplash);
	CfgReader cfgReader;
	cfgReader.LoadFile("config");

	dieSplashCount = 0;
	showedDieSplash = false;
	dieSplashWaitTime = 10;
	
	
	if(map01.OnActivate(cfgReader.GetCharStar("Level_01"), cfgReader.GetCharStar("Level_01_Tile_Sprite")))	
	player01.OnActivate(map01.GetPlayerStartPos("PlayerX"), map01.GetPlayerStartPos("PlayerY"), map01.GetPlayerStartPos("PlayerD"), map01.GetPlayerStartPos("StartLength"));
	//debugger.OnActivate();
	snack.OnActivate(map01.GetDimen().w, map01.GetDimen().h);
	
	
	//Gamemodes activate
	pause.OnActivate();
	mapEdit.OnActivate();
	
	text.OnActivate();
	GiveSnackNewPos();
	//renderDebug = cfgReader.GetBool("Default_Active_State");
	gamemodeS = cfgReader.GetString("Default_Gamemode");
	if(gamemodeS == "PLAYING") gamemode = PLAYING;
	else if (gamemodeS == "PAUSED") gamemode = PAUSED;
	else cout << "[GStateGame] [ERROR] Couldn't get default gamemode!" << endl;
	
	
}

void GStateGame::OnDeactivate() {
	//Area::AreaControl.OnCleanup();
	player01.OnDeactivate();
	snack.OnDeactivate();
	//debugger.OnDeactivate();
	
	//Gamemodes deactivate
	pause.OnDeactivate();
	mapEdit.OnDeactivate();
	
	map01.OnDeactivate();
	text.OnDeactivate();
}

void GStateGame::OnLoop()
{
	if(!player01.isDead)
	{
		if(gamemode == PLAYING)
		{
			snack.OnLoop();
			player01.OnLoop(map01.GetTiles());

			if(player01.newTile)
			{
				for(int i = 3; i < player01.GetPlayerLength(); i++)
				{
					if(Mechanic::Collision(player01.getPos(0), player01.getPos(i)))
						player01.isDead = true;
				}
				
				for( int i = 1; i < map01.GetTiles().size(); i++ ) // For tiles in map
				{
					//This should check for the tile at the players position
					if(map01.GetTiles()[i].TypeID == 1 )
					{
						
						if(Mechanic::Collision(player01.getPos(0), { (Sint16)((i % map01.GetDimen().w ) * TileSize), (Sint16)((i / map01.GetDimen().w) * TileSize), (Uint16)TileSize, (Uint16)TileSize } ))
						{
							player01.isDead = true;
						}
					}
				}
				if(Mechanic::Collision(player01.getPos(0), snack.GetPos() ))
				{
					snack.OnCollision();
					player01.OnSnackCollision();
					GiveSnackNewPos();
				}
			}
		}
		else if(gamemode == PAUSED)
			pause.OnLoop();
		else if(gamemode == MAP_EDITOR)
			mapEdit.OnLoop();
	} else if(showedDieSplash) {
		GStateManager::SetActiveGState(GSTATE_MENU);	
	}
}
	
int GStateGame::GetGStateID()
{
	return GStateID;
}

void GStateGame::GiveSnackNewPos()
{
	snack.setNewRandPos();
	do {
		snackPos = snack.GetPos();
		Debug::Dbug("[SNACK] " + to_string(snackPos.x / TileSize) + "X" + to_string(snackPos.y / TileSize));
		Debug::Dbug("[SNACK] tile: " + to_string(snackPos.y) + to_string(snackPos.x / map01.GetDimen().w));
		Debug::Dbug("[SNACK] TypeID = " + to_string(map01.GetTiles()[snackPos.y + snackPos.x / map01.GetDimen().w + 1].TypeID));

		if( map01.GetTiles()[snackPos.y + snackPos.x / map01.GetDimen().w].TypeID == 0 )
			snackIsColliding = false;
		else
		{
			snackIsColliding = true;
			snack.setNewRandPos();
		}
	} while( snackIsColliding );
	string temp = to_string(snack.GetPoints());
	surf_score = text.RenderText(temp, {255,255,255}, TEXT_READABLE);
	if(surf_score == NULL)
		cout << "NULL" << endl;
	cout << surf_score->w << "x" << surf_score->h << endl;
}
	
void GStateGame::OnRender(SDL_Surface* Surf_Display) {
	//Area::AreaControl.OnRender(Surf_Display, Camera::CameraControl.GetX(), Camera::CameraControl.GetY());
	map01.OnRender(Surf_Display);
	snack.OnRender(Surf_Display);
	player01.OnRender(Surf_Display);
	Surface::OnDraw(Surf_Display, surf_score, 32, 32);
	
	if( gamemode == PAUSED )
		pause.OnRender(Surf_Display);
	else if( gamemode == MAP_EDITOR)
		mapEdit.OnRender(Surf_Display);
	if(player01.isDead)
	{
		dieSplashCount++;
		Surface::OnDraw(Surf_Display, Surf_dieSplash, (Surf_Display->w / 2) - (Surf_dieSplash->w / 2), (Surf_Display->h / 2) - (Surf_dieSplash->h / 2));
		if(dieSplashCount > dieSplashWaitTime)
			showedDieSplash = true;
	}
}

GStateGame* GStateGame::GetInstance() {
    return &Instance;	
}