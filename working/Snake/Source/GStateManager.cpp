#include "../Include/GStateManager.h"
 
// Refer to Other Game States Here
#include "../Include/GStateIntro.h"
#include "../Include/GStateMenu.h"
#include "../Include/GStateHighscore.h"
#include "../Include/GStateGame.h"
 
GState* GStateManager::ActiveGState = 0;
 
void GStateManager::OnEvent(SDL_Event* EventHolder) {
    if(ActiveGState) ActiveGState->OnEvent(EventHolder);
}
 
void GStateManager::OnLoop() {
    if(ActiveGState) ActiveGState->OnLoop();
}
 
void GStateManager::OnRender(SDL_Surface* Surf_Display) {
    if(ActiveGState) ActiveGState->OnRender(Surf_Display);
}
 
void GStateManager::SetActiveGState(int GStateID) {
    if(ActiveGState) ActiveGState->OnDeactivate();
 
    // Also, add your Game State Here so that the Manager can switch to it
    if(GStateID == GSTATE_NONE)     ActiveGState = 0;
    if(GStateID == GSTATE_INTRO)    ActiveGState = GStateIntro::GetInstance();
	if(GStateID == GSTATE_MENU)     ActiveGState = GStateMenu::GetInstance();
	if(GStateID == GSTATE_HIGHSCORE)ActiveGState = GStateHighscore::GetInstance();
    if(GStateID == GSTATE_GAME)     ActiveGState = GStateGame::GetInstance();
	
    if(ActiveGState) ActiveGState->OnActivate();
}
 
GState* GStateManager::GetActiveGState() {
    return ActiveGState;
}