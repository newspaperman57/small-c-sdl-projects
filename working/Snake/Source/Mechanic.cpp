#include "../Include/Mechanic.h"
/*
bool Player::OnTileCollision(int snakePart, int tileType, vector<Tile> tiles )
{
	if (tiles[(int)(playerPos[snakePart].x / TILE_SIZE) + (playerPos[snakePart].y / TILE_SIZE)*32].TypeID == tileType || tiles[(int)((playerPos[snakePart].x + TILE_SIZE - 1 ) / TILE_SIZE) + ((playerPos[snakePart].y + TILE_SIZE - 1 ) / TILE_SIZE)*32].TypeID == tileType )
	{		
		return true;
	}
	return false;
}
*/
bool Mechanic::Collision(SDL_Rect a, SDL_Rect b)
{	
	if( a.y + a.h <= b.y ||
		b.y + b.h <= a.y ||
		a.x + a.w <= b.x ||
		b.x + b.w <= a.x )
		return false;
	else
		return true;
}