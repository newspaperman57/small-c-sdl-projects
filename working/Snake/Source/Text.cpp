#include "../Include/Text.h"

void Text::OnActivate()
{
	fontPath = "Fonts/calibri.ttf";
	fontSize = 42;
	font = NULL;
	font = TTF_OpenFont( fontPath, fontSize );
	if(font == NULL)
	{
		cout << "[TEXT] [ERROR] Unable to load font" << endl;
	}
	textColor = {255,255,255};
}

void Text::OnDeactivate()
{
	TTF_CloseFont( font );
}

SDL_Surface* Text::RenderText(string textA, SDL_Color color, int type )
{	
	int w,h;
	SDL_Surface* surf_temp;
	const char* text = textA.c_str();
	if( TTF_SizeText(font, text, &w,&h) == -1 )
		cout << "[TEXT] [ERROR] " << __LINE__ << endl;
	
	cout << w << "x" << h << endl;
	
	surf_temp = NULL;
	if(type == TEXT_READABLE)
	{
		//Add a black corner border
		SDL_Surface* surf_text = NULL;
		surf_text = Surface::MakeEmptySurface(w+2,h+2);
		Surface::FillRect(surf_text, NULL, 0xFF, 0x00, 0xFF);
		Surface::SetColorKey(surf_text, 0xFF, 0x00, 0xFF);
		
		
		surf_temp = Surface::MakeEmptySurface(w,h);
		surf_temp = TTF_RenderText_Solid( font, text, {50,50,50} );
		
		SDL_Surface* surf_temp2 = Surface::MakeEmptySurface(w,h);
		if ( surf_temp2 == NULL )
			cout << "[TEXT] [ERROR] " << __LINE__ << endl;
		
		surf_temp2 = TTF_RenderText_Solid( font, text, color );
		if ( surf_temp2 == NULL)
			cout << "[TEXT] [ERROR] " << __LINE__ << endl;
			
		Surface::OnDraw(surf_text, surf_temp, 0,1);
		Surface::OnDraw(surf_text, surf_temp2, 1,0);
		
		SDL_FreeSurface(surf_temp);
		SDL_FreeSurface(surf_temp2);
		
		return surf_text;
	}
	
	surf_temp = Surface::MakeEmptySurface(w,h);
	if ( surf_temp == NULL )
		cout << "[TEXT] [ERROR] " << __LINE__ << endl;
	
	surf_temp = TTF_RenderText_Solid( font, text, color );
	if ( surf_temp == NULL)
		cout << "[TEXT] [ERROR] " << __LINE__ << endl;
		
	return surf_temp;
}


void Text::SetFont(char* fontFilePath, int fonSize)
{
	fontPath = fontFilePath;
	fontSize = fonSize;
	ResetFont();
}

//TTF_SizeText(font, message.c_str(), &w,&h);

//
// PRIVATE
//

void Text::ResetFont()
{
	TTF_CloseFont( font );
	font = TTF_OpenFont( fontPath, fontSize );
	if(font == NULL)
	{
		cout << "[TEXT] [ERROR] Unable to reset font" << endl;
	}
}