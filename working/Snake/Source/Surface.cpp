#include "../Include/Surface.h"
 
Surface::Surface() {
	cout << "[SURACE] Constructing..." << endl;
}

SDL_Surface* Surface::OnLoad(char* File) {
    SDL_Surface* Surf_Temp = NULL;
    SDL_Surface* Surf_Return = NULL;
 
    if((Surf_Temp = IMG_Load(File)) == NULL) {
		cout << "[Surface] IMG_Load \"" << File << "\": " << IMG_GetError() << endl;
		return NULL;
    }
	cout << "[Surface] Loaded: " << File << endl;
    Surf_Return = SDL_DisplayFormat(Surf_Temp);
    SDL_FreeSurface(Surf_Temp);
    return Surf_Return;
}

SDL_Surface* Surface::OnLoad(char* File, int x, int y, int w, int h) {
    SDL_Surface* Surf_Temp = NULL;
	SDL_Surface* Surf_Temp2 = NULL;
    SDL_Surface* Surf_Return = NULL;
	
	cout << "[Surface] Loading \"" << File << "\"" << endl;
	
    if((Surf_Temp = IMG_Load(File)) == NULL) {
		cout << "[SURFACE] IMG_Load "<< File <<" : " << IMG_GetError() << endl;
		return NULL;
    }
	Surf_Temp2 = MakeEmptySurface(w,h);
	OnDraw(Surf_Temp2, Surf_Temp, 0, 0, x, y, w, h);
	
    Surf_Return = SDL_DisplayFormat(Surf_Temp2);
    SDL_FreeSurface(Surf_Temp);
    SDL_FreeSurface(Surf_Temp2);

    return Surf_Return;
}

bool Surface::SetColorKey(SDL_Surface* surface, Uint8 R, Uint8 G, Uint8 B)
{
	if( surface == NULL )	return false;
	
	Uint32 colorkey = SDL_MapRGB( surface->format, R, G, B );
    SDL_SetColorKey( surface, SDL_SRCCOLORKEY, colorkey );
	return true;
    
}

bool Surface::SetAlpha( SDL_Surface* surf, Uint8 alpha )
{
	if(SDL_SetAlpha( surf, SDL_SRCALPHA, alpha ) == 0)
		return true;
	else
		return false;
}

bool Surface::ReplaceColor( SDL_Surface* surf, int r1, int g1, int b1, int r2, int g2, int b2)
{
	Uint32 color1 = SDL_MapRGB(surf->format,r1,g1,b1);
	Uint32 color2 = SDL_MapRGB(surf->format,r2,g2,b2);
	SDL_LockSurface(surf);
	for(int y=0;y<surf->h;y++) {
		for(int x=0;x<surf->w;x++) {
			if(GetPixel(surf,x,y) == color1)
				PutPixel(surf,x,y,color2);
		}
	}
	SDL_UnlockSurface(surf);
	return true;
}

bool Surface::FillRect(SDL_Surface* dst, SDL_Rect* dstrect, Uint8 r, Uint8 g, Uint8 b )
{
	if(dst == NULL)
		return false;
	
	if ( SDL_FillRect( dst, dstrect, SDL_MapRGB( dst->format, r, g, b)) == -1)
		return false;
	return true;
}

SDL_Surface* Surface::MakeEmptySurface(int X, int Y)
{
	Uint32 rmask, gmask, bmask, amask;
    #if SDL_BYTEORDER == SDL_BIG_ENDIAN
        rmask = 0xff000000;
        gmask = 0x00ff0000;
        bmask = 0x0000ff00;
        amask = 0x000000ff;
    #else
        rmask = 0x000000ff;
        gmask = 0x0000ff00;
        bmask = 0x00ff0000;
        amask = 0xff000000;
    #endif
	SDL_Surface* temp = SDL_DisplayFormat(SDL_CreateRGBSurface(SDL_SWSURFACE, X, Y, 32, rmask, gmask, bmask, amask));
    return temp;

}
		
bool Surface::OnDraw(SDL_Surface* Surf_Dest, SDL_Surface* Surf_Src, int X, int Y) {
    if(Surf_Dest == NULL || Surf_Src == NULL) {
		return false;
    }
 
    SDL_Rect DestR;
 
    DestR.x = X;
    DestR.y = Y;
 
    if(SDL_BlitSurface(Surf_Src, NULL, Surf_Dest, &DestR) == -1)
		return false;
    return true;
}

bool Surface::OnDraw(SDL_Surface* Surf_Dest, SDL_Surface* Surf_Src, int X, int Y, int X2, int Y2, int W, int H) {
    if(Surf_Dest == NULL || Surf_Src == NULL) {
        return false;
    }
 
    SDL_Rect DestR;
 
    DestR.x = X;
    DestR.y = Y;
 
    SDL_Rect SrcR;
 
    SrcR.x = X2;
    SrcR.y = Y2;
    SrcR.w = W;
    SrcR.h = H;
 
    SDL_BlitSurface(Surf_Src, &SrcR, Surf_Dest, &DestR);
 
    return true;
}

void Surface::HSLToRGB(int* H, int* S, int* L)/*These get turned to RGB*/
{
    float r, g, b;
	float h,s,l;
	h = (float)*H / 359.0f;
	s = (float)*S / 100.0f;
	l = (float)*L / 100.0f;
    if(s == 0){
        r = g = b = l; // achromatic
        r = g = b = l; // achromatic
    }
	else
	{
        float q,p;
		if(l < 0.5f) q = l * (1 + s);
		else q = (l + s) - (l * s);
		
        p = 2 * l - q;
        r = HueToRgb(p, q, h + 1.0f/3.0f);
        g = HueToRgb(p, q, h);
        b = HueToRgb(p, q, h - 1.0f/3.0f);
		
    }
	*H = r*255+0.5f;
	*S = g*255+0.5f;
	*L = b*255+0.5f;
}

//
//	PRIVATE
//

Uint32 Surface::GetPixel(SDL_Surface *surface, int x, int y)
{
    int bpp = surface->format->BytesPerPixel;
    Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;

    switch(bpp) {
    case 1:
        return *p;

    case 2:
        return *(Uint16 *)p;

    case 3:
        if(SDL_BYTEORDER == SDL_BIG_ENDIAN)
            return p[0] << 16 | p[1] << 8 | p[2];
        else
            return p[0] | p[1] << 8 | p[2] << 16;

    case 4:
        return *(Uint32 *)p;

    default:
        return 0;
    }
}

void Surface::PutPixel(SDL_Surface *surface, int x, int y, Uint32 pixel)
{
	//The surface must be locked before calling this!
    
	int bpp = surface->format->BytesPerPixel;
    Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;

    switch(bpp) {
    case 1:
        *p = pixel;
        break;

    case 2:
        *(Uint16 *)p = pixel;
        break;

    case 3:
        if(SDL_BYTEORDER == SDL_BIG_ENDIAN) {
            p[0] = (pixel >> 16) & 0xff;
            p[1] = (pixel >> 8) & 0xff;
            p[2] = pixel & 0xff;
        } else {
            p[0] = pixel & 0xff;
            p[1] = (pixel >> 8) & 0xff;
            p[2] = (pixel >> 16) & 0xff;
        }
        break;

    case 4:
        *(Uint32 *)p = pixel;
        break;
    }
}

float Surface::HueToRgb(float p, float q, float t)
{
	if(t < 0.0f) t += 1.0f;
	if(t > 1.0f) t -= 1.0f;
	if(t < 1.0f/6.0f) return p + (q - p) * 6.0f * t;
	if(t < 1.0f/2.0f) return q;
	if(t < 2.0f/3.0f) return p + (q - p) * ((2.0f/3.0f) - t) * 6.0f;
	return p;
}