#ifndef __PAUSE
#define __PAUSE

#include <iostream>

#include "Surface.h"
#include "CfgReader.h"

#include "Animation.h"

class Pause
{
	private:
		SDL_Surface* PauseSprite;
		SDL_Surface* pauseScreen;
		SDL_Rect ScreenSize;
		
		int FrameCap;
		
	public:
		Animation pauseAnimation;
	
		Pause();
		~Pause();
		void OnLoop();
		void OnRender( SDL_Surface* );
		void OnActivate();
		void OnDeactivate();
};

#endif