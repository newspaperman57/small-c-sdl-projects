#ifndef __GSTATEHIGHSCORE
#define __GSTATEHIGHSCORE

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>

#include <fstream>
#include <string>
#include <vector>

#include "GState.h"
#include "Surface.h"

using namespace std;

struct highscore {
	string name;
	int score;
};

class GStateHighscore : public GState {
    private:
        static GStateHighscore Instance;
		
        GStateHighscore();
		
		void LoadList(char* filePath);
		void OnKeyDown(SDLKey sym, SDLMod mod, Uint16 unicode);
		
		TTF_Font* font;
		vector<highscore> hscores;
    public:
		
        void OnActivate();
 
        void OnDeactivate();
 
        void OnLoop();
 
        void OnRender(SDL_Surface* Surf_Display);
		
    public:
        static GStateHighscore* GetInstance();
};

#endif