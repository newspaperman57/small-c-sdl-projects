#ifndef __TILE
#define __TILE

enum {
	TILE_TYPE_NONE = 0,
	TILE_TYPE_NORMAL,
	TILE_TYPE_SOLID
};

class Tile {
	public:
		int TileID;
		int TypeID;
		
		Tile();
};

#endif