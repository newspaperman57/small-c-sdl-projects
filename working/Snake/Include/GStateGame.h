#ifndef __GSTATEGAME
#define __GSTATEGAME



#include <SDL/SDL.h>
#include <vector>
#include <string>

#include "Event.h" 
#include "GState.h"
#include "Surface.h"
#include "Text.h"

#include "Debug.h"

#include "CfgReader.h"
#include "Map.h"
//#include "Area.h"
#include "Camera.h"
#include "Mechanic.h"
#include "Player.h"
#include "Snack.h"
#include "Pause.h"
#include "GModeMapEditor.h"

using namespace std;

enum{
	PLAYING,
	PAUSED,
	MAP_EDITOR
};

class GStateGame : public GState {
    private:
        static GStateGame Instance;

		Pause pause;
		GModeMapEditor mapEdit;
		Snack snack;
		Map map01;
		Player player01;
		Text text;
		
		int GStateID;
		bool snackIsColliding, showedDieSplash;
		SDL_Rect snackPos;
		int WWIDTH, WHEIGHT, TileSize, dieSplashCount, dieSplashWaitTime;
		char* startLevel;
		int gamemode, lastGamemode;
		string gamemodeS;
		SDL_Surface* surf_score;
		SDL_Surface* Surf_dieSplash;
		
        GStateGame();
		~GStateGame();
		
		void OnKeyDown(SDLKey sym, SDLMod mod, Uint16 unicode);
		void OnMouseMove(int mX, int mY, int relX, int relY, bool Left,bool Right,bool Middle);
        void OnInputBlur();
		
    public:
		int GetGStateID();
		
        void OnActivate();
 
        void OnDeactivate();
 
        void OnLoop();
 
        void OnRender(SDL_Surface* Surf_Display);
		
		void GiveSnackNewPos();
		
        static GStateGame* GetInstance();
};
 
#endif