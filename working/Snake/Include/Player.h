#ifndef __PLAYER
#define __PLAYER

#include <SDL/SDL.h>
#include <vector>
#include <iostream>

#include "Map.h"
#include "CfgReader.h"
#include "Surface.h"
#include "Tile.h"
#include "GStateManager.h"
#include "Debug.h"

#include "Entity.h"

using namespace std;


enum{ UP, RIGHT, DOWN, LEFT };
enum{ HEAD0, HEAD90, HEAD180,  HEAD270, BODY };

struct bodyP 
{
	int d; 
	int x; 
	int y; 
	int tileConstraintBuffer;
};

class Player : public Entity
{
	private:
		bool last;
		int startLength;
		int TileSize;
		int direction;
		int speed;
		int OLDdirection;
		int snakeHead;
		
		int h1,h2;
		int h12,h22;
		
		std::vector<bodyP> playerPos;
		std::vector<SDL_Surface*> playerSurfaces;
		
		int AddPlayerS(SDL_Surface*);
		void addTail(int);
		bool OnTileCollision(int snakePart, int tileType, vector<Tile> tiles );

	public:
		~Player();
		Player();
		SDL_Surface* GetPlayerS(int);
		
		void OnKeyDown(SDLKey sym);
		
		void OnActivate(int, int, int, int);
		void OnDeactivate();
		bool OnLoop(vector<Tile>);
		void OnRender(SDL_Surface* );
		void OnSnackCollision();
		
		bool isDead;
		vector<bodyP> getPos();
		SDL_Rect getPos(int);
		int GetPlayerLength();
		bool newTile;
};

#endif
