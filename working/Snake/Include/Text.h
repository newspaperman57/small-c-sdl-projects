#ifndef __TEXT
#define __TEXT

#include <SDL/SDL_ttf.h>

#include "Surface.h"

#include <string>
#include <iostream>

using namespace std;

enum {
	TEXT_NORMAL,
	TEXT_READABLE
};

class Text {
	private:
		TTF_Font* font;
		SDL_Color textColor;
		char* fontPath;
		int fontSize;

	public:
		void OnActivate();
		void OnDeactivate();
		
		SDL_Surface* RenderText(string textA, SDL_Color color = {255,255,255}, int type = TEXT_NORMAL );
		void SetFont(char* fontFilePath = "Fonts/calibri.ttf", int fonSize = 42);
		
	private:
		void ResetFont();
};

#endif