#ifndef __SURFACE
#define __SURFACE
 
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>

#include <iostream>

using namespace std;

class Surface {
    public:
        Surface();
 
    public:
        //Load Images
		static SDL_Surface* OnLoad(char* File);
		static SDL_Surface* OnLoad(char* File, int x, int y, int w, int h);
		
		//Surface editing
		static bool SetColorKey(SDL_Surface* surface, Uint8 R = 0xFF, Uint8 G = 0x00, Uint8 B = 0xFF);
		static bool SetAlpha( SDL_Surface* surf, Uint8 alpha );
		static bool ReplaceColor( SDL_Surface* surf, int r1, int g1, int b1, int r2, int g2, int b2);
		static bool FillRect(SDL_Surface* dst, SDL_Rect* dstrect, Uint8 r, Uint8 g, Uint8 b );
		
		//New surfaces
		static SDL_Surface* MakeEmptySurface(int X, int Y);
		
		//Surface copying
		static bool OnDraw(SDL_Surface* Surf_Dest, SDL_Surface* Surf_Src, int X = 0, int Y = 0);
        static bool OnDraw(SDL_Surface* Surf_Dest, SDL_Surface* Surf_Src, int X, int Y, int X2, int Y2, int W, int H);
		
		//Color converters
		static void HSLToRGB(int* H, int* S, int* L);
		
	private:
		static Uint32 GetPixel(SDL_Surface *surface, int x, int y);
		static void PutPixel(SDL_Surface *surface, int x, int y, Uint32 pixel);
		static float HueToRgb(float p, float q, float t);
};
 
#endif
