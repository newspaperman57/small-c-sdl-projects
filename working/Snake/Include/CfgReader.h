#ifndef __CFGREADER
#define __CFGREADER

#include <fstream>
#include <iostream>
#include <cstring>
#include <string>

using namespace std;

class CfgReader {
	private:
		string buffer;
		bool fileIsOpen;
		string stringVariable;
		int intVariable;
		bool boolVariable;
		bool succes;
		
		ifstream file;
	public:
		CfgReader();
		~CfgReader();
		
		string GetString( string );
		int GetInt( string );
		bool GetBool( string );
		char* GetCharStar( string );
		float GetFloat( string );

		void LoadFile( char* );
		bool FileIsOpen();
};

#endif