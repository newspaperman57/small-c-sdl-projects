#ifndef __SNACK
#define __SNACK

#include <time.h>
#include <stdlib.h>
#include <iostream>
#include <SDL/SDL.h>

#include "Surface.h"
#include "CfgReader.h"

using namespace std;

struct position
{
	int x;
	int y;
};

class Snack
{
	private:
		int getRandomInt(int);
		SDL_Surface* snackImage;
		position pos;
		int TileSize, maxW, maxH, points;
	public:
		Snack();
		void OnActivate(int,int);
		void setNewRandPos();
		void OnDeactivate();
		void OnLoop();
		void OnRender(SDL_Surface*);
		void OnCollision();
		SDL_Rect GetPos();
		int GetPoints();
};

#endif