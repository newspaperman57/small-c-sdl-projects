#ifndef __MAP
#define __MAP

#include <SDL/SDL.h>
#include <vector>
#include <string>
#include <iostream>
#include <fstream>

#include "CfgReader.h"
#include "Tile.h"
#include "Surface.h"

using namespace std;

class Map {
	private:
		vector<Tile> tiles;
		SDL_Rect dimensions;
		void TilesToSurface();
		int TileSize;
	public:
		int PlayerX;
		int PlayerY;
		int PlayerD;
		
		vector<Tile> GetTiles();
	
		SDL_Surface* surf_Tileset;
		SDL_Surface* surf_Map;
		
		Map();
		
		int GetPlayerStartPos(string);
		int PlayerStartLength;
		bool OnActivate(char* file, char* tilesPath);
		void OnDeactivate();
		SDL_Rect GetDimen();
		void OnRender(SDL_Surface* surf_Display);
};

#endif
