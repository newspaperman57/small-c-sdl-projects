#ifndef __GSTATEMENU
#define __GSTATEMENU

#include <SDL/SDL.h>
#include <vector>
#include <iostream>

#include "CfgReader.h"
#include "GState.h"
#include "Surface.h"

using namespace std;

enum{PLAY = 0,
	 HIGHSCORE = 1,
	 EXIT = 2
	};

class GStateMenu : public GState {
    private:
		int GStateID;
        static GStateMenu Instance;
		int menuState;
		int StartingMenuState, WWIDTH, WHEIGHT;
		string StartingMenuStateS;
        GStateMenu();
		void OnKeyDown(SDLKey sym, SDLMod mod, Uint16 unicode);
		
		vector<SDL_Surface*> menuItems;
		
    public:
        void OnActivate();
        void OnDeactivate();
        void OnLoop();
        void OnRender(SDL_Surface* Surf_Display);
		int GetGStateID();
        static GStateMenu* GetInstance();
};

#endif
