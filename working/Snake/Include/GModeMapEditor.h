#ifndef __GMODE_MAP_EDITOR
#define __GMODE_MAP_EDITOR

#include <iostream>

#include "Surface.h"
#include <SDL/SDL.h>

class GModeMapEditor
{
	private:
		int size, tileX, tileY;
		SDL_Surface* surf_cursor;
		SDL_Surface* surf_tilePicker;
		
	public:
	
		GModeMapEditor();
		~GModeMapEditor();
		void OnActivate();
		void OnDeactivate();
		void OnLoop();
		void OnRender( SDL_Surface* );
		
		void OnMouseMove(int X, int Y);
		
};

#endif