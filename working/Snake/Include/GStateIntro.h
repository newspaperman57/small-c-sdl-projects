#ifndef __GSTATEINTRO
#define __GSTATEINTRO

#include "GState.h"
#include "Surface.h"
#include "CfgReader.h"

#include "Debug.h"

class GStateIntro : public GState {
    private:
        static GStateIntro Instance;
        SDL_Surface* Surf_Logo;
        int StartTime;
		int GStateID;
		int waitTime;
        GStateIntro();
		char* splashDir;
 
    public:
		int GetGStateID();
        void OnActivate();
        void OnDeactivate();
        void OnLoop();
        void OnRender(SDL_Surface* Surf_Display);
 
        static GStateIntro* GetInstance();
};
 
#endif