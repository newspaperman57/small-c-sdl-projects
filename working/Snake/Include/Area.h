#ifndef __AREA
#define __AREA
 
#include "Map.h"
 
class Area {
    private:
        int AreaSize, TileSize;
        SDL_Surface* Surf_Tileset;
		int MAP_WIDTH, MAP_HEIGHT;
 
    public:
        static Area AreaControl;
        std::vector<Map> MapList;
		
        Area();
        bool OnLoad(char* File,int,int);
        void OnRender(SDL_Surface* Surf_Display, int CameraX, int CameraY);
        void OnCleanup();
};
 
#endif