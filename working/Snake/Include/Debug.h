#ifndef __DEBUG
#define __DEBUG

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>

#include "Surface.h"
#include "CfgReader.h"

#include <fstream>
#include <string>
#include <vector>

using namespace std;

enum {
	DBUG_NORMAL,
	DBUG_ALERT,
	DBUG_ERROR
};

struct DbugMsg {
	SDL_Surface* surf;
	float time;
	int alpha, w, h;
};

class Debug {
	private:
		static TTF_Font* font;
		static SDL_Color textColor;
		static string debugText;
		static ofstream logger;
		static vector<DbugMsg> dbugMessages;
		static SDL_Surface* surf_debug;
		
		static int displayTime, listMaxSize, fadeCycleAmount, startAlpha;
		static bool render;
		
		
		static void SetTextColor( Uint8, Uint8, Uint8 );
		
 
    public:
		Debug();
		~Debug();
	
	public:
		static void Init();
		static void OnLoop();
		static void OnRender(SDL_Surface*);
		static void ToggleRender();
		
		static void Log( string file, int line, string message = "" );
		static void Log( string message );
		static void Dbug( string message, int type = DBUG_NORMAL );
		static SDL_Surface* GetDebugSurf();
};

#endif