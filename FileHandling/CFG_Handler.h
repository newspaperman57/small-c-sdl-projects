#include <fstream>
#include <iostream>
#include <string>

using namespace std;

class CFG_Handler {
	private:
		string buffer;
		bool fileIsOpen;
		string stringVariable;
		int intVariable;
		bool boolVariable;
		bool succes;
		
		ifstream file;
	public:
		CFG_Handler();
		~CFG_Handler();
		
		string GetString( string );
		int GetInt( string );
		bool GetBool( string );
		
		void LoadFile( char* );
		bool FileIsOpen();
};