#include "CFG_Handler.h"

CFG_Handler::CFG_Handler() {
	fileIsOpen = false;
	intVariable = 0;
	stringVariable = "ERROR";
	boolVariable = false;
}

CFG_Handler::~CFG_Handler() {
	file.close();
}

bool CFG_Handler::FileIsOpen()
{
	return fileIsOpen;
}

string CFG_Handler::GetString( string wantedInt )
{
	string buffer = "ERROR!";
	string lastBuffer = "ERROR!";
	bool reachedEndOfFile = false;
	file >> buffer;
	if( buffer == "ERROR!" )
		return "ERROR";
	while(!reachedEndOfFile)
	{
		if(buffer[0] != '#' )
		{
			lastBuffer = buffer;
			file >> buffer;
			while( buffer[0] != '@' and buffer[0] != '#' )
			{
				if( buffer == "=" && lastBuffer == wantedInt )
				{
					file >> buffer;
					stringVariable = "";
					while( buffer[0] != '@' and buffer[0] != '#' )
					{
						stringVariable += buffer;
						file >> buffer;
						if( buffer[0] != '@' and buffer[0] != '#' )
							stringVariable += " ";
					}
					return stringVariable;
				}
				lastBuffer = buffer;
				file >> buffer;
			}
			if( buffer == "@END" )
			{
				reachedEndOfFile = true; 
				break;
			}
		} else {
			do
			{
				lastBuffer = buffer;
				file >> buffer;
			} while( buffer[0] != '@' );
		}
	}
	cout << "Didn't find a SHIT!" << endl;
	return stringVariable;
}

int CFG_Handler::GetInt( string wantedInt )
{
	string buffer = "ERROR!";
	string lastBuffer = "ERROR!";
	bool reachedEndOfFile = false;
	file >> buffer;
	if( buffer == "ERROR!" )
		return 0;
	while(!reachedEndOfFile)
	{
		if(buffer[0] != '#' )
		{
			lastBuffer = buffer;
			file >> buffer;
			while( buffer[0] != '@' and buffer[0] != '#' )
			{
				if( buffer == "=" && lastBuffer == wantedInt )
				{
					file >> intVariable;
					return intVariable;
				}
				lastBuffer = buffer;
				file >> buffer;
			}
			if( buffer == "@END" )
			{
				reachedEndOfFile = true; 
				break;
			}
		} else {
			do
			{
				lastBuffer = buffer;
				file >> buffer;
			} while( buffer[0] != '@' );
		}
	}
	cout << "Didn't find a SHIT!" << endl;
	return 0;
}


bool CFG_Handler::GetBool( string wantedBool )
{
	string buffer = "ERROR!";
	string lastBuffer = "ERROR!";
	string boolAsString;
	bool reachedEndOfFile = false;
	file >> buffer;
	if( buffer == "ERROR!" )
		return 0;
	while(!reachedEndOfFile)
	{
		if(buffer[0] != '#' )
		{
			lastBuffer = buffer;
			file >> buffer;
			while( buffer[0] != '@' and buffer[0] != '#' )
			{
				if( buffer == "=" && lastBuffer == wantedBool )
				{
					file >> boolAsString;
					if(boolAsString == "true")
						return true;
					else if(boolAsString == "false")
						return false;
				}
				lastBuffer = buffer;
				file >> buffer;
			}
			if( buffer == "@END" )
			{
				reachedEndOfFile = true; 
				break;
			}
		} else {
			do
			{
				lastBuffer = buffer;
				file >> buffer;
			} while( buffer[0] != '@');
		}
	}
	cout << "Didn't find a SHIT!" << endl;
	return 0;
}

void CFG_Handler::LoadFile( char* filePath )
{
	file.open( filePath );
}