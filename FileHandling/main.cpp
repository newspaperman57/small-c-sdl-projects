#include <iostream>
#include "CFG_Handler.h"

int main()
{
	char* filePath = "FileToRead.cfg";
	
	CFG_Handler fileH;
	if( fileH.FileIsOpen() == false)
		fileH.LoadFile(filePath);
		
	if( fileH.GetBool("BOOL"))
		cout << "SUCCES!" << endl;
	
	return 0;
}